<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





/**
 * This class is a set of fields.
 * Each field have a name and a value.
 *
 * For example for a database this class
 * represents a line in the table
 *
 * @category   Addons
 * @package    Libraries
 * @subpackage ORM
 * @author     Samuel Zebina <samuel.zebina@cantico.fr>
 * @author     Laurent Choulette <laurent.choulette@cantico.fr>
 * @copyright  2008 by CANTICO ({@link http://www.cantico.fr})
 * @license    http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 */
abstract class ORM_Record
{
    /**
     * @var ORM_RecordSet
     */
    protected $oParentSet = null;

    /**
     * @var bool
     */
    private $modified = false;

    /**
     * @var bool
     */
    private $primaryKeyModified = false;

    /**
     * @var ArrayObject
     */
    private	$oData	= null;



    /**
     * Constructs a new ORM_Record.
     *
     * @param ORM_RecordSet $parentSet The set connected to this record.
     *
     * @see ORM_RecordSet::newRecord()
     */
    public function __construct(ORM_RecordSet $parentSet)
    {
        $this->setParentSet($parentSet);

        $this->setData(new ArrayObject(array(), ArrayObject::ARRAY_AS_PROPS));

        $fields = $parentSet->getFields();
        foreach ($fields as $field) {
            if ($field instanceof ORM_RecordSet) {
                $fieldName = $field->getName();
                $this->initValue($fieldName, $field->newRecord());
            }
        }
    }


    /**
     * Set the ORM_RecordSet connected to this record.
     *
     * @param ORM_RecordSet $oParentSet The ORM_RecordSet connected to this record.
     *
     * @return void
     */
    public function setParentSet(ORM_RecordSet $oParentSet = null)
    {
        $this->oParentSet = $oParentSet;
    }


    /**
     * Gets the ORM_RecordSet connected to this record.
     *
     * @return ORM_RecordSet The ORM_RecordSet connected to this record if is defined, null otherwise.
     */
    public function getParentSet()
    {
        return $this->oParentSet;
    }


    /**
     * Returns the value of the specified field or an iterator if $sFieldName represents a 'Many relation'.
     *
     * @param string $sFieldName The name of the field or relation for which the value must be returned.
     *
     * @return mixed The value of the field or null if the field is not a part of the record.
     */
    public function __get($sFieldName)
    {
        return $this->getValue($sFieldName);
    }


    /**
     * Returns the value of the specified field or an iterator if $sFieldName represents a 'Many relation'.
     *
     * @param string $sFieldName The name of the field or relation for which the value must be returned.
     * @param array  $args       Optional arguments.
     *
     * @return mixed The value of the field or null if the field is not a part of the record.
     */
    public function __call($sFieldName, $args)
    {
        $value =  $this->getValue($sFieldName);
        $field = $this->oParentSet->$sFieldName;
        if (!is_null($value) && $field instanceof ORM_FkField) {
            $set = $field->getForeignSet();
            $record = $set->get($value);
            return $record;
        }
        return $value;
    }


    /**
     * Checks whether the specified field is considered 'set'.
     *
     * @param string $sFieldName The name of the field to check.
     *
     * @return bool
     */
    public function __isset($sFieldName)
    {
        return $this->oParentSet->getBackend()->isRecordFieldSet($this, $sFieldName);
    }


    /**
     * Sets the value of a field, if the fieldname is not a part of the record nothing happend
     * This is a shortcut to ORM_Record::setValue().
     * The internal 'modified flag' IS affected.
     *
     * @param string $fieldName  The name of the field for which the value must be set.
     * @param mixed  $fieldValue The value of the field for which the value must be set.
     *
     * @see ORM_Record::setValue()
     * @see ORM_Record::initValue()
     * @return void
     */
    public function __set($fieldName, $fieldValue)
    {
        $this->setValue($fieldName, $fieldValue);
    }



    /**
     * @internal
     * @param bool $primaryKeyModified
     * @return self
     */
    public function setPrimaryKeyModified($primaryKeyModified = true)
    {
        $this->primaryKeyModified = $primaryKeyModified;
        return $this;
    }


    /**
     * Checks if the primary key of the record has been 'manually' changed.
     *
     * @return bool
     */
    public function isPrimaryKeyModified()
    {
        return $this->primaryKeyModified;
    }


    /**
     * Gets the value of a field, if the fieldname is not a part of the record null is returned.
     * If the field is an ORM_Record, the primary key value is returned.
     *
     * @param string $fieldName  The name of the field for which the value must be retrieved.
     * @since 0.9.2
     * @return mixed
     */
    public function getScalarValue($fieldName)
    {
        $value = $this->getValue($fieldName);
        if ($value instanceof ORM_Record) {
            $recordSet = $value->getParentSet();
            $pkName = $recordSet->getPrimaryKey();
            return $value->getValue($pkName);
        }
        return $value;
    }


    /**
     * Gets the value of a field, if the fieldname is not a part of the record null is returned.
     *
     * @param string $fieldName  The name of the field for which the value must be retrieved.
     * @since 0.9.2
     * @see ORM_Record::__get()
     * @see ORM_Record::__call()
     * @return mixed
     */
    public function getValue($fieldName)
    {
        return $this->oParentSet->getBackend()->getRecordValue($this, $fieldName);
    }

    /**
     * Sets the value of a field, if the fieldname is not a part of the record nothing happen.
     * The difference with initValue() is that the internal 'modified flag' IS affected.
     *
     * @param string $fieldName  The name of the field for which the value must be set.
     * @param mixed  $fieldValue The value of the field for which the value must be set.
     *
     * @see ORM_Record::initValue()
     * @see ORM_Record::__set()
     * @return void
     */
    public function setValue($fieldName, $fieldValue)
    {
        $this->oParentSet->getBackend()->setRecordValue($this, $fieldName, $fieldValue);
    }


    /**
     * Sets the value of a field, if the fieldname is not a part of the record nothing happen.
     * The difference with initValue() is that the internal 'modified flag' IS affected.
     *
     * @param string $fieldName  The name of the field for which the value must be set.
     * @param mixed  $fieldValue The value of the field for which the value must be set.
     *
     * @see ORM_Record::initValue()
     * @see ORM_Record::__set()
     * @return void
     */
    public function setInputValue($fieldName, $fieldValue)
    {
        $this->oParentSet->getBackend()->setRecordValue($this, $fieldName, $fieldValue);
    }


    /**
     * Initializes the value of a field, if the fieldname is not a part of the record nothing happen.
     * The difference with setValue() is that the internal 'modified flag' IS NOT affected.
     *
     * @param string $fieldName  The name of the field for which the value must be set.
     * @param mixed  $fieldValue The value of the field for which the value must be set.
     *
     * @see ORM_Record::setValue()
     * @see ORM_Record::__set()
     * @return void
     */
    public function initValue($fieldName, $fieldValue)
    {
        $this->oParentSet->getBackend()->initRecordValue($this, $fieldName, $fieldValue);
    }


    /**
     * Changes the modified status of the record.
     *
     * @param bool $modified The new 'modified' status.
     *
     * @return ORM_Record
     */
    final public function setModified($modified = true)
    {
        $this->modified = $modified;
        if ($modified == false) {
            $this->setPrimaryKeyModified(false);
        }
        return $this;
    }


    /**
     * Returns the modified status of the record.
     *
     * @return bool
     */
    final public function isModified()
    {
        return $this->modified;
    }



    /**
     * Saves the record.
     *
     * @return boolean True on success, false otherwise
     */
    public function save()
    {
        if ($this->oParentSet instanceof ORM_RecordSet) {
            return $this->oParentSet->save($this);
        }
        return false;
    }


    /**
     * Delete the record
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->oParentSet instanceof ORM_RecordSet) {
            $pk = $this->oParentSet->getPrimaryKey();

            if (!isset($pk)) {
                throw new Exception('The delete method require a primary key on the parent set');
            }

            if (!isset($this->$pk)) {
                return false;
            }

            return $this->oParentSet->delete($this->oParentSet->getPrimaryKeyField()->is($this->$pk));
        }
        return false;
    }



    /**
     * Returns the data attached to the Record.
     *
     * @return ArrayObject
     */
    public function getData()
    {
        return $this->oData;
    }



    /**
     * Attach arbitrary data to the Record.
     *
     * @param mixed $oArrayObject The data to attach.
     *
     * @return $this
     */
    public function setData($oArrayObject)
    {
        $this->oData = $oArrayObject;
        return $this;
    }



    /**
     * Sets multiple field values at once.
     *
     * All elements of the provided array having a key that is also a field name of
     * the record will be used to set the corresponding record value.
     * Keys of the array that have no corresponding field name in the record are ignored.
     *
     * @param array $values
     *
     * @see ORM_Record::setFormInputValues()
     * @return ORM_Record
     */
    public function setValues(array $values)
    {
        foreach ($values as $fieldName => $mixedValue) {
            if ($this->oParentSet->fieldExist($fieldName)) {
                if (is_array($mixedValue)) {
                    assert('$this->$fieldName instanceof ORM_Record /* The field "' . $fieldName . '" must be an ORM_Record instead of "' . gettype($this->$fieldName) . '" Missing join. */');
                    $this->$fieldName->setValues($mixedValue);
                } else {
                    assert('!($this->$fieldName instanceof ORM_Record) /* The field "' . $fieldName . '" is an ORM_Record and the value is a "' . gettype($mixedValue) . '" instead of an array. */');
                    $this->$fieldName = $mixedValue;
                }
            }
        }

        return $this;
    }




    /**
     * Sets multiple field values at once using a array posted from an html form.
     *
     * The difference with ORM_Record::setValues() is that each value is
     * converted using the ORM_Field::input() method corresponding to its field.
     *
     * @param array $values
     *
     * @see ORM_Record::setValues()
     * @return ORM_Record
     */
    public function setFormInputValues(array $values)
    {
        $fieldNames = array_keys($values);
        foreach ($fieldNames as $fieldName) {
            if (substr($fieldName, -3) === '_ID' && $values[$fieldName] !== '') {
                $realFieldName = substr($fieldName, 0, strlen($fieldName) - 3);
                $values[$realFieldName] = (int)$values[$fieldName];
                unset($values[$fieldName]);
            }
        }

        foreach ($values as $fieldName => $mixedValue) {
            if ($this->oParentSet->fieldExist($fieldName)) {
                if (is_array($mixedValue) && $this->$fieldName instanceof ORM_Record) {
                    $this->$fieldName->setFormInputValues($mixedValue);
                } else {
                    $field = $this->oParentSet->$fieldName;
                    $field->setRecordValue($this, $this->oParentSet->$fieldName->input($mixedValue));
                }
            }
        }

        return $this;
    }




    /**
     * Returns the values of the record as an associative array.
     *
     * Joined records are returned as sub-arrays.
     *
     * @see ORM_Record::getOutputValues()
     * @return array
     */
    public function getValues()
    {
        $fields	= $this->oParentSet->getFields();
        $values	= array();
        foreach ($fields as $fieldName => $field) {
            if (!($field instanceof ORM_RecordSet)) {
                $values[$fieldName] = $this->$fieldName;
            } else {
                $values[$fieldName] = $this->$fieldName->getValues();
            }
        }

        return $values;
    }




    /**
     * Returns the output values of the record as an associative array.
     *
     * The difference with ORM_Record::getValues() is that values are
     * returned in human-readable format i.e. an internal date '2010-12-25'
     * will be returned here as '25-12-2010'.
     *
     * Joined records are returned as sub-arrays.
     *
     * @see ORM_Record::getValues()
     * @return array
     */
    public function getOutputValues()
    {
        $fields	= $this->oParentSet->getFields();
        $values	= array();
        foreach ($fields as $fieldName => $field) {
            if (!($field instanceof ORM_RecordSet)) {
                $values[$fieldName] = $field->output($this->$fieldName);
            } else {
                $values[$fieldName] = $this->$fieldName->getOuputValues();
            }
        }
        return $values;
    }



    /**
     * Returns the output values of the record as an associative array suited for a form.
     * Joined records are returned as sub-arrays.
     *
     * @return array
     */
    public function getFormOutputValues()
    {
        $fields	= $this->oParentSet->getFields();
        $values	= array();
        foreach ($fields as $fieldName => $field) {
            if (!($field instanceof ORM_RecordSet)) {
                $values[$fieldName] = $field->formOutput($this->$fieldName, $this);
            } else {
                $values[$fieldName] = $this->$fieldName->getFormOutputValues();
            }
        }
        return $values;
    }


    /**
     * Generic method used to display the main field or syntetic information of the record
     * This method is used in lists or drop down when a script need to display a list of unknowns records
     * the default behaviour is to return the name field if exists or the content of the first string field
     *
     * @return string
     */
    public function getRecordTitle()
    {
        if (!($this->oParentSet instanceof ORM_RecordSet)) {
            throw new ORM_Exception('missing a recordSet associated to a record');
        }

        if (isset($this->name)) {
            return $this->oParentSet->name->output($this->name);
        }

        foreach ($this->oParentSet->getFields() as $field) {
            if ($field instanceof ORM_StringField) {
                return $field->output($this->__get($field->getName()));
            }
        }

        throw new ORM_Exception(sprintf('the %s::getRecordTitle() method failed to get a displayable field', get_class($this)));
    }


    /**
     * Get Foreign key primary key
     * return the primary key of a foreign key even if the record is joined or not
     *
     * @param string | ORM_Field $field
     *
     * @return mixed
     */
    public function getFkPk($field)
    {
        if ($field instanceof ORM_Field) {
            $field = $field->getName();
        }

        if ($this->$field instanceof ORM_Record) {
            $foreignRecord = $this->$field;
            /* @var $foreignRecord ORM_Record */

            $pkname = $foreignRecord->getParentSet()->getPrimaryKey();
            return $foreignRecord->$pkname;
        }

        if ('0' === $this->$field) {
            // return the same value as if the set is joined
            return null;
        }

        return $this->$field;
    }


    /**
     * Call destructor before unset() to free memory usage of a record
     *
     */
    public function __destruct()
    {
        $this->oParentSet = null;
    }
}
