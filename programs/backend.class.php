<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/set.class.php';
require_once dirname(__FILE__) . '/criteria.class.php';
require_once dirname(__FILE__) . '/field.class.php';
require_once dirname(__FILE__) . '/exception.class.php';

/**
 * Common base class for all backends.
 */
abstract class ORM_BackEnd
{

    protected $sBackendType = '';


    /**
     * The constructor for Orm Backend.
     */
    public function __construct()
    {
    }

    /**
     * The destructor for Orm Backend.
     */
    public function __destruct()
    {
    }


    /**
     * Sets the table name associated to the specified recordset class.
     *
     * @param ORM_RecordSet $recordSet
     * @param string $tablename
     *
     * @return self
     */
    abstract public function setTableName(ORM_RecordSet $recordSet, $tablename);


    /**
     * Returns the table name associated to the specified recordset class.
     * @param ORM_RecordSet $recordSet
     *
     * @return string
     */
    abstract public function getTableName(ORM_RecordSet $recordSet);

    /**
     * Returns the value of the specified record field.
     * If the specified field does not exist, null is returned.
     *
     * @param ORM_Record $oRecord
     *            The record to retrieve a value from.
     * @param string $sFieldName
     *            The name of the field to retrieve, eg 'start'.
     * @return mixed
     */
    abstract public function getRecordValue(ORM_Record $oRecord, $sFieldName);

    /**
     * Sets the value of the specified record field.
     *
     * If the specified field does not exist the record is left unchanged,
     * otherwise the value of the field is changed and the record is marked
     * as "modified".
     *
     * @param ORM_Record $oRecord
     *            The record to modify.
     * @param string $sFieldName
     *            The name of the field to set, eg 'start'.
     * @param mixed $mixedValue
     *            The value for the field to set, eg. '2013-01-10'.
     */
    abstract public function setRecordValue(
        ORM_Record $oRecord,
        $sFieldName,
        $mixedValue
    );

    /**
     * Similar to setRecordValue() except that the record will not be marked as
     * modified.
     *
     * If the specified field does not exist the record is left unchanged.
     *
     * @param ORM_Record $oRecord
     *            The record to initialize.
     * @param string $sFieldName
     *            The name of the field to set, eg 'start'.
     * @param mixed $mixedValue
     *            The value for the field to set, eg. '2013-01-10'.
     */
    abstract public function initRecordValue(
        ORM_Record $oRecord,
        $sFieldName,
        $mixedValue
    );

    /**
     * Checks if the record has the specified field set.
     *
     * To be considered set:
     * - fieldname must be one of the fields of the record set associated to
     *   the record,
     * - there must be a value associated with this field name,
     * - if the value is a record, it must have associated values (ie. must not
     *   be an empty record).
     *
     * @param ORM_Record $oRecord The record to check the field from.
     * @param string $sFieldName
     * @return bool
     */
    abstract public function isRecordFieldSet(ORM_Record $oRecord, $sFieldName);

    /**
     * Retrieves multiple records from a record set matching a specified criteria.
     *
     * @param ORM_RecordSet $oSet
     *            The record set from which the records will be retrieved.
     * @param ORM_Criteria $oCriteria
     *            The criteria used for selecting records.
     *
     * @return ORM_Iterator
     */
    abstract public function select(
        ORM_RecordSet $oSet,
        ORM_Criteria $oCriteria = null
    );

    /**
     * Retrieves a record from a record set.
     *
     * @param ORM_RecordSet $oSet
     *            The record set from which the record will be retrieved.
     * @param ORM_Criteria|string $oMixed
     *            If specified, can be either a criteria for selecting records
     *            or the value of the primary key.
     * @param string $sPropertyName
     *            Used only if $oMixed is not a criteria.
     *            If specified, this is the field whose value will be used
     *            instead of the primary key.
     *            If not specified or null, the record set primary key will
     *            be used.
     *
     * @return ORM_Record
     */
    abstract public function get(
        ORM_RecordSet $oSet,
        $oMixed = null,
        $sPropertyName = null
    );


    /**
     * Get the select query
     *
     * @param ORM_RecordSet $oSet
     *            The recordset on which the query is applied.
     * @param ORM_Criteria $oCriteria
     *            selecting records
     * @param array $aOrder
     *            set of record, the key of the array must be the fieldname and the value must be ASC or DESC
     * @param array $aGroupBy
     * @param string	$sLimit	start,numelement
     * @return string select query
     */
    abstract public function getSelectQuery(ORM_RecordSet $oSet, ORM_Criteria $oCriteria = null, $aOrder = array(), $aGroupBy = array(), $sLimit = null);


    /**
     * Save a record to backend (create or modify)
     * @param ORM_RecordSet $oSet
     * @param ORM_Record $oRecord
     * @return bool
     */
    abstract public function save(ORM_RecordSet $oSet, ORM_Record $oRecord);

    /**
     * Delete record from backend
     * @param ORM_RecordSet $oSet
     * @param ORM_Criteria $oCriteria
     * @return bool
     */
    abstract public function delete(ORM_RecordSet $oSet, ORM_Criteria $oCriteria = null);

    /**
     * Combine two criteria with AND
     * @param ORM_Criteria $oLeftCriteria
     * @param ORM_Criteria $oRightCriteria
     * @return string
     */
    abstract public function andCriteria(ORM_Criteria $oLeftCriteria, ORM_Criteria $oRightCriteria);

    /**
     * Combine two criteria with OR
     * @param ORM_Criteria $oLeftCriteria
     * @param ORM_Criteria $oRightCriteria
     * @return string
     */
    abstract public function orCriteria(ORM_Criteria $oLeftCriteria, ORM_Criteria $oRightCriteria);

    /**
     * Invert criteria with NOT
     * @param ORM_Criteria $oCriteria
     * @return string
     */
    abstract public function notCriteria(ORM_Criteria $oCriteria);

    /**
     * Greater than
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function greaterThan(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function greaterThanOrEqual(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function lessThan(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function lessThanOrEqual(ORM_Field $oField, $sValue);
    
    /**
     * @param ORM_Field $oField
     * @param mixed $min
     * @param mixed $max
     * @return string
     */
    abstract public function between(ORM_Field $oField, $min, $max);

    /**
     * @param ORM_Field $oField
     * @param mixed $mixedValue
     * @return string
     */
    abstract public function in(ORM_Field $oField, $mixedValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $mixedValue
     * @return string
     */
    abstract public function is(ORM_Field $oField, $mixedValue);


    /**
     * @param ORM_Field $oField
     * @param mixed $mixedValue
     * @return string
     */
    abstract public function isNot(ORM_Field $oField, $mixedValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $mixedValue
     * @return string
     */
    abstract public function notIn(ORM_Field $oField, $mixedValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function notLike(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function soundsLike(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function like(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function startsWith(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function endsWith(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function contains(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function containsWords(ORM_Field $oField, $sValue);

    /**
     * @param ORM_Field $oField
     * @param mixed $sValue
     * @return string
     */
    abstract public function hasValue(ORM_Field $oField, $sValue);
    /**
     * @param ORM_Field $oField
     * @return string
     */
    abstract public function fieldIsNull(ORM_Field $oField);

    // Operation on field

    /**
     * @param ORM_IsNullOperation $operation
     * @return string
     */
    abstract public function isNullOperation(ORM_IsNullOperation $operation);

    /**
     * @param ORM_IfNullOperation $operation
     * @return string
     */
    abstract public function ifNullOperation(ORM_IfNullOperation $operation);

    /**
     * @param ORM_NotOperation $operation
     * @return string
     */
    abstract public function notOperation(ORM_NotOperation $operation);

    /**
     * @param ORM_OrOperation $operation
     * @return string
     */
    abstract public function orOperation(ORM_OrOperation $operation);

    /**
     * @param ORM_AndOperation $operation
     * @return string
     */
    abstract public function andOperation(ORM_AndOperation $operation);

    /**
     * @param ORM_EqualsOperation $operation
     * @return string
     */
    abstract public function equalsOperation(ORM_EqualsOperation $operation);

    /**
     * @param ORM_InOperation $operation
     * @return string
     */
    abstract public function inOperation(ORM_InOperation $operation);

    /**
     * @param ORM_IfEqualsOperation $operation
     * @return string
     */
    abstract public function ifEqualsOperation(ORM_IfEqualsOperation $operation);

    /**
     * @param ORM_CriterionOperation $operation
     * @return string
     */
    abstract public function criterionOperation(ORM_CriterionOperation$operation);

    // Aggregate operations usable with groupBy.
    // //////////////////////////////////////////

    /**
     * @param ORM_CountOperation $operation
     * @return string
     */
    abstract public function countOperation(ORM_CountOperation $operation);

    /**
     * @param ORM_MaxOperation $operation
     * @return string
     */
    abstract public function maxOperation(ORM_MaxOperation $operation);

    /**
     * @param ORM_MinOperation $operation
     * @return string
     */
    abstract public function minOperation(ORM_MinOperation $operation);

    /**
     * @param ORM_SumOperation $operation
     * @return string
     */
    abstract public function sumOperation(ORM_SumOperation $operation);

    /**
     * @param ORM_AverageOperation $operation
     * @return string
     */
    abstract public function averageOperation(ORM_AverageOperation $operation);

    /**
     * @param ORM_GroupConcatOperation $operation
     * @return string
     */
    abstract public function groupConcatOperation(ORM_GroupConcatOperation $operation);

    // String manipulation operations.
    // ////////////////////////////////

    /**
     * @param ORM_LengthOperation $operation
     * @return string
     */
    abstract public function lengthOperation(ORM_LengthOperation $operation);

    /**
     * @param ORM_ToUpperOperation $operation
     * @return string
     */
    abstract public function toUpperOperation(ORM_ToUpperOperation $operation);

    /**
     * @param ORM_ToLowerOperation $operation
     * @return string
     */
    abstract public function toLowerOperation(ORM_ToLowerOperation $operation);

    /**
     * @param ORM_RightOperation $operation
     * @return string
     */
    abstract public function rightOperation(ORM_RightOperation $operation);

    /**
     * @param ORM_LeftOperation $operation
     * @return string
     */
    abstract public function leftOperation(ORM_LeftOperation $operation);

    /**
     * @param ORM_LeftPadOperation $operation
     * @return string
     */
    abstract public function leftPadOperation(ORM_LeftPadOperation $operation);

    /**
     * @param ORM_ConcatOperation $operation
     * @return string
     */
    abstract public function concatOperation(ORM_ConcatOperation $operation);

    /**
     * @param ORM_TrimOperation $operation
     * @return string
     */
    abstract public function trimOperation(ORM_TrimOperation $operation);

    /**
     * @param ORM_ReplaceOperation $operation
     * @return string
     */
    abstract public function replaceOperation(ORM_ReplaceOperation $operation);

    /**
     * @param ORM_StrCmpOperation $operation
     * @return string
     */
    abstract public function strCmpOperation(ORM_StrCmpOperation $operation);

    /**
     * @param ORM_FindInSetOperation $operation
     * @return string
     */
    abstract public function findInSetOperation(ORM_FindInSetOperation $operation);

    /**
     * @param ORM_FindOperation $operation
     * @return string
     */
    abstract public function findOperation(ORM_FindOperation $operation);

    /**
     * @param ORM_FieldOperation $operation
     * @return string
     */
    abstract public function fieldOperation(ORM_FieldOperation $operation);

    // Arithmetic operations.
    // ///////////////////////

    /**
     * @param ORM_RoundOperation $operation
     * @return string
     */
    abstract public function roundOperation(ORM_RoundOperation $operation);

    /**
     * @param ORM_AbsOperation $operation
     * @return string
     */
    abstract public function absOperation(ORM_AbsOperation $operation);

    /**
     * @param ORM_AddOperation $operation
     * @return string
     */
    abstract public function addOperation(ORM_AddOperation $operation);

    /**
     * @param ORM_SubtractOperation $operation
     * @return string
     */
    abstract public function subtractOperation(ORM_SubtractOperation $operation);

    /**
     * @param ORM_MultiplyOperation $operation
     * @return string
     */
    abstract public function multiplyOperation(ORM_MultiplyOperation $operation);

    /**
     * @param ORM_DivideOperation $operation
     * @return string
     */
    abstract public function divideOperation(ORM_DivideOperation $operation);

    /**
     * Returns the argument X, converted from degrees to radians.
     * @param ORM_RadiansOperation $operation
     * @return string
     */
    abstract public function radiansOperation(ORM_RadiansOperation $operation);

    /**
     * Returns the cosine of X, where X is given in radians.
     * @param ORM_CosOperation $operation
     * @return string
     */
    abstract public function cosOperation(ORM_CosOperation $operation);

    /**
     * Returns the arc cosine of X, that is, the value whose cosine is X. Returns NULL if X is not in the range -1 to 1.
     * @param ORM_AcosOperation $operation
     * @return string
     */
    abstract public function acosOperation(ORM_AcosOperation $operation);

    /**
     * Returns the sine of X, where X is given in radians.
     * @param ORM_SinOperation $operation
     * @return string
     */
    abstract public function sinOperation(ORM_SinOperation $operation);


    /**
     * Returns the arc sine of X, that is, the value whose sine is X. Returns NULL if X is not in the range -1 to 1
     * @param   ORM_AsinOperation   $operation
     * @return string
     */
    abstract public function asinOperation(ORM_AsinOperation $operation);

    /**
     * Returns the tangent of X, where X is given in radians.
     * @param   ORM_TanOperation    $operation
     * @return string
     */
    abstract public function tanOperation(ORM_TanOperation $operation);

    /**
     * Returns the arc tangent of the two variables
     * @param   ORM_AtanOperation   $operation
     * @return string
     */
    abstract public function atanOperation(ORM_AtanOperation $operation);

    // Date/time operations.
    // ///////////////////////

    /**
     * date diff
     * @param   ORM_DateDiffOperation   $operation
     * @return string
     */
    abstract public function dateDiffOperation(ORM_DateDiffOperation $operation);

    /**
     * Time diff
     * @param   ORM_TimeDiffOperation   $operation
     * @return string
     */
    abstract public function timeDiffOperation(ORM_TimeDiffOperation $operation);

    /**
     * Operation to get the week number from a date
     * @param   ORM_WeekOperation   $operation
     * @return string
     */
    abstract public function weekOperation(ORM_WeekOperation $operation);

    /**
     * Operation to get a random number
     */
    abstract public function randOperation(ORM_RandOperation $operation);


    // XML manipulation operations.
    // ////////////////////////////

    /**
     * @param ORM_ExtractValueOperation $operation
     * @return string
     */
    abstract public function extractValueOperation(ORM_ExtractValueOperation $operation);
}

/**
 * ORM Backend exception
 */
class ORM_BackEndException extends ORM_Exception
{

    /**
     * @var string
     */
    private $query;

    /**
     *
     * @param string $message
     * @param string $query
     * @param int $code
     */
    public function __construct($message, $query, $code = null)
    {
        $this->query = $query;
        parent::__construct($message, $code);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $str = get_class($this) . " \n<br /><br />\n\n";
        $str .= $this->message . " \n\n<br /><br />\n\n";
        $str .= $this->query . " \n\n<br /><br />\n\n";
        $str .= $this->getTraceAsString() . " \n";

        return $str;
    }
}

/**
 * Exception if a select fail
 */
class ORM_BackEndSelectException extends ORM_BackEndException
{
}

/**
 * Exception if a save fail
 */
class ORM_BackEndSaveException extends ORM_BackEndException
{
}

/**
 * Exception if a delete fail
 */
class ORM_BackEndDeleteException extends ORM_BackEndException
{
}
