<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/set.class.php';
require_once dirname(__FILE__) . '/criteria.class.php';
require_once dirname(__FILE__) . '/exception.class.php';
require_once dirname(__FILE__) . '/customProperty.class.php';










/**
 * This class is used to describe a field. That is to say his name, description,
 * if it is read-only. In addition, if those properties are not sufficient it is
 * possible using ORM_CustomProperties class to add other custom properties.
 *
 * @example fieldExample.class.php This example is in the "examples" subdirectory
 */
abstract class ORM_Field
{
    private $oCustomProperties	= null;

    protected $sName			= '';
    protected $sDescription		= '';
    protected $oParentSet		= null;

    private $iUniqueId			= 0;
    private static $iGuid		= 0;

    private $bReadOnly          = false;

    /**
     * List of keys to link to the set once the field is associated to a set
     * @var array
     */
    private $scheduledKeys      = array();

    /**
     * @var bool
     */
    private $isNullAllowed      = false;

    /**
     * @param string				$sName				The name of the field
     * @param bool					$bReadOnly			True if the field is read only, False otherwise
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property
     *
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        $this->setName($sName);
        $this->setReadOnly($bReadOnly);
        $this->setCustomPropertiesObject($oCustomProperties);
        $this->iUniqueId = self::$iGuid++;
    }


    /**
     * Return a value that indicate if this class
     * manage custom property
     *
     * @return bool True if the class manage custom property, False otherwise
     */
    private function isCustomPropertiesEnabled()
    {
        return isset($this->oCustomProperties);
    }


    /**
     * Set the object that allow this class to extend property
     *
     * @param ORM_CustomProperties $oCustomProperties
     *
     * @return self	The $this of this class
     */
    public function setCustomPropertiesObject(ORM_CustomProperties $oCustomProperties = null)
    {
        $this->oCustomProperties = $oCustomProperties;
        return $this; /* Fluent interface */
    }


    /**
     * Get a custom property value
     *
     * @param string $sName Name of the custom property
     * @return mixed
     *
     * @throws ORM_OutOfBoundException if the property does not exists.
     */
    public function getCustomPropertyValue($sName)
    {
        if (!$this->isCustomPropertiesEnabled()) {
            return null;
        }
        return $this->oCustomProperties->get($sName);
    }


    /**
     * Set a custom property value
     *
     * @param string			$sName		Name of the custom property
     * @param mixed				$mixedValue	Value of the custom property
     *
     * @return ORM_Field|null				The $this of this class if the property exists, null otherwise
     *
     * @throws ORM_OutOfBoundException if the property does not exists.
     */
    public function setCustomPropertyValue($sName, $mixedValue)
    {
        if (!$this->isCustomPropertiesEnabled()) {
            return null;
        }

        $this->oCustomProperties->set($sName, $mixedValue);
        return $this; /* Fluent interface */
    }


    /**
     * Get the unique identifier of the field
     *
     * @return int
     */
    public function getUniqueId()
    {
        return $this->iUniqueId;
    }



    /**
     * Checks that the name is valid for this field.
     * Overridden by ORM_RecordSet.
     *
     * @param string $name
     * @return bool
     */
    public function isValidName($name)
    {
        return $name === '' || preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $name);
    }


    /**
     * Set the name of the field.
     *
     * The name must contain only letters, numbers and underscore (_) characters.
     * It must not start with a number.
     *
     * @param string $name The name of the field
     *
     * @throws ORM_IllegalArgumentException
     *
     * @return self
     */
    public function setName($name)
    {
        if (!is_string($name)) {
            throw new ORM_IllegalArgumentException('Setting invalid name for ORM_Field: name is not a string.');
        }
        if (!$this->isValidName($name)) {
            throw new ORM_IllegalArgumentException(
                'Adding a field with an invalid name. '
                . 'The field name "' . $name . '" does not match ^[a-zA-Z_][a-zA-Z0-9_]*$.'
            );
        }

        $this->sName = $name;
        return $this;
    }


    /**
     * Returns the field name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->sName;
    }


    /**
     * For RecordSets, checks if the specified field is a part of the set, for other fields return false.
     *
     * @param string $sFieldName Name of the field
     *
     * @return boolean ORM_Field True if the field exist, false otherwise
     */
    public function fieldExist($sFieldName)
    {
        return false;
    }


    /**
     * Returns a corresponding input widget.
     *
     * @since 0.9.6
     *
     * @return Widget_Displayable_Interface
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->LineEdit()->setName($this->getName());
    }


    /**
     * Returns a corresponding labelled input widget.
     *
     * @since 0.9.6
     *
     * @return Widget_LabelledWidget
     */
    public function getLabelledWidget()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            $this->getDescription(),
            $this->getWidget()
        );
    }


    /**
     * Returns a widget displaying the specified value
     *
     * @since 0.11.14
     *
     * @param mixed $mixedValue
     * @return Widget_Displayable_Interface
     */
    public function outputWidget($mixedValue)
    {
        $W = bab_Widgets();

        return $W->Html()->setHtml(bab_toHtml($this->output($mixedValue)));
    }


    /**
     * Returns the field path.
     *
     * @return string
     */
    public function getPath()
    {
        $path = $this->getName();
        for ($parentSet = $this->getParentSet(); $parentSet !== null; $parentSet = $parentSet->getParentSet()) {
            $parentName = $parentSet->getName();
            if ($parentName !== '') {
                $path = $parentName . '/' . $path;
            }
        }
        return $path;
    }


    /**
     * Sets the field textual description.
     * @return self
     */
    public function setDescription($sDescription)
    {
        assert('is_string($sDescription); /* Parameter $sDescription must be a string. */');
        $this->sDescription = $sDescription;
        return $this;
    }


    /**
     * Returns the field textual description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->sDescription;
    }


    /**
     * Defines if this field is readonly.
     *
     * @since 0.9.6
     *
     * @param bool  $readOnly
     * @return self
     */
    public function setReadOnly($readOnly = true)
    {
        $this->bReadOnly = (bool)$readOnly;
        return $this;
    }


    /**
     * Return a value that indicate if this field is readonly
     *
     * @return bool
     */
    public function isReadOnly()
    {
        return $this->bReadOnly;
    }


    /**
     * Create an index key with just this field
     * or schedule an index key creation if the set is not allready associated
     *
     * @return self
     */
    public function index()
    {
        if (isset($this->oParentSet)) {
            $this->oParentSet->addIndexKey($this);
        } else {
            require_once dirname(__FILE__).'/fieldkey.class.php';

            $key = new ORM_FieldKey(array($this));
            $key->unique = false;
            $this->scheduledKeys[] = $key;
        }

        return $this;
    }

    /**
     * Create a unique key with just this field
     * or schedule an unique key creation if the set is not allready associated
     *
     * @return self
     */
    public function unique()
    {
        if (isset($this->oParentSet)) {
            $this->oParentSet->addUniqueKey($this);
        } else {
            require_once dirname(__FILE__).'/fieldkey.class.php';

            $key = new ORM_FieldKey(array($this));
            $key->unique = true;
            $this->scheduledKeys[] = $key;
        }

        return $this;
    }



    /**
     * Checks if the field allows null values.
     *
     * @since 0.9.1
     *
     * @return bool
     */
    public function isNullAllowed()
    {
        return $this->isNullAllowed;
    }


    /**
     * Defines if the field should allow null values.
     *
     * @since 0.9.1
     *
     * @param bool $allowed
     * @return self
     */
    public function  setNullAllowed($allowed = true)
    {
        $this->isNullAllowed = $allowed;
        return $this;
    }


    /**
     * Set the set associated to this field
     *
     * @param ORM_RecordSet $oParentSet
     */
    public function setParentSet(ORM_RecordSet $oParentSet = null)
    {
        $this->oParentSet = $oParentSet;

        foreach($this->scheduledKeys as $key) {
            $this->oParentSet->addFieldKey($key);
        }

        $this->scheduledKeys = array();
    }


    /**
     * Get the associated set
     *
     * @return ORM_RecordSet
     */
    public function getParentSet()
    {
        return $this->oParentSet;
    }


    /**
     * Converts the value to the corresponding internal format stored in the field.
     *
     * @deprecated use ORM_Field::input() instead
     * @see ORM_Field::input()
     * @param mixed $mixedValue
     */
    public function convert($mixedValue)
    {
        return $this->input($mixedValue);
    }


    /**
     * Returns the value with a suitable string for display.
     *
     * @param mixed $mixedValue
     * @return string
     */
    public function output($mixedValue)
    {
        return (string)$mixedValue;
    }


    /**
     * Checks that the human-readable $value is valid and could be converted to a suitable format for saving.
     *
     * @param mixed $value
     * @return string
     */
    public function checkInput($value)
    {
        if (!$this->isNullAllowed() && is_null($value)) {
            return 'The null value is not allowed.';
        }

        return null;
    }

    /**
     * Converts the human-readable $value into a suitable format for saving.
     *
     * @param mixed $value
     * @return mixed
     */
    public function input($value)
    {
        return $value;
    }

    /**
     * Get the value to record, called automatically by the backend before saving the value
     * do not call directly
     *
     * @param mixed         $value
     * @param babDatabase   $database
     * @return string
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($value);
    }


    /**
     * Returns the value with a suitable form for an input field value.
     *
     * @param mixed $mixedValue
     * @return mixed
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        return $mixedValue;
    }


    /**
     * Test if a record value from the field is set
     *
     * @since 0.9.3
     *
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        return isset($mixedValue);
    }


    /**
     * Sets the language of the field.
     *
     * For now only used by ORM_MultilangTextField.
     *
     * @since 0.9.4
     *
     * @param string $language  The language code ('en', 'fr'...).
     * @return self
     */
    public function useLang($language = null)
    {
        return $this;
    }


    /**
     * @since 0.9.8
     *
     * @param ORM_Record $record
     *
     * @return mixed
     */
    public function getRecordValue(ORM_Record $record)
    {
        $data = $record->getData();
        $fieldName = $this->getName();
        if (!$data->offsetExists($fieldName)) {
            return null;
        }
        return $data->offsetGet($fieldName);
    }

    /**
     * @since 0.9.8
     *
     * @param ORM_Record $record
     * @param mixed $mixedValue
     */
    public function setRecordValue(ORM_Record $record, $mixedValue)
    {
        $this->initRecordValue($record, $mixedValue);
        $record->setModified(true);
    }

    /**
     * @since 0.9.8
     *
     * @param ORM_Record $record
     * @param mixed $mixedValue
     */
    public function initRecordValue(ORM_Record $record, $mixedValue)
    {
        $record->getData()->offsetSet($this->getName(), $mixedValue);
    }


    /**
     * The afterSave method is called on each field by the backend after a save has been performed.
     * The record primary key is available.
     *
     * If an error occured, the function must return false else return true.
     *
     * @since 0.9.8
     *
     * @param ORM_Record $record
     * @return boolean
     */
    public function afterSave(ORM_Record $record)
    {
        return true;
    }

    //Criterion



    /**
     * Returns an equal criterion "name = 'Samuel'"
     *
     * @param mixed	$mixedValue		A scalar value.
     *
     * @return ORM_IsCriterion
     */
    public function is($mixedValue)
    {
        return new ORM_IsCriterion($this, $mixedValue);
    }


    /**
     * Return an not equal criterion "NOT(name = 'sam')"
     *
     * @param mixed	$mixedValue		A scalar value.
     *
     * @return ORM_NotCriterion
     */
    public function isNot($mixedValue)
    {
        return new ORM_NotCriterion($this, $mixedValue);
    }


    /**
     * Return a greater than criterion
     *
     * @param mixed	$mixedValue		One value or an array of values.
     *
     * @return ORM_GreaterThanCriterion
     */
    public function greaterThan($mixedValue)
    {
        return new ORM_GreaterThanCriterion($this, $mixedValue);
    }


    /**
     * Return a greater than or equal criterion
     *
     * @param mixed	$mixedValue		One value or an array of values.
     *
     * @return ORM_GreaterThanOrEqualCriterion
     */
    public function greaterThanOrEqual($mixedValue)
    {
        return new ORM_GreaterThanOrEqualCriterion($this, $mixedValue);
    }


    /**
     * Return a less than criterion
     *
     * @param mixed	$mixedValue		One value or an array of values.
     *
     * @return ORM_LessThanCriterion
     */
    public function lessThan($mixedValue)
    {
        return new ORM_LessThanCriterion($this, $mixedValue);
    }


    /**
     * Return a less than or equal criterion
     *
     * @param mixed	$mixedValue		One value or an array of values.
     *
     * @return ORM_LessThanOrEqualCriterion
     */
    public function lessThanOrEqual($mixedValue)
    {
        return new ORM_LessThanOrEqualCriterion($this, $mixedValue);
    }


    /**
     * Return an "in" criterion or a subselect.
     * For a regular IN, the method accept one or more mixed value argument or
     * an array with mixed values.
     * For a subselect, an ORM_CriterionBase object can be set as the first
     * parameter and an optional string for the second parameter for the field
     * name to use in the subselect.
     * If the field name is not given, the system will try to get the best
     * fieldname to use to match the main field within the hasOne relations.
     *
     * @param mixed	...$mixedValue		One or more mixed value or an array of values.
     *
     * @return ORM_InCriterion
     */
    public function in()
    {
        $aArgList = func_get_args();
        $iNumArgs = func_num_args();
        if (0 < $iNumArgs && is_array($aArgList[0])) {
            $aArgList = $aArgList[0];
        }
        return new ORM_InCriterion($this, $aArgList);
    }

    /**
     * Return an not in criterion
     *
     * @param mixed	...$mixedValue		One or more mixed value
     *
     * @return ORM_NotInCriterion
     */
    public function notIn()
    {
        $aArgList = func_get_args();
        $iNumArgs = func_num_args();
        if (0 < $iNumArgs && is_array($aArgList[0])) {
            $aArgList = $aArgList[0];
        }
        return new ORM_NotInCriterion($this, $aArgList);
    }

    /**
     * Return a like criterion
     *
     * @param string	$sValue
     *
     * @return ORM_LikeCriterion
     */
    public function like($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_LikeCriterion($this, $sValue);
    }

    /**
     * Return a not like criterion
     *
     * @param string	$sValue
     *
     * @return ORM_NotLikeCriterion
     */
    public function notLike($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_NotLikeCriterion($this, $sValue);
    }

    /**
     * Return a sounds like criterion
     *
     * @since 0.9.9
     *
     * @param string	$sValue
     *
     * @return ORM_SoundsLikeCriterion
     */
    public function soundsLike($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_SoundsLikeCriterion($this, $sValue);
    }



    /**
     * Return a start with criterion
     *
     * @param string	$sValue
     *
     * @return ORM_StartsWithCriterion
     */
    public function startsWith($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_StartsWithCriterion($this, $sValue);
    }

    /**
     * Return an end with criterion
     *
     * @param string	$sValue
     *
     * @return ORM_EndsWithCriterion
     */
    public function endsWith($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_EndsWithCriterion($this, $sValue);
    }

    /**
     * Return a contains criterion
     *
     * @param string	$sValue
     *
     * @return ORM_ContainsCriterion
     */
    public function contains($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_ContainsCriterion($this, $sValue);
    }


    /**
     * Return a containsWords criterion
     *
     * @param string	$sValue
     *
     * @return ORM_ContainsWordsCriterion
     */
    public function containsWords($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_ContainsWordsCriterion($this, $sValue);
    }


    /**
     * Return a hasValue criterion
     *
     * @param string	$sValue
     *
     * @return ORM_HasValueCriterion
     */
    public function hasValue($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_HasValueCriterion($this, $sValue);
    }


    /**
     * The matchOne method explodes the search string given as parameter,
     * elements are words or groups of words enclosed in double quotes.
     * One element from the list of elements found in parameter must match
     * with a "contains" criterion.
     *
     * @param	string	$sValue
     *
     * @return	ORM_MatchSearchCriterion
     */
    public function matchOne($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_MatchSearchCriterion($this, $sValue, '_OR_', 'contains');
    }

    /**
     * The matchAll method explodes the search string given as parameter,
     * elements are words or groups of words with double quotes
     * all elements from the list of elements found in parameter must match a
     * contains criterion.
     *
     * @param	string	$sValue
     *
     * @return	ORM_MatchSearchCriterion
     *
     */
    public function matchAll($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_MatchSearchCriterion($this, $sValue, '_AND_', 'contains');
    }



    /**
     * The matchAllWords method explodes the search string given as parameter,
     * elements are words or groups of words with double quotes
     * all elements from the list of elements found in parameter must match a
     * containsWords criterion.
     *
     * @param	string	$sValue
     *
     * @return	ORM_MatchSearchCriterion
     *
     */
    public function matchAllWords($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_MatchSearchCriterion($this, $sValue, '_AND_', 'containsWords');
    }



    /**
     * The likeOne method explodes the search string given as parameter,
     * elements are words or groups of words with double quotes
     * one element from the list of elements found in parameter must match a
     * like criterion.
     *
     * @param	string	$sValue
     *
     * @return	ORM_MatchSearchCriterion
     *
     */
    public function likeOne($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_MatchSearchCriterion($this, $sValue, '_OR_', 'like');
    }

    /**
     * The likeAll method explodes the search string given as parameter,
     * elements are words or groups of words with double quotes
     * all elements from the list of elements found in parameter must match a
     * like criterion.
     *
     * @param	string	$sValue
     *
     * @return	ORM_MatchSearchCriterion
     *
     */
    public function likeAll($sValue)
    {
        assert('is_string($sValue); /* Parameter $sValue must be a string. */');
        return new ORM_MatchSearchCriterion($this, $sValue, '_AND_', 'like');
    }




    /**
     * Return a is null criterion based on the field
     *
     * @return ORM_IsNullCriterion
     */
    public function fieldIsNull()
    {
        return new ORM_IsNullCriterion($this);
    }



    /**
     * Return a criteria matching all the criteria passed in parameters.
     *
     * Takes any number of ORM_Criteria of nested arrays of ORM_Criteria as parameters.
     * If no parameter is passed
     *
     * @since 0.9.12
     *
     * @return ORM_Criteria
     */
    public function all()
    {
        $args = func_get_args();
        return ORM_And($args, $this);
    }


    /**
     * Return a criteria matching at least one of the criteria passed in parameters.
     *
     * Takes any number of ORM_Criteria of nested arrays of ORM_Criteria as parameters.
     *
     * @since 0.9.12
     *
     * @return ORM_Criteria
     */
    public function any()
    {
        $args = func_get_args();
        return ORM_Or($args, $this);
    }


    /**
     * Return a none criterion (always false)
     *
     * @since 0.9.12
     *
     * @return ORM_FalseCriterion
     */
    public function none()
    {
        return new ORM_FalseCriterion($this);
    }
    
    public function between($min, $max)
    {
        return new ORM_BetweenCriterion($this, $min, $max);
    }


    //Operations


    /**
     * @return ORM_NotOperation
     */
    public function _not()
    {
        $operation = new ORM_NotOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     * @param ORM_BoolField|bool $expression
     * @return ORM_OrOperation
     */
    public function _or_($expression)
    {
        $operation = new ORM_OrOperation('', $this, $expression);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     * @param ORM_BoolField|bool $expression
     * @return ORM_AndOperation
     */
    public function _and_($expression)
    {
        $operation = new ORM_AndOperation('', $this, $expression);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     * @param mixed $expression
     * @return ORM_EqualsOperation
     */
    public function equals($expression)
    {
        $operation = new ORM_EqualsOperation('', $this, $expression);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     * @param mixed $expression
     * @param mixed $valueTrue
     * @param mixed $valueFalse
     * @return ORM_IfEqualsOperation
     */
    public function ifEquals($expression, $valueTrue, $valueFalse)
    {
        $operation = new ORM_IfEqualsOperation('', $this, $expression, $valueTrue, $valueFalse);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     *
     * @return ORM_IsNullOperation
     */
    public function isNull()
    {
        return new ORM_IsNullOperation('', $this);
    }

    /**
     * Specify a value if the field/expression is null.
     *
     * @return ORM_IfNullOperation
     */
    public function ifNull($valueIfNull)
    {
        $operation = new ORM_IfNullOperation('', $this, $valueIfNull);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Length in characters of the field.
     *
     * @return ORM_LengthOperation
     */
    public function length()
    {
        $operation = new ORM_LengthOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Uppercase the field.
     *
     * @return ORM_ToUpperOperation
     */
    public function toUpper()
    {
        $operation = new ORM_ToUpperOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     * Lowercase the field.
     *
     * @return ORM_ToLowerOperation
     */
    public function toLower()
    {
        $operation = new ORM_ToLowerOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Right characters of the field.
     *
     * @param ORM_NumericField|int $length		The number of characters.
     * @return ORM_RightOperation
     */
    public function right($length)
    {
        $operation = new ORM_RightOperation('', $this, $length);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Left characters of the field.
     *
     * @param ORM_NumericField|int $length		The number of characters.
     * @return ORM_LeftOperation
     */
    public function left($length)
    {
        $operation = new ORM_LeftOperation('', $this, $length);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     *
     *
     * @param mixed $date       The date to "subtract"
     * @return ORM_DateDiffOperation
     */
    public function datediff($date)
    {
        $operation = new ORM_DateDiffOperation('', $this, $date);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     *
     *
     * @param mixed $datetime       The datetime to "subtract"
     * @return ORM_TimeDiffOperation
     */
    public function timediff($datetime)
    {
        $operation = new ORM_TimeDiffOperation('', $this, $datetime);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Get week number from date, numbered as in ISO-8601, week start on monday
     * same behaviour as the php date function with 'W'
     * use mode 3 in MySql function
     *
     * @since 0.9.7
     *
     * @return ORM_WeekOperation
     */
    public function week()
    {
        $operation = new ORM_WeekOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * @return ORM_RandOperation
     */
    public function rand($seed = null)
    {
        $operation = new ORM_RandOperation('', $seed);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     *
     *
     * @param mixed $xpath      An xpath string
     * @return ORM_ExtractValueOperation
     */
    public function extractValue($xpath)
    {
        $operation = new ORM_ExtractValueOperation('', $this, $xpath);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Concatenation of the field and all operands.
     *
     * @param mixed ...$operands		Any number of strings or ORM_Fields
     * @return ORM_ConcatOperation
     */
    public function concat()
    {
        return call_user_func_array(
            array($this->getParentSet(), 'concat'),
            array_merge(array($this), func_get_args())
        );
    }

    /**
     * Get position in a list of values, can be used to order by a query result in an order defined in the list
     *
     * @todo Accept a SET field as a parameter?
     *
     * @param array $list
     */
    public function findInSet(array $list)
    {
        $operation = new ORM_FindInSetOperation('', $this, $list);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Get position of a key in a SET
     *
     * @param string $key
     */
    public function find($key)
    {
        $operation = new ORM_FindOperation('', $this, $key);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * get position in a list of values, can be used to order by a query result in an order defined in the list
     * @param array $list
     */
    public function field(Array $list)
    {
        $operation = new ORM_FieldOperation('', $this, $list);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     * Trim a field or string
     *
     * @param string 	$remstr		string to trim default is space
     * @param int 		$where		ORM_TrimOperation::BOTH | ORM_TrimOperation::LEADING | ORM_TrimOperation::TRAILING
     * @return ORM_TrimOperation
     */
    public function trim($remstr = ' ', $where = ORM_TrimOperation::BOTH)
    {
        $operation = new ORM_TrimOperation('', $this, $remstr, $where);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Replace substring in a field or string
     *
     * @param string 	$fromStr	Substring to replace
     * @param string 	$toStr		Replacement
     * @return ORM_ReplaceOperation
     */
    public function replace($fromStr, $toStr)
    {
        $operation = new ORM_ReplaceOperation('', $this, $fromStr, $toStr);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Pad a string on the left of the field to a certain length with the specified string.
     *
     * @param ORM_NumericField|int      $length The number of characters.
     * @param ORM_StringField|string    $str    The padding string.
     * @return ORM_LeftPadOperation
     */
    public function leftPad($length, $str)
    {
        $operation = new ORM_LeftPadOperation('', $this, $length, $str);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Radians value of the field.
     *
     * @return ORM_RadiansOperation
     */
    public function radians()
    {
        $operation = new ORM_RadiansOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Cosinus value of the field.
     *
     * @return ORM_CosOperation
     */
    public function cos()
    {
        $operation = new ORM_CosOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Arc Cosinus value of the field.
     *
     * @return ORM_AcosOperation
     */
    public function acos()
    {
        $operation = new ORM_AcosOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Sinus value of the field.
     *
     * @return ORM_SinOperation
     */
    public function sin()
    {
        $operation = new ORM_SinOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Arc Sinus value of the field.
     *
     * @return ORM_AsinOperation
     */
    public function asin()
    {
        $operation = new ORM_AsinOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Tangant value of the field.
     *
     * @return ORM_TanOperation
     */
    public function tan()
    {
        $operation = new ORM_TanOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Arc Tangant value of the field.
     *
     * @return ORM_AtanOperation
     */
    public function atan()
    {
        $operation = new ORM_AtanOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Rounds a decimal or float field
     * @param int	$decimals optional number of decimals
     * @return ORM_RoundOperation
     */
    public function round($decimals = 0)
    {
        $operation = new ORM_RoundOperation('', $this, $decimals);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Absolute value of the field.
     *
     * @return ORM_AbsOperation
     */
    public function abs()
    {
        $operation = new ORM_AbsOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     * Compare 2 strings
     * 0 if strings are equal,
     * -1 if the first is smaller,
     * 1 in others cases
     *
     * @param ORM_Field|string $str
     *
     * @return ORM_StrCmpOperation
     */
    public function strcmp($str)
    {
        $operation = new ORM_StrCmpOperation('', $this, $str);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     * Sum of the field and operand.
     *
     * @param ORM_NumericField|int  $operand
     * @return ORM_AddOperation
     */
    public function plus($operand)
    {
        $operation = new ORM_AddOperation('', $this, $operand);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Difference of the field and operand.
     *
     * @param ORM_NumericField|int  $operand
     * @return ORM_SubtractOperation
     */
    public function minus($operand)
    {
        $operation = new ORM_SubtractOperation('', $this, $operand);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Multiplication of the field and operand.
     *
     * @param ORM_NumericField|int  $operand
     * @return ORM_MultiplyOperation
     */
    public function times($operand)
    {
        $operation = new ORM_MultiplyOperation('', $this, $operand);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * Division of the field by operand.
     *
     * @param ORM_NumericField|int  $operand
     * @return ORM_DivideOperation
     */
    public function divideBy($operand)
    {
        $operation = new ORM_DivideOperation('', $this, $operand);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }


    /**
     * @param bool $distinct
     *            True to eliminate duplicate values.
     * @return ORM_CountOperation
     */
    public function count($distinct = null)
    {
        $operation = new ORM_CountOperation('', $this, $distinct);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     *
     * @return ORM_MaxOperation
     */
    public function max()
    {
        $operation = new ORM_MaxOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     *
     * @return ORM_MinOperation
     */
    public function min()
    {
        $operation = new ORM_MinOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     *
     * @return ORM_SumOperation
     */
    public function sum()
    {
        $operation = new ORM_SumOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }



    /**
     *
     * @return ORM_AverageOperation
     */
    public function average()
    {
        $operation = new ORM_AverageOperation('', $this);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }

    /**
     *
     * @param string $separator
     *            The string literal value that should be inserted between
     *            group values.
     * @param bool $distinct
     *            True to eliminate duplicate values.
     * @return ORM_GroupConcatOperation
     */
    public function groupConcat($separator = null, $distinct = null)
    {
        $operation = new ORM_GroupConcatOperation('', $this, $separator, $distinct);
        $operation->setParentSet($this->getParentSet());
        return $operation;
    }





    /**
     * Call destructor before unset() to free memory usage of a field
     *
     */
    public function __destruct()
    {
        $this->oParentSet = null;
        $this->oCustomProperties = null;
    }
}

/**
 * Returns a field containing a record identifier.
 *
 * There must be at most one PkField in a Set.
 *
 * @param string $sName
 *            of the field.
 * @param bool $bReadOnly
 *            the field is read only, false otherwise.
 * @param ORM_CustomProperties $oCustomProperties
 *            ORM_Field class to extends property.
 *
 * @return ORM_PkField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_PkField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_PkField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a record identifier.
 *
 * There must be at most one PkField in a Set.
 */
class ORM_PkField extends ORM_Field
{

    /**
     *
     * @var bool
     */
    protected $auto_increment = true;

    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }

    /**
     * Set the primary key auto increment status
     * default is true if supported by backend
     * @param bool $status
     *
     * @return self
     */
    public function setAutoIncrement($status)
    {
        $this->auto_increment = $status;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isAutoIncremented()
    {
        return $this->auto_increment;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::setRecordValue()
     */
    public function setRecordValue(ORM_Record $record, $mixedValue)
    {
        if ($mixedValue != $this->getRecordValue($record)) {
            $record->setPrimaryKeyModified(true);
        }
        parent::setRecordValue($record, $mixedValue);
    }
}




/**
 * Returns a field containing a record identifier.
 *
 * There must be at most one PkField in a Set.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_PkStringField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_PkStringField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_PkStringField($sName, $bReadOnly, $oCustomProperties);
}


class ORM_PkStringField extends ORM_PkField
{
    protected $auto_increment = false;
}




/**
 * Abstract base class for all numeric fields (ORM_IntField, ORM_CurrencyField...)
 */
abstract class ORM_NumericField extends ORM_Field
{
    private $_decimalSeparator = ',';

    private $_thousandsSeparator = null;

    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output($mixedValue)
     */
    public function output($mixedValue, $decimals = null)
    {
        if (!isset($decimals)) {
            $decimals = 2;
        }

        if (null === $this->_thousandsSeparator) {
            $_thousandsSeparator = bab_nbsp();
        } else {
            $_thousandsSeparator = $this->_thousandsSeparator;
        }

        return number_format((float)$mixedValue, $decimals, $this->_decimalSeparator, $_thousandsSeparator);
    }

    /**
     * Get empty value, when NULL is not allowed on field
     * @return string
     */
    protected function getEmptyValue()
    {
        return '0';
    }

    /**
     * Converts the human-readable $value into a suitable format for saving.
     *
     * @param mixed $mixedValue
     * @return mixed
     */
    public function input($mixedValue)
    {
        if (is_string($mixedValue)) {
            $number = str_replace(array(' ', bab_nbsp()), '', $mixedValue);
            return $number;
        }
        return $mixedValue;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * set the decimal separator
     * @param	string	$sep
     * @return self
     */
    public function setDecimalSeparator($sep)
    {
        $this->_decimalSeparator = $sep;
        return $this;
    }


    /**
     * Return the decimal separator.
     *
     * @since 0.11.15
     *
     * @return string
     */
    public function getDecimalSeparator()
    {
        return $this->_decimalSeparator;
    }


    /**
     * set the thousands separator
     * @param	string	$sep
     * @return self
     */
    public function setThousandsSeparator($sep)
    {
        $this->_thousandsSeparator = $sep;
        return $this;
    }


    /**
     * Return the thousands separator.
     *
     * @since 0.11.15
     *
     * @return string
     */
    public function getThousandsSeparator()
    {
        return $this->_thousandsSeparator;
    }

    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && '0' !== $mixedValue);
    }
}



/**
 * Returns a field containing an integer value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_IntField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_IntField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_IntField($sName, $bReadOnly, $oCustomProperties);
}

/**
 * A field containing an integer value.
 */
class ORM_IntField extends ORM_NumericField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }

    /**
     * (non-PHPdoc)
     * @see programs/ORM_NumericField#output($mixedValue)
     */
    public function output($mixedValue, $decimals = null)
    {
        if (!isset($decimals)) {
            $decimals = 0;
        }
        return parent::output($mixedValue, $decimals);
    }

    /**
     * Returs an correponding input widget
     *
     * @return Widget_RegExpLineEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->RegExpLineEdit()
                    ->setRegExp('^[0-9]+$')
                    ->setSize(5)
                    ->setName($this->getName());
    }
}









/**
 * Returns a field containing a user.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_UserField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_UserField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_UserField($sName, $bReadOnly, $oCustomProperties);
}

/**
 * A field containing a user.
 */
class ORM_UserField extends ORM_Field
{
    /**
     * get User infos from ovidentia database
     * @return array|false
     */
    public function getUserInfo($mixedValue)
    {
        if ((int)$mixedValue == 0 || (int)$mixedValue == $mixedValue || !is_numeric($mixedValue)) {
            return false;
        }
        return bab_userInfos::getRow($mixedValue);
    }

    /**
     * Output of a decimal field
     * @see programs/ORM_Field#output($mixedValue)
     */
    public function output($mixedValue)
    {
        return bab_getUserName($mixedValue, true);
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::formOutput()
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        return (int)$mixedValue;
    }

}



/**
 * Returns a field containing an ovidentia group.
 *
 * @since 0.9.10
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_GroupField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_GroupField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_GroupField($sName, $bReadOnly, $oCustomProperties);
}

/**
 * A field containing a group.
 *
 * @since 0.9.10
 */
class ORM_GroupField extends ORM_Field
{
    /**
     * Output of a decimal field
     * @see programs/ORM_Field#output($mixedValue)
     */
    public function output($mixedValue)
    {
        return bab_getGroupName($mixedValue);
    }
}

/**
 * ORM_BoolInterface
 *
 * @since 0.11.15
 */
interface ORM_BoolInterface
{
    public function getSanitizedValue($value, $database);

    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel);

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value);
}


/**
 * Returns a field containing an boolean value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_BoolField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_BoolField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_BoolField($sName, $bReadOnly, $oCustomProperties);
}



/**
 * A field containing an boolean value.
 */
class ORM_BoolField extends ORM_Field implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;


    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }

    /**
     * Converts the human-readable $value into a suitable format for saving.
     *
     * @param mixed $mixedValue
     */
    public function input($mixedValue)
    {
        if ($this->isNullAllowed() && is_null($mixedValue)) {
            return null;
        }
        if (empty($mixedValue)) {
            return 0;
        }
        return 1;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }

    /**
     * {@inheritDoc}
     * @see ORM_Field::getWidget()
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->CheckBox()
            ->setName($this->getName());
    }
}











/**
 * Returns a field containing an decimal value.
 *
 * @param 	string					$sName				The name of the field.
 * @param	int						$iAccuracy			number of decimals
 * @param 	bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param 	ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_DecimalField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_DecimalField($sName, $iAccuracy = 2, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_DecimalField($sName, $iAccuracy, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an currency value.
 */
class ORM_DecimalField extends ORM_NumericField
{
    private $precision = 10;

    private $decimalPlaces = 2;


    /**
     * @param string                $name               The name of the field.
     * @param int                   $decimalPlaces      Number of digits after the decimal point.
     * @param bool                  $readOnly           True if the field is read only, false otherwise.
     * @param ORM_CustomProperties  $customProperties   Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $name is not valid.
     */
    public function __construct($name, $decimalPlaces = 2, $readOnly = false, ORM_CustomProperties $customProperties = null)
    {
        parent::__construct($name, $readOnly, $customProperties);
        $this->setDecimalPlaces($decimalPlaces);
    }

    /**
     * Get the number of decimal places.
     *
     * @since 0.9.14
     * @return int
     */
    public function getDecimalPlaces()
    {
        return $this->decimalPlaces;
    }

    /**
     * @deprecated by getDecimalPlaces().
     * @return int
     */
    public function getDecimals()
    {
        return $this->getDecimalPlaces();
    }

    /**
     * Sets the number of decimal places.
     *
     * @since 0.9.14
     * @param int $decimalPlaces
     * @return self
     */
    public function setDecimalPlaces($decimalPlaces)
    {
        $this->decimalPlaces = $decimalPlaces;
        return $this;
    }

    /**
     * Get the total number of digits.
     *
     * @since 0.9.14
     * @return int
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * Sets the precision (total number of digits) of the decimal number, and optionally
     * the number of decimal digits.
     *
     * @since 0.9.14
     * @param int $precision
     * @param int $decimalPlaces
     * @return self
     */
    public function setPrecision($precision, $decimalPlaces = null)
    {
        $this->precision = $precision;
        if (isset($decimalPlaces)) {
            $this->setDecimalPlaces($decimalPlaces);
        }
        return $this;
    }

    /**
     * Returns a corresponding input widget
     *
     * @return Widget_RegExpLineEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->LineEdit()
//             ->setRegExp('^-?(0|([1-9]\d*))([\.\,]\d{1,'.$this->getDecimalPlaces().')?$')
            ->setName($this->getName());
    }


    /**
     * Output of a decimal field
     * @see programs/ORM_Field#output($mixedValue)
     */
    public function output($mixedValue, $decimals = null)
    {
        if (!isset($decimals)) {
            $decimals = $this->decimalPlaces;
        }
        return parent::output($mixedValue, $decimals);
    }

    /**
     * Output of a decimal field for a form input field value
     * @return string
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        return $this->output($mixedValue);
    }

    /**
     * Format the decimals of a value
     * @param mixed $value  A float or a string with . (no comma or space allowed)
     * @return string
     */
    private function getFormatted($value)
    {
        return sprintf('%01.'.$this->getDecimalPlaces().'f', round($value, $this->getDecimalPlaces()));
    }

    /**
     * Get empty value, when NULL is not allowed on field
     * @return string
     */
    protected function getEmptyValue()
    {
        return sprintf('%01.'.$this->getDecimalPlaces().'f', 0);
    }


    public function checkInput($mixedValue)
    {
        if ($this->isNullAllowed() && is_null($mixedValue)) {
            return null;
        }
        if (is_string($mixedValue)) {
            $mixedValue = str_replace(array(' ', chr(0xC2) . chr(0xA0), chr(160), ','), array('', '', '', '.'), $mixedValue);
        }
        if (is_numeric($mixedValue)) {
            return null;
        }

        return 'The value is not a valid number.';
    }

    /**
     * Input of a decimal field before saving
     * @param	mixed	$mixedValue
     * @return mixed
     */
    public function input($mixedValue)
    {
        if ($this->isNullAllowed() && is_null($mixedValue)) {
            return null;
        }
        if (!is_string($mixedValue)) {
            return $this->getFormatted($mixedValue);
        }

        if ('' === $mixedValue) {
             return $this->getEmptyValue();
        }

        $mixedValue = str_replace(array(' ', chr(0xC2) . chr(0xA0), chr(160), ','), array('', '', '', '.'), $mixedValue);
        if (is_numeric($mixedValue)) {
            return $this->getFormatted($mixedValue);
        }

        return $mixedValue;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }

    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && $this->getEmptyValue() !== $this->getFormatted($mixedValue));
    }
}










/**
 * Returns a field containing an currency (decimal) value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_CurrencyField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_CurrencyField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_CurrencyField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an currency value.
 */
class ORM_CurrencyField extends ORM_DecimalField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, 2, $bReadOnly, $oCustomProperties);
        $this->setPrecision(12, 2);
    }
}














/**
 * Returns a field containing an date value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_DateField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_DateField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_DateField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an date value.
 */
class ORM_DateField extends ORM_Field
{
    const EMPTY_DATE = '0000-00-00';

    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }

    /**
     * Returs an correponding input widget
     *
     * @return Widget_DatePicker
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->DatePicker()->setName($this->getName());
    }


    /**
     *
     */
    public function input($sDate)
    {
        if ($this->isNullAllowed() && is_null($sDate)) {
            return null;
        }

        $aElement = null;

        if (preg_match('/^(\d+)-(\d+)-(\d+)$/', $sDate, $aElement)) {
            if (strlen($aElement[1]) == 4) {
                $iYear	= (int)$aElement[1];
                $iMonth	= (int)$aElement[2];
                $iDay	= (int)$aElement[3];
            } else {
                $iYear	= (int)$aElement[3];
                $iMonth	= (int)$aElement[2];
                $iDay	= (int)$aElement[1];
                if ($iYear < 100) {
                    $iYear += 2000;
                }
            }
            return sprintf('%04d-%02d-%02d', $iYear, $iMonth, $iDay);
        }
        return self::EMPTY_DATE;
    }



    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output($mixedValue)
     *
     * @return string
     */
    public function output($mixedValue)
    {
        return bab_shortDate(bab_mktime($mixedValue), false);
    }


    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && self::EMPTY_DATE !== $mixedValue
            && ORM_DatetimeField::EMPTY_DATETIME !== $mixedValue
            && '' !== (string) $mixedValue);
    }
}


/**
 * Returns a field containing an time value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_TimeField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_TimeField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_TimeField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an time value.
 */
class ORM_TimeField extends ORM_Field
{
    const EMPTY_TIME = '00:00:00';

    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }


    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && self::EMPTY_TIME !== $mixedValue && '' !== (string) $mixedValue);
    }
}


/**
 * Returns a field containing an datetime value.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_DatetimeField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_DatetimeField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_DatetimeField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an datetime value.
 */
class ORM_DatetimeField extends ORM_Field
{
    const EMPTY_DATETIME = '0000-00-00 00:00:00';

    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }


    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output($mixedValue)
     *
     * @return string
     */
    public function output($mixedValue)
    {
        return bab_shortDate(bab_mktime($mixedValue), true);
    }

    /**
     * Returs an correponding input widget
     *
     * @return Widget_DateTimePicker
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->DateTimePicker()->setName($this->getName());
    }


    /**
     *
     */
    public function input($sDatetime)
    {
        if ($this->isNullAllowed() && is_null($sDatetime)) {
            return null;
        }
        if (strpos($sDatetime, ' ') === false) {
            $sDatetime .= ' 00:00:00';
        }

        $aElement = null;

        if (preg_match('/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)$/', $sDatetime, $aElement)) {
            if (strlen($aElement[1]) == 4) {
                $iYear		= (int)$aElement[1];
                $iMonth		= (int)$aElement[2];
                $iDay		= (int)$aElement[3];
                $iHour		= (int)$aElement[4];
                $iMinute	= (int)$aElement[5];
                $iSecond	= (int)$aElement[6];
            } else {
                $iYear		= (int)$aElement[3];
                $iMonth		= (int)$aElement[2];
                $iDay		= (int)$aElement[1];
                $iHour		= (int)$aElement[4];
                $iMinute	= (int)$aElement[5];
                $iSecond	= (int)$aElement[6];
                if ($iYear < 100) {
                    $iYear += 2000;
                }
            }
            $value = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $iYear, $iMonth, $iDay, $iHour, $iMinute, $iSecond);
            return $value;
        }

        if (preg_match('/^(\d+)-(\d+)-(\d+) (\d+):(\d+)$/', $sDatetime, $aElement)) {
            if (strlen($aElement[1]) == 4) {
                $iYear		= (int)$aElement[1];
                $iMonth		= (int)$aElement[2];
                $iDay		= (int)$aElement[3];
                $iHour		= (int)$aElement[4];
                $iMinute	= (int)$aElement[5];
                $iSecond	= 0;
            } else {
                $iYear		= (int)$aElement[3];
                $iMonth		= (int)$aElement[2];
                $iDay		= (int)$aElement[1];
                $iHour		= (int)$aElement[4];
                $iMinute	= (int)$aElement[5];
                $iSecond	= 0;
                if ($iYear < 100) {
                    $iYear += 2000;
                }
            }
            $value = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $iYear, $iMonth, $iDay, $iHour, $iMinute, $iSecond);
            return $value;
        }

        return self::EMPTY_DATETIME;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }


    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && self::EMPTY_DATETIME !== $mixedValue && ORM_DateField::EMPTY_DATE !== $mixedValue && '' !== (string) $mixedValue);
    }
}




/**
 * A field containing a textual data.
 */
abstract class ORM_TextualField extends ORM_Field
{
    /**
     * @param string $value
     */
    public function input($value)
    {
        if ($this->isNullAllowed() && is_null($value)) {
            return null;
        }

        $isAjaxRequest = (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

        if ($isAjaxRequest) {
            // Strings received from an ajax connection are always UTF-8 encoded.
            $value = bab_getStringAccordingToDataBase($value, bab_charset::UTF_8);
        } elseif (bab_charset::getIso() !== bab_charset::UTF_8) {
            $value = iconv(bab_charset::getIso(), bab_charset::UTF_8, $value);
            $value = html_entity_decode($value, ENT_QUOTES, bab_charset::UTF_8);
            $value = iconv(bab_charset::UTF_8, bab_charset::getIso() .'//TRANSLIT', $value);
        }
        return $value;
    }


    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        // Default behaviour for string fields when null not allowed
        return (isset($mixedValue) && '' !== (string) $mixedValue);
    }
}



/**
 * Returns a field containing a text string with a limited size.
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_StringField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_StringField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_StringField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text string with a limited size.
 */
class ORM_StringField extends ORM_TextualField
{
    private $iMaxLength;

    /**
     * @param string				$sName				The name of the field.
     * @param int					$iMaxLength			The maximum number of characters the field can contain.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
        $this->iMaxLength = $iMaxLength;
    }

    /**
     * Returns the maximum number of characters the field can contain.
     *
     * @return int
     */
    public function getMaxLength()
    {
        return $this->iMaxLength;
    }

    /**
     * Returs an correponding input widget
     *
     * @return Widget_LineEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        $pattern = $this->getCustomPropertyValue('pattern');

        if (isset($pattern)) {
            $lineEdit = $W->RegExpLineEdit();
            $lineEdit->setRegExp($pattern);
        } else {
            $lineEdit = $W->LineEdit();
        }

        $lineEdit->setMaxSize((int) $this->getMaxLength());
        $lineEdit->setName($this->getName());

        return $lineEdit;
    }
}




/**
 * Returns a field containing an URL.
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_UrlField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_UrlField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_UrlField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text string with a limited size.
 */
class ORM_UrlField extends ORM_StringField
{
}




/**
 * Returns a field containing an phone number.
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_PhoneField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_PhoneField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_PhoneField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a phone number.
 */
class ORM_PhoneField extends ORM_StringField
{
    /**
     * {@inheritDoc}
     * @see ORM_StringField::getWidget()
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->InternationalTelLineEdit()
            ->setPreferredCountries(array('fr'))
            ->setOption('initialCountry', 'fr')
            ->setOption('autoPlaceholder', 'off')
            ->setName($this->getName());
    }



    /**
     * {@inheritDoc}
     * @see ORM_Field::outputWidget()
     */
    public function outputWidget($phoneNumber)
    {
        $W = bab_Widgets();
        $PhoneNumber = bab_Functionality::get('PhoneNumber');
        if (!$PhoneNumber) {
            return $W->Link(
                $this->output($phoneNumber),
                'tel:' . $phoneNumber
            );
        }
        $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();
        try {
            $phoneNumberObject = $phoneNumberUtil->parse($phoneNumber, 'FR');
            $phoneNumber = $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::RFC3966);
        } catch (\libphonenumber\NumberParseException $e) {
            return $W->Link(
                $this->output($phoneNumber),
                'tel:' . $phoneNumber
            );
        }

        return $W->Link(
            $this->output($phoneNumber),
            $phoneNumber
        );
    }

    /**
     * {@inheritDoc}
     * @see ORM_TextualField::input()
     */
    public function input($phoneNumber)
    {
        if ($this->isNullAllowed() && is_null($phoneNumber)) {
            return null;
        }

        $PhoneNumber = bab_Functionality::get('PhoneNumber');
        if (!$PhoneNumber) {
            return $phoneNumber;
        }
        $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();

        try {
            $phoneNumberObject = $phoneNumberUtil->parse($phoneNumber, 'FR');
            $phoneNumber = $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::E164);
        } catch (\libphonenumber\NumberParseException $e) {
        }

        return $phoneNumber;

    }



    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output($mixedValue)
     *
     * @return string
     */
    public function output($phoneNumber)
    {
        $PhoneNumber = bab_Functionality::get('PhoneNumber');
        if (!$PhoneNumber) {
            return $phoneNumber;
        }
        $phoneNumberUtil = $PhoneNumber->PhoneNumberUtil();

        try {
            $phoneNumberObject = $phoneNumberUtil->parse($phoneNumber, 'FR');
            $regionCode = $phoneNumberUtil->getRegionCodeForNumber($phoneNumberObject);
            if ($regionCode === 'FR') {
                $format = \libphonenumber\PhoneNumberFormat::NATIONAL;
            } else {
                $format = \libphonenumber\PhoneNumberFormat::INTERNATIONAL;
            }
            //$format = $PhoneNumber->getDefaultFormat();
            $phoneNumber = $phoneNumberUtil->format($phoneNumberObject, $format);
        } catch (\libphonenumber\NumberParseException $e) {
        }

        return $phoneNumber;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::formOutput()
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        return $mixedValue;
    }

    /**
     * Test if a record value from the field is set
     * @param	mixed	$mixedValue
     * @return bool
     */
    public function isValueSet($mixedValue)
    {
        if ($this->isNullAllowed()) {
            return isset($mixedValue);
        }

        return (isset($mixedValue) && ORM_DateField::EMPTY_DATE !== $mixedValue
            && ORM_DatetimeField::EMPTY_DATETIME !== $mixedValue
            && '' !== (string) $mixedValue);
    }
}


/**
 * A field containing a RRULE icalendar attribute
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 */
function ORM_RruleField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_RruleField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}

/**
 * A field containing a RRULE icalendar attribute
 * This use the LibRrule addon
 */
class ORM_RruleField extends ORM_StringField
{

    /**
     * @return Func_Rrule
     */
    private function getFunctionality()
    {
        $rrule = bab_functionality::get('Rrule');
        if (!$rrule) {
            throw new ORM_Exception('The field ORM_RruleField require LibRrule addon');
        }

        return $rrule;
    }


    /**
     * Returns the value with a suitable form for an input field value.
     *
     * @param mixed $mixedValue
     * @return mixed
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        if ('' === $mixedValue || !isset($mixedValue)) {
            return array();
        }

        return $this->getFunctionality()->formOutput($mixedValue);
    }




    /**
     * RRULE is received from form as an array compatible with the Rrule class
     * @see Func_Rrule
     * @see ORM_StringField::input()
     */
    public function input($mixedValue)
    {
        if (is_string($mixedValue) || !isset($mixedValue)) {
            // rrule is selected from a select or direct input
            return $mixedValue;
        }

        return $this->getFunctionality()->formInput($mixedValue);
    }
}






/**
 * Returns a field containing an external file.
 *
 * @since 0.9.14
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_ImageField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_ImageField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_ImageField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an external file.
 *
 * @since 0.9.14
 */
class ORM_ImageField extends ORM_FileField
{
    private $associated = null;

    private $widget = null;


    /**
     * @param ORM_AreaField $associated
     * @return self
     */
    public function setAssociatedField($associated)
    {
        if(!$this->associated || $this->associated != $associated) {
            $this->associated = $associated;
            $this->getWidget()->setAssociatedCroper($this->associated->getWidget());
            $associated->setAssociatedField($this);
        }

        return $this;
    }

    /**
     * Returns a corresponding input widget
     *
     * @return Widget_ImagePicker
     */
    public function getWidget()
    {
        if (isset($this->widget)) {
            return $this->widget;
        }
        $W = bab_Widgets();
        $widget = $W->ImagePicker();
        $widget->setName($this->getName());
        $widget->oneFileMode(true);
        $this->widget = $widget;
        return $widget;
    }
}







/**
 * Returns a field containing an external file.
 *
 * @since 0.9.7
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_FileField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_FileField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_FileField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an external file.
 *
 * @since 0.9.7
 */
class ORM_FileField extends ORM_UrlField
{

    /**
     * Returns a corresponding input widget
     *
     * @return Widget_FilePicker
     */
    public function getWidget()
    {
        $W = bab_Widgets();
        $widget = $W->FilePicker();
        $widget->setName($this->getName());
        $widget->oneFileMode(true);

        return $widget;
    }



    /**
     * @param ORM_Record $record
     * @return bab_Path
     */
    public function getFolderPath(ORM_Record $record)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        $path = new bab_Path(bab_getAddonInfosInstance('LibOrm')->getUploadPath());
        $path->push($record->getParentSet()->getTableName());
        $path->push(parent::getName());


        return $path;
    }


    /**
     * @param ORM_Record $record
     * @param int $offset   The offset where the reading starts on the original stream.
     * @param int $maxlen   Maximum length of data read. The default is to read until end of file is reached.
     * @return bab_Path
     */
    public function getFilePath(ORM_Record $record, $offset = 0, $maxlen = 0)
    {
        $value = parent::getRecordValue($record);

        if ($value instanceof bab_Path) {
            return $value;
        }
        if ($value === '') {
            require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
            $path = new bab_Path('');

            return $path;
        }
        $filePath = $this->getFolderPath($record);
        $id = $record->getValue($record->getParentSet()->getPrimaryKey());
        $filePath->push($id);
        $filePath->push($value);

        return $filePath;
    }



    /**
     * Returns the value with a suitable form for an input field value.
     *
     * @param mixed $mixedValue
     * @return mixed
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        return $this->getFilePath($record);
    }


    /**
     * (non-PHPdoc)
     * @see ORM_StringField::input()
     */
    public function input($mixedValue)
    {

        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        if (isset($mixedValue['files'])) {

            $widget = $this->getWidget();
            $tmpPath = $widget->getTemporaryBasePath($widget->getName());
            $tmpPath->push($mixedValue['uid']);

            $mixedValue = $tmpPath;

            if ($files = $widget->getFolderFiles($tmpPath)) {
                foreach ($files as $file) {
                    return $file->getFilePath();
                }
            }
        } else if ($this->isNullAllowed()) {
            $mixedValue = null;
        } else {
            $mixedValue = '';
        }


        return $mixedValue;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Field::afterSave()
     */
    public function afterSave(ORM_Record $record)
    {
        $srcFile = parent::getRecordValue($record);

        $id = $record->getValue($record->getParentSet()->getPrimaryKey());
        $destPath = $this->getFolderPath($record);
        $destPath->push($id);


        if ($destPath->fileExists() && (!isset($srcFile) || ($srcFile instanceof bab_Path))) {
            // delete only if new upload or if no file uploaded
            // srcFile is a bab_Path if this is a new upload
            // srcFile is a string if the record is loaded from database
            try {
                $destPath->deleteDir();
            } catch (bab_FolderAccessRightsException $e) {
                bab_debug($e->getMessage());
            }
        }


        if ($srcFile instanceof bab_Path && $srcFile->isFile()) {
            $srcFile = $srcFile->toString();
            bab_debug('create dir '.$destPath->tostring());
            $destPath->createDir();
            parent::setRecordValue($record, basename($srcFile));
            return rename($srcFile, $destPath->getRealPath() . '/' . basename($srcFile));
        }

        return null;
    }
}




/**
 * Returns a field containing a text string with a limited size.
 *
 * @param string				$sName				The name of the field.
 * @param int					$iMaxLength			The maximum number of characters the field can contain.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_EmailField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_EmailField($sName, $iMaxLength = 255, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_EmailField($sName, $iMaxLength, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text string with a limited size.
 */
class ORM_EmailField extends ORM_StringField
{
    /**
     * {@inheritDoc}
     * @see ORM_Field::outputWidget()
     */
    public function outputWidget($email)
    {
        $W = bab_Widgets();

        return $W->Link(
            $this->output($email),
            'mailto:' . $email
        );
    }
}


/**
 * Returns a field containing a text of (almost) unlimited size.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_TextField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_TextField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_TextField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text of (almost) unlimited size.
 */
class ORM_TextField extends ORM_TextualField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }


    /**
     * Returs an correponding input widget
     *
     * @return Widget_TextEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->TextEdit()->setName($this->getName());
    }
}


/**
 * Returns a field containing a Html input of (almost) unlimited size.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_TextField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_HtmlField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_HtmlField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a Html input of (almost) unlimited size.
 */
class ORM_HtmlField extends ORM_TextField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }

    /**
     * @param string $value
     */
    public function input($value)
    {
        return parent::input($value);
    }


    /**
     * Returns the value with a suitable form for display.
     *
     * @param string $value
     * @return string
     */
    public function output($value)
    {
        return bab_toHtml(parent::output($value), BAB_HTML_REPLACE);
    }


    /**
     * Returs an correponding input widget
     *
     * @return Widget_BabHtmlEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        return $W->BabHtmlEdit()->setName($this->getName());
    }
}


/**
 * Returns a field containing an enum value.
 *
 * @param string				$sName				The name of the field.
 * @param array	 				$aValues			Enum values
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_EnumField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_EnumField($sName, $aValues, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_EnumField($sName, $aValues, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing an enum value.
 */
class ORM_EnumField extends ORM_Field
{
    private $aValues;

    /**
     * @param string				$sName				The name of the field.
     * @param array	 				$aValues			Enum values
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $aValues, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
        $this->aValues = $aValues;
    }

    /**
     *
     * @return array
     */
    public function getValues()
    {
        return $this->aValues;
    }


    /**
     * Returs an correponding input widget
     *
     * @return Widget_Select
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        $widget = $W->Select();

        if ($this->isNullAllowed()) {
            $widget->addOption('', '');
        }

        $values = $this->getValues();
        foreach ($values as $key => $value) {
            $widget->addOption($key, $value);
        }
        $widget->setName($this->getName());
        return $widget;
    }

    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output()
     *
     *
     * @return	string
     */
    public function output($mixedValue)
    {
        if (!isset($this->aValues[$mixedValue])) {
            return null;
        }

        return $this->aValues[$mixedValue];
    }
}




/**
 * Returns a field ccontaining a set of values.
 *
 * @param string				$sName				The name of the field.
 * @param array	 				$aValues			Set values
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_SetField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_SetField($sName, $aValues, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_SetField($sName, $aValues, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a set of values.
 */
class ORM_SetField extends ORM_Field
{
    private $aValues;

    /**
     * @param string				$sName				The name of the field.
     * @param array	 				$aValues			Set values
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $aValues, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
        $this->aValues = $aValues;
    }

    /**
     *
     * @return array
     */
    public function getValues()
    {
        return $this->aValues;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        if (is_array($value)) {
            return "('" . implode(',', $value) . "')";
        }
        return "('" . $value . "')";
    }



    /**
     * {@inheritDoc}
     * @see ORM_Field::formOutput()
     *
     * @return array
     */
    public function formOutput($mixedValue, ORM_Record $record = null)
    {
        if ($mixedValue === '') {
            return array();
        }
        $values = explode(',', $mixedValue);

        return $values;
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::output()
     *
     * @return string
     */
    public function output($mixedValue)
    {
        $values = $this->formOutput($mixedValue);
        $outputValues = array();
        foreach ($values as $value) {
            if (isset($this->aValues[$value])) {
                $outputValues[] = $this->aValues[$value];
            }
        }

        return implode(', ', $outputValues);
    }

    /**
     * Returs an correponding input widget
     *
     * @return Widget_Select
     */
    public function getWidget()
    {
        $W = bab_Widgets();

        $widget = $W->Multiselect();

        if ($this->isNullAllowed()) {
            $widget->addOption('', '');
        }

        $values = $this->getValues();
        foreach ($values as $key => $value) {
            $widget->addOption($key, $value);
        }
        $widget->setName($this->getName());
        return $widget;
    }
}




/**
 * Returns a field containing a reference to a record of another (foreign) Set.
 *
 * @param string				$name				    The name of the field.
 * @param string 				$foreignSetClassname	The name of the Set class this field is refering to.
 * @param bool					$readOnly			    True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$customProperties	    Allow the ORM_Field class to extends property.
 * @param string                $fieldClassName         The classname of the field to use. Allow different type of foreign key.
 *
 * @return ORM_FkField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_FkField($name, $foreignSetClassname, $readOnly = false, ORM_CustomProperties $customProperties = null, $fieldClassName = 'ORM_IntField')
{
    return new ORM_FkField($name, $foreignSetClassname, $readOnly, $customProperties, $fieldClassName);
}


/**
 * A field containing a reference to a record of another (foreign) Set.
 */
class ORM_FkField extends ORM_Field
{

    /**
     * Set used to select the relation
     * @see ORM_Record::__call()
     *
     * @var ORM_RecordSet
     */
    private $foreignSet = null;

    /**
     * Set class name used to describle the relation link to the set
     * @var string
     */
    private $foreignSetClassname = null;

    /**
     *
     * @var string
     */
    private $foreignKeyType = 'ORM_IntField';


    /**
     * Creates a new ORM_FkField object.
     *
     * @param string $name
     *            The name of the field.
     * @param string $foreignSetClassname
     *            The classname of the record set this field is refering to.
     * @param bool $readOnly
     *            True if the field is read only, false otherwise.
     * @param ORM_CustomProperties $customProperties
     *            Allows the ORM_Field class to extend property.
     * @param string $fieldClassName
     *            The classname of the field to use. Allow different type of foreign key.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct(
        $name,
        $foreignSetClassname,
        $readOnly = false,
        ORM_CustomProperties $customProperties = null,
        $fieldClassName = 'ORM_IntField'
    ) {
        parent::__construct($name, $readOnly, $customProperties);
        $this->foreignSetClassname = $foreignSetClassname;
        $this->setForeignKeyType($fieldClassName);
    }

    /**
     * Returns the classname of the record set this field is refering to.
     *
     * @return string
     */
    public function getForeignSetName()
    {
        return $this->foreignSetClassname;
    }

    /**
     * Set the recordSet used to select the relation
     * @see ORM_Record::__call()
     *
     * @param ORM_RecordSet $set
     *
     * @return self
     */
    public function setForeignSet(ORM_RecordSet $set)
    {
        $this->foreignSet = $set;
        $this->foreignSetClassname = get_class($set);
        return $this;
    }


    /**
     * get the recordSet to use for relation, create the recordSet if not exists
     * @throws ORM_Exception if the set class is not loaded
     *
     * @see ORM_Record::__call()
     * @return ORM_RecordSet
     */
    public function getForeignSet()
    {
        if (!isset($this->foreignSet)) {
            $this->foreignSet = $this->newSet();

            if (!isset($this->foreignSet)) {
                throw new ORM_Exception(sprintf('Failed to create SET instance from class %s', $this->getForeignSetName()));
            }
        }

        return $this->foreignSet;
    }


    /**
     * Instantiates an returns the record set referenced by the foreign key.
     *
     * @return ORM_RecordSet
     */
    public function newSet()
    {
        $className = $this->foreignSetClassname;

        if (!class_exists($className)) {
            return null;
        }

        $set = new $className();

        $set->setName($this->getName());
        $set->setDescription($this->getDescription());
        return $set;
    }

    /**
     * Returns the classname of the field to use as foreign key.
     * The default value is 'ORM_IntField'.
     *
     * @return string
     */
    public function getForeignKeyType()
    {
        return $this->foreignKeyType;
    }


    /**
     * Sets the classname of the field to use as foreign key.
     *
     * @param string $foreignKeyType
     *
     * @return self
     */
    public function setForeignKeyType($foreignKeyType)
    {
        $this->foreignKeyType = $foreignKeyType;
        return $this;
    }
}






/**
 * Returns a field containing a text string in several languages.
 *
 * @since 0.9.14
 *
 * @param string				$name				The name of the field.
 * @param bool					$readOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$customProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_AreaField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_AreaField($name,$readOnly = false, ORM_CustomProperties $customProperties = null)
{
    return new ORM_AreaField($name, $readOnly, $customProperties);
}


/**
 * A field containing a text string in several languages.
 *
 *  @since 0.9.14
 */
class ORM_AreaField extends ORM_TextField
{
    private $options = array(
        'x',//point x
        'y',//point y
        'width',//width
        'height',//height
        'rotate'//rotation
    );

    private $maxWidth = 0;
    private $minWidth = 0;

    private $maxHeight = 0;
    private $minHeight = 0;

    private $ratio = 0;

    private $associated = null;

    private $widget = null;

    const XML_ROOT_TAG_NAME = 'array';



    /**
     *
     * @param int   $minWidth
     * @param int   $minHeight
     * @return self
     */
    public function setMinSize($minWidth, $minHeight)
    {
        $this->minWidth = $minWidth;
        $this->minHeight = $minHeight;
        return $this;
    }


    /**
     * @param float $ratio
     * @return self
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
        return $this;
    }


    /**
     * @param ORM_ImageField    $associated
     * @return self
     */
    public function setAssociatedField($associated)
    {
        if(!$this->associated || $this->associated != $associated) {
            $this->associated = $associated;
            $associated->setAssociatedField($this);
        }

        return $this;
    }


    /**
     * Returns a corresponding input widget
     *
     * @return Widget_ImageCropper
     */
    public function getWidget()
    {
        if($this->widget) {
            return $this->widget;
        }
        $minWidthSize = 640;
        $minHeightSize = 480;

        $W = bab_Widgets();

        $widget = $W->ImageCropper();
        $widget->setVisibleLayout(false);
        $widget->setMinSize($minWidthSize, $minHeightSize);
        if($this->minWidth && $this->minHeight) {
            $widget->setMinSelectedSize($this->minWidth, $this->minHeight);
        }
        if($this->ratio) {
            $widget->setRatio($this->ratio);
        }
        $widget->setName($this->getName());
        $this->widget = $widget;

        return $widget;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Field::formOutput()
     */
    public function formOutput($value, ORM_Record $record = NULL)
    {
        $result = array();
        if (!empty($value)) {
            $strings = new SimpleXMLElement("<?xml version='1.0' encoding='" . bab_charset::getIso() . "' ?>\n" . $value);
        } else {
            $strings = new stdClass();
        }

        foreach ($this->options as $option) {
            if (isset($strings->{$option})) {
                $result[$option] = bab_getStringAccordingToDataBase($strings->{$option}, bab_charset::UTF_8);
            } else {
                $result[$option] = null;
            }
        }

        return $result;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_TextField::input()
     */
    public function input($value)
    {
        if ($this->isNullAllowed() && is_null($value)) {
            return null;
        }

        $xml = '<' . self::XML_ROOT_TAG_NAME . '>';
        foreach ($this->options as $option) {
            if (!isset($value[$option])) {
                continue;
            }
            $xml .= '<' . $option .'><![CDATA[' . parent::input($value[$option]) . ']]></' . $option .'>';
        }
        $xml .= '</' . self::XML_ROOT_TAG_NAME . '>';
        return $xml;
    }

    /**
     *
     * @param string $option
     * @return ORM_ExtractValueOperation
     */
    public function getAreaField($option = null)
    {
        $field = new ORM_ExtractValueOperation($this, '/' . self::XML_ROOT_TAG_NAME . '/' . $option);
//        $field = new ORM_ReplaceOperation($field, '&amp;', '&');
        $field->setName($this->getName() . '_' . $option);
        return $field;
    }


    /**
     * @return void
     */
    public function extractAreaFields()
    {
        $parentSet = $this->getParentSet();
        foreach ($this->options as $option) {
            $field = $this->getAreaField($option);
            $parentSet->addFields($field);
        }
    }


    /**
     * @param string $option  The option code 'x', 'y', 'width', 'height', 'rotate'
     *                          If null or not specified, the current language as returned by bab_Language() is used.
     * @return ORM_ExtractValueOperation
     */
    public function extractArea($option)
    {
        $field = new ORM_ExtractValueOperation($this, '/' . self::XML_ROOT_TAG_NAME . '/' . $option);
        $field->setDescription($this->getDescription());
        return $field;
    }
}






/**
 * Returns a field containing a text string in several languages.
 *
 * @since 0.9.4
 *
 * @param string				$name				The name of the field.
 * @param bool					$readOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$customProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_MultilangTextField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_MultilangTextField($name,$readOnly = false, ORM_CustomProperties $customProperties = null)
{
    return new ORM_MultilangTextField($name, $readOnly, $customProperties);
}


/**
 * A field containing a text string in several languages.
 *
 *  @since 0.9.4
 */
class ORM_MultilangTextField extends ORM_TextField
{
    private $languages = array();

    const XML_ROOT_TAG_NAME = 'array';

    /**
     * Sets the possible languages for this field.
     *
     * @param string[]  $languages
     * @return self
     */
    public function setLanguages(array $languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * Removes illegal xml characters from an xml string.
     *
     * @param string $xml
     * @return string
     */
    static private function sanitizeXml($xml)
    {
        static $pattern = null;
        if (!isset($pattern)) {
            if (bab_charset::getIso() === bab_charset::UTF_8) {
                $pattern = '/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u';
            } else {
                $pattern = '/[^\x09\x0a\x0d\x20-\xFF]+/';
            }
        }

        $xml  = preg_replace($pattern, '', $xml);

        return $xml;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Field::formOutput()
     */
    public function formOutput($value, ORM_Record $record = NULL)
    {
        $result = array();
        $value = self::sanitizeXml($value);
        if (!empty($value)) {
            $strings = new SimpleXMLElement("<?xml version='1.0' encoding='" . bab_charset::getIso() . "' ?>\n" . $value);
        } else {
            $strings = new stdClass();
        }

        foreach ($this->languages as $language) {
            if (isset($strings->{$language})) {
                $result[$language] = bab_getStringAccordingToDataBase($strings->{$language}, bab_charset::UTF_8);
            } else {
                $result[$language] = null;
            }
        }

        return $result;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_TextField::input()
     */
    public function input($value)
    {
        if ($this->isNullAllowed() && is_null($value)) {
            return null;
        }

        $xml = '<' . self::XML_ROOT_TAG_NAME . '>';
        foreach ($this->languages as $language) {
            if (!isset($value[$language])) {
                continue;
            }
            $xml .= '<' . $language .'><![CDATA[' . self::sanitizeXml(parent::input($value[$language])) . ']]></' . $language .'>';
        }
        $xml .= '</' . self::XML_ROOT_TAG_NAME . '>';
        return $xml;
    }

    /**
     *
     * @param string $language
     * @return ORM_ExtractValueOperation
     */
    public function getLangField($language = null)
    {
        $field = new ORM_ExtractValueOperation('', $this, '/' . self::XML_ROOT_TAG_NAME . '/' . $language);
//        $field = new ORM_ReplaceOperation($field, '&amp;', '&');
        $field->setName($this->getName() . '_' . $language);
        return $field;
    }

    /**
     * @return void
     */
    public function extractLangFields()
    {
        $parentSet = $this->getParentSet();
        foreach ($this->languages as $language) {
            $field = $this->getLangField($language);
            $parentSet->addFields($field);
        }
    }


    /**
     * Sets the language of the field.
     * Once used the field value will be the string in the selected language.
     *
     * @param string $language  The language code ('en', 'fr'...).
     *                          If null or not specified, the current language as returned by bab_Language() is used.
     * @return ORM_ExtractValueOperation
     */
    public function extractLang($language = null)
    {
        if (!isset($language)) {
            $language = bab_getLanguage();
        }
        $field = new ORM_ExtractValueOperation('', $this, '/' . self::XML_ROOT_TAG_NAME . '/' . $language);
        $field->setDescription($this->getDescription());
        return $field;
    }

    /**
     * Sets the language of the field.
     * Once used the field value will be the string in the selected language.
     *
     * @param string $language  The language code ('en', 'fr'...).
     *                          If null or not specified, the current language as returned by bab_Language() is used.
     * @return self
     */
    public function useLang($language = null)
    {
        if ($language === false) {
            return $this;
        }
        $field = $this->extractLang($language);
        $parentSet = $this->getParentSet();
        $field->setName($this->getName());
        $parentSet->addFields($field);
        return $this;
    }
}


/**
 * Returns a field containing a sitemap node.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_SitemapItemField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_SitemapItemField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_SitemapItemField($sName, 255, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a sitemap node value.
 */
class ORM_SitemapItemField extends ORM_StringField
{
    /**
     * Returns an correponding input widget
     *
     * @return Widget_Select
     */
    public function getWidget()
    {
        $W = bab_Widgets();
        $widget = $W->SitemapItemPicker();
        $widget->setName($this->getName());
        $widget->setMaxSize((int) $this->getMaxLength());
        return $widget;
    }
}

/**
 * Returns a field containing a text of (almost) unlimited size.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_MediumTextField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_MediumTextField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_MediumTextField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text of (almost) unlimited size.
 */
class ORM_MediumTextField extends ORM_TextField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }
    
    
    /**
     * Returs an correponding input widget
     *
     * @return Widget_TextEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();
        
        return $W->TextEdit()->setName($this->getName());
    }
}

/**
 * Returns a field containing a text of (almost) unlimited size.
 *
 * @param string				$sName				The name of the field.
 * @param bool					$bReadOnly			True if the field is read only, false otherwise.
 * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
 *
 * @return ORM_LongTextField
 *
 * @throws ORM_IllegalArgumentException if $sName is not valid.
 */
function ORM_LongTextField($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
{
    return new ORM_LongTextField($sName, $bReadOnly, $oCustomProperties);
}


/**
 * A field containing a text of (almost) unlimited size.
 */
class ORM_LongTextField extends ORM_TextField
{
    /**
     * @param string				$sName				The name of the field.
     * @param bool					$bReadOnly			True if the field is read only, false otherwise.
     * @param ORM_CustomProperties	$oCustomProperties	Allow the ORM_Field class to extends property.
     *
     * @throws ORM_IllegalArgumentException if $sName is not valid.
     */
    public function __construct($sName, $bReadOnly = false, ORM_CustomProperties $oCustomProperties = null)
    {
        parent::__construct($sName, $bReadOnly, $oCustomProperties);
    }
    
    
    /**
     * Returs an correponding input widget
     *
     * @return Widget_TextEdit
     */
    public function getWidget()
    {
        $W = bab_Widgets();
        
        return $W->TextEdit()->setName($this->getName());
    }
}


//------ Operation test

/**
 * Operations abstract class
 */
abstract class ORM_Operation extends ORM_Field
{
    /**
     * Value on which the operation is applied.
     *
     * @var mixed
     */
    protected $mixedValue = null;


//     /**
//      * Remove parameters from the parent constructor
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

    /**
     * @param mixed $mixedValue
     */
    public function setValue($mixedValue)
    {
        $this->mixedValue = $mixedValue;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->mixedValue;
    }

    /**
     * Get backend result
     * @param ORM_BackEnd $oBackEnd
     *
     * @return string
     */
    abstract public function toString(ORM_BackEnd $oBackEnd);
}

/**
 * Abstract for binary operations (OR, AND)
 */
abstract class ORM_BinaryOperation extends ORM_Operation
{
    private $rightValue = null;

    /**
     * @param string    $name
     * @param mixed     $leftValue
     * @param mixed     $rightValue
     */
    public function __construct($name, $leftValue = null, $rightValue = null)
    {
        parent::__construct($name);
        $this->setValue($leftValue);
        $this->rightValue = $rightValue;
    }

    /**
     * @return mixed
     */
    public function getRightValue()
    {
        return $this->rightValue;
    }
}



/**
 * ORM_NotOperation
 *
 * @since 0.11.15
 */
class ORM_NotOperation extends ORM_Operation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }

    /**
     * Creates a new ORM_NotOperation.
     *
     * @param mixed $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->notOperation($this);
    }
}


/**
 * ORM_OrOperation
 *
 * @since 0.11.15
 */
class ORM_OrOperation extends ORM_BinaryOperation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->orOperation($this);
    }
}



/**
 * ORM_AndOperation
 *
 * @since 0.11.15
 */
class ORM_AndOperation extends ORM_BinaryOperation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->andOperation($this);
    }
}


/**
 * ORM_EqualsOperation
 *
 * @since 0.11.15
 */
class ORM_EqualsOperation extends ORM_BinaryOperation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->equalsOperation($this);
    }
}



/**
 * ORM_InOperation
 *
 * @since 0.11.15
 */
class ORM_InOperation extends ORM_BinaryOperation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }



    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->inOperation($this);
    }
}



/**
 * ORM_IfEqualsOperation
 *
 * @since 0.11.15
 */
class ORM_IfEqualsOperation extends ORM_Operation
{
    private $expression;

    private $trueValue;

    private $falseValue;


     /**
      * Creates a new ORM_IfEqualsOperation.
      *
      * @param string   $name
      * @param mixed    $mixedValue
      * @param mixed    $expression
      * @param mixed    $valueTrue
      * @param mixed    $valueFalse
      */
    public function __construct($name, $mixedValue, $expression, $valueTrue, $valueFalse)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
        $this->expression = $expression;
        $this->trueValue = $valueTrue;
        $this->falseValue = $valueFalse;
    }

    public function getExpression()
    {
        return $this->expression;
    }

    public function getTrueValue()
    {
        return $this->trueValue;
    }


    public function getFalseValue()
    {
        return $this->falseValue;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->ifEqualsOperation($this);
    }
}



/**
 * Returns a boolean operation based on a criterion.
 *
 * @param string            $name          The name of the operation.
 * @param ORM_Criteria      $criterion  number of decimals
 *
 * @since 0.11.12
 *
 * @return ORM_CriterionOperation
 */
function ORM_CriterionOperation($name, ORM_Criteria $criterion)
{
    return new ORM_CriterionOperation($name, $criterion);
}


/**
 * ORM_CriterionOperation
 *
 * @since 0.11.12
 */
class ORM_CriterionOperation extends ORM_Operation implements ORM_BoolInterface
{
    /**
     * @var string
     */
    private $falseLabel = null;

    /**
     * @var string
     */
    private $trueLabel = null;

    /**
     * {@inheritDoc}
     * @see ORM_Field::getSanitizedValue()
     */
    public function getSanitizedValue($value, $database)
    {
        return $database->quote($this->input($value));
    }


    /**
     * Set values used by the output boolean field method
     * @param string $falseLabel
     * @param string $trueLabel
     *
     * @return self
     */
    public function setOutputOptions($falseLabel, $trueLabel)
    {
        $this->falseLabel = $falseLabel;
        $this->trueLabel = $trueLabel;

        return $this;
    }

    /**
     * Get value to display
     *
     * @param bool $value
     * @return string
     */
    public function output($value)
    {
        if ($value) {
            if (! isset($this->trueLabel)) {
                return parent::output($value);
            }
            return $this->trueLabel;
        }

        if (! isset($this->falseLabel)) {
            return parent::output($value);
        }
        return $this->falseLabel;
    }


    /**
     *
     * @param string $name
     * @param ORM_Criteria $criterion
     */
    public function __construct($name, ORM_Criteria $criterion)
    {
        parent::__construct($name);
        $this->setValue($criterion);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->criterionOperation($this);
    }
}


class ORM_CountOperation extends ORM_Operation
{
    private $distinct = false;

    /**
     * Creates a new ORM_CountOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     * @param bool      $distinct   True to eliminate duplicate values.
     */
    public function __construct($name, $mixedValue, $distinct = null)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
        if (isset($distinct)) {
            $this->setDistinct($distinct);
        }
    }

    /**
     * @return boolean
     */
    public function getDistinct()
    {
        return $this->distinct;
    }

    /**
     * @param boolean $distinct True to eliminate duplicate values.
     * @return self
     */
    public function setDistinct($distinct = true)
    {
        $this->distinct = $distinct;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->countOperation($this);
    }
}



class ORM_MaxOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_MaxOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->maxOperation($this);
    }
}



class ORM_MinOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_MinOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->minOperation($this);
    }
}


class ORM_SumOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_SumOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->sumOperation($this);
    }


    /**
     * (non-PHPdoc)
     * @see programs/ORM_Field#output($mixedValue)
     */
    public function output($mixedValue, $decimals = null)
    {
        $value = $this->getValue();
        if ($value instanceof ORM_Field) {
            return $value->output($mixedValue, $decimals);
        }
        return parent::output($mixedValue, $decimals);
    }
}


class ORM_AverageOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_AverageOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->averageOperation($this);
    }
}



// Text string operations.
//////////////////////////

class ORM_LengthOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_LengthOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue = '')
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->lengthOperation($this);
    }
}


class ORM_ToUpperOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_ToUpperOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue = '')
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->toUpperOperation($this);
    }
}


class ORM_ToLowerOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_ToLowerOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue = '')
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->toLowerOperation($this);
    }
}


class ORM_LeftOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->leftOperation($this);
    }
}


class ORM_RightOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->rightOperation($this);
    }
}


class ORM_LeftPadOperation extends ORM_Operation
{
    public $str;
    public $length;
    public $padStr;

    /**
     * Creates a new ORM_LeftPadOperation.
     *
     * @param string                    $name
     * @param string|ORM_StringField    $str
     * @param int|ORM_NumericField      $length
     * @param string|ORM_StringField    $padStr
     */
    public function __construct($name, $str, $length, $padStr)
    {
        parent::__construct($name);

        $this->str = $str;
        $this->length = $length;
        $this->padStr = $padStr;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->leftPadOperation($this);
    }
}


class ORM_ConcatOperation extends ORM_Operation
{
    /**
     * Creates a new ORM_ConcatOperation.
     *
     * Takes a variable number of arguments.
     */
    public function __construct($name)
    {
        parent::__construct($name);

        $this->mixedValue = new ArrayIterator(array());

        $aArgList = func_get_args();
        $this->initFromArray($aArgList);
    }

    /**
     * Intilializes the values from an array.
     * Recursive.
     *
     * @param array $values
     */
    private function initFromArray(array $values)
    {
        foreach ($values as $mixedValue) {
            if (is_array($mixedValue)) {
                $this->initFromArray($mixedValue);
            } else {
                $this->mixedValue->append($mixedValue);
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->concatOperation($this);
    }
}


class ORM_GroupConcatOperation extends ORM_Operation
{
    private $separator = ' ';
    private $distinct = false;

     /**
     * Creates a new ORM_GroupConcatOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     * @param string    $separator
     * @param boolean   $distinct   True to eliminate duplicate values.
     */
    public function __construct($name, $mixedValue, $separator = null, $distinct = null)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
        if (isset($separator)) {
            $this->setSeparator($separator);
        }
        if (isset($distinct)) {
            $this->setDistinct($distinct);
        }
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     * @return self
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDistinct()
    {
        return $this->distinct;
    }

    /**
     * @param boolean $distinct True to eliminate duplicate values.
     * @return self
     */
    public function setDistinct($distinct = true)
    {
        $this->distinct = $distinct;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->groupConcatOperation($this);
    }
}



/**
 * Trim operation
 */
class ORM_TrimOperation extends ORM_Operation
{

    const BOTH      = 1;
    const LEADING   = 2;
    const TRAILING  = 3;

    /**
     * The string to be removed.
     *
     * @var string
     */
    private $remstr = null;

    /**
     * @var int
     */
    private $where = null;

    /**
     * Creates a new ORM_TrimOperation.
     *
     * @param string                    $name
     * @param string|ORM_StringField    $mixedValue
     * @param string                    $remstr            The string to be removed. If not specified remove spaces.
     * @param int                       $where             Specifies where to trim using one of:
     *                                                          ORM_TrimOperation::BOTH,
     *                                                          ORM_TrimOperation::LEADING,
     *                                                          ORM_TrimOperation::TRAILING
     */
    public function __construct($name, $mixedValue, $remstr = ' ', $where = ORM_TrimOperation::BOTH)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);

        $this->remstr = $remstr;
        $this->where = $where;
    }

    /**
     * String to be removed
     * @return string
     */
    public function getRemoveStr()
    {
        return $this->remstr;
    }

    /**
     * One of the fllowing values:
     *            ORM_TrimOperation::BOTH,
     *            ORM_TrimOperation::LEADING,
     *            ORM_TrimOperation::TRAILING
     * @return int
     */
    public function getWhere()
    {
        return $this->where;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->trimOperation($this);
    }
}




/**
 * Replace operation
 */
class ORM_ReplaceOperation extends ORM_Operation
{

    /**
     * @var string
     */
    private $fromStr = null;

    /**
     * @var string
     */
    private $toStr = null;

    /**
     * Creates a new ORM_ReplaceOperation.
     *
     * @param string                    $name
     * @param string|ORM_StringField    $mixedValue
     * @param string                    $fromStr
     * @param string                    $toStr
     */
    public function __construct($name, $mixedValue, $fromStr, $toStr)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);

        $this->fromStr = $fromStr;
        $this->toStr = $toStr;
    }

    /**
     * @return string
     */
    public function getFromStr()
    {
        return $this->fromStr;
    }

    /**
     * @return string
     */
    public function getToStr()
    {
        return $this->toStr;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->replaceOperation($this);
    }
}



class ORM_FindOperation extends ORM_Operation
{
    /**
     * @var string
     */
    private $key;

    /**
     * Creates a new ORM_FindOperation.
     *
     * @param string        $name
     * @param ORM_Field     $field
     * @param string        $key
     */
    public function __construct($name, ORM_Field $field, $key)
    {
        parent::__construct($name);
        $this->setValue($field);

        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKeyValue()
    {
        return $this->key;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->findOperation($this);
    }
}



class ORM_FindInSetOperation extends ORM_Operation
{
    /**
     * @var array
     */
    private $list;

    /**
     * Creates a new ORM_FindInSetOperation.
     *
     * @param string        $name
     * @param ORM_Field     $field
     * @param array         $list
     */
    public function __construct($name, ORM_Field $field, array $list)
    {
        parent::__construct($name);
        $this->setValue($field);

        $this->list = $list;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->findInSetOperation($this);
    }
}




class ORM_FieldOperation extends ORM_Operation
{
    /**
     * @var array
     */
    private $list;

    /**
     * Creates a new ORM_FieldOperation.
     *
     * @param string        $name
     * @param ORM_Field     $field
     * @param array         $list
     */
    public function __construct($name, ORM_Field $field, $list)
    {
        parent::__construct($name);
        $this->setValue($field);

        $this->list = $list;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->fieldOperation($this);
    }
}



// Arithmetic operations.
/////////////////////////


/**
 * Returns the argument X, converted from degrees to radians.
 */
class ORM_RadiansOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_RadiansOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->radiansOperation($this);
    }
}


/**
 * Returns the cosine of X, where X is given in radians.
 */
class ORM_CosOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_CosOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->cosOperation($this);
    }
}


/**
 * Returns the arc cosine of X, that is, the value whose cosine is X. Returns NULL if X is not in the range -1 to 1.
 *
 */
class ORM_AcosOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_AcosOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->acosOperation($this);
    }
}

/**
 * Returns the sine of X, where X is given in radians.
 */
class ORM_SinOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_SinOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->sinOperation($this);
    }
}


/**
 * Returns the arc sine of X, that is, the value whose sine is X. Returns NULL if X is not in the range -1 to 1
 */
class ORM_AsinOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_AsinOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->asinOperation($this);
    }
}

/**
 * Returns the tangent of X, where X is given in radians.
 */
class ORM_TanOperation extends ORM_Operation
{
    /**
     * Creates a new ORM_TanOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->tanOperation($this);
    }
}

/**
 * Returns the arc tangent of the two variables
 */
class ORM_AtanOperation extends ORM_Operation
{
    /**
     * Creates a new ORM_AtanOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->atanOperation($this);
    }
}


class ORM_RoundOperation extends ORM_Operation
{

    /**
     * The number of decimal digits.
     * @var int
     */
    private $decimals = 0;

    /**
     * Creates a new ORM_RoundOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     * @param int       $decimal        Specifies the number of decimal digits to round to.
     */
    public function __construct($name, $mixedValue, $decimals = 0)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);

        $this->decimals = $decimals;
    }

    /**
     * Get the number of decimal digits to round to.
     *
     * @return int
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->roundOperation($this);
    }
}


class ORM_AbsOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_AbsOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->absOperation($this);
    }
}


class ORM_StrCmpOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->strCmpOperation($this);
    }
}



class ORM_AddOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->addOperation($this);
    }
}


class ORM_SubtractOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->subtractOperation($this);
    }
}


class ORM_MultiplyOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->multiplyOperation($this);
    }
}


class ORM_DivideOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->divideOperation($this);
    }
}




/// Date and time operations.
/////////////////////////////


class ORM_DateDiffOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->dateDiffOperation($this);
    }
}

class ORM_TimeDiffOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->timeDiffOperation($this);
    }
}


/**
 * @since 0.9.7
 */
class ORM_WeekOperation extends ORM_Operation
{
    /**
     * Creates a new ORM_WeekOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->weekOperation($this);
    }
}



class ORM_IsNullOperation extends ORM_Operation
{
     /**
     * Creates a new ORM_IsNullOperation.
     *
     * @param string    $name
     * @param mixed     $mixedValue
     */
    public function __construct($name, $mixedValue)
    {
        parent::__construct($name);
        $this->setValue($mixedValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->isNullOperation($this);
    }
}



class ORM_IfNullOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->ifNullOperation($this);
    }
}



class ORM_RandOperation extends ORM_Operation
{
    /**
     * Creates a new ORM_RandOperation.
     *
     * @param string    $name
     * @param int|null  $seed      The optional seed value of the random sequence
     */
    public function __construct($name, $seed = null)
    {
        parent::__construct($name);
        $this->setValue($seed);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->randOperation($this);
    }
}




/// XML manipulation operations.
////////////////////////////////

/**
 * @since 0.9.4
 */
 class ORM_ExtractValueOperation extends ORM_BinaryOperation
{
    /**
     * (non-PHPdoc)
     * @see ORM_Operation::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->extractValueOperation($this);
    }


    /**
     * {@inheritDoc}
     * @see ORM_Field::useLang()
     */
    public function useLang($language = null)
    {
        if ($language === false) {
            $parentSet = $this->getParentSet();
            $field = $this->getValue();
            $field->setName($this->getName());
            $parentSet->addFields($field);
        }
        return $this;
    }

}





/**
 * Returns a criteria matching all the conditions.
 *
 * You should probably use ORM_Field::all() instead.
 *
 * @since 0.9.5
 *
 * @see ORM_Field::all()
 *
 * @param ORM_Criteria|ORM_Criteria[]   $conditions
 * @param ORM_Field|null                $field (optional)
 *
 * @return ORM_Criteria
 */
function ORM_And($conditions, ORM_Field $field = null)
{
    if ($conditions instanceof ORM_Criteria) {
        return $conditions;
    }
    if (count($conditions) === 0) {
        return new ORM_TrueCriterion($field);
    }
    $criteria = array_shift($conditions);
    $criteria = ORM_And($criteria, $field);
    foreach ($conditions as $condition) {
        $condition = ORM_And($condition, $field);
        $criteria = $criteria->_AND_($condition);
    }
    return $criteria;
}


/**
 * Returns a criteria matching at least one of the conditions.
 *
 * You should probably use ORM_Field::any() instead.
 *
 * @since 0.9.5
 *
 * @see ORM_Field::any()
 *
 * @param ORM_Criteria|ORM_Criteria[]   $conditions
 * @param ORM_Field|null                $field (optional)
 * @return ORM_Criteria
 */
function ORM_Or($conditions, ORM_Field $field = null)
{
    if ($conditions instanceof ORM_Criteria) {
        return $conditions;
    }
    if (count($conditions) === 0) {
        return new ORM_TrueCriterion($field);
    }
    $criteria = array_shift($conditions);
    $criteria = ORM_Or($criteria, $field);
    foreach ($conditions as $condition) {
        $condition = ORM_Or($condition, $field);
        $criteria = $criteria->_OR_($condition);
    }
    return $criteria;
}
