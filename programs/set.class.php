<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/backend.class.php';
require_once dirname(__FILE__) . '/field.class.php';
require_once dirname(__FILE__) . '/criteria.class.php';
require_once dirname(__FILE__) . '/record.class.php';








/**
 * ORM_RecordSet is used as factory to create, delete and retrieve ORM_Records.
 *
 * @category   Addons
 * @package    Libraries
 * @subpackage ORM
 * @author     Samuel Zebina <samuel.zebina@cantico.fr>
 * @author     Laurent Choulette <laurent.choulette@cantico.fr>
 * @copyright  2008 by CANTICO ({@link http://www.cantico.fr})
 * @license    http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 */
class ORM_RecordSet extends ORM_Field
{
    const SUFFIX = 'Set';

    protected $sRecordClassName = null;

    /**
     *
     * @var ORM_PkField
     */
    protected $primaryKey = null;

    /**
     *
     * @var string
     */
    protected $sPrimaryKeyName = null;


    protected $aField = array();

    protected $defaultCriteria = null;

    /**
     * The default backend associated to new recordSets
     * @var ORM_Backend
     */
    protected static $defaultBackend = null;


    /**
     * The backend associated to this specific recordSet
     * @var ORM_Backend
     */
    private $backend = null;

    /**
     * The backend table name associated to this recordset instance
     * @var string
     * @since 0.11.2
     */
    public $backendTableName = null;


    /**
     * List of keys in set
     * @var array
     */
    protected $fieldKeys        = array();


    /**
     * Constructs a new ORM_RecordSet.
     *
     * @param string $sName field name.
     */
    public function __construct($sName = '')
    {
        parent::__construct($sName);
        $this->computeRecordClassName();
    }


    /**
     * This method compute the record class name associated to this set
     * Override this method to compute your own.
     *
     * @return void
     */
    protected function computeRecordClassName()
    {
        $sSetClassName			= get_class($this);
        $this->sRecordClassName = substr($sSetClassName, 0, -strlen(ORM_RecordSet::SUFFIX));
    }


    /**
     * Returns the computed record classname associated to this RecordSet.
     *
     * @return string
     */
    public function getRecordClassName()
    {
        return $this->sRecordClassName;
    }


    /**
     * Set a table name associated to the set
     * @param string $name
     *
     */
    public function setTableName($name)
    {
        $backend = $this->getBackend();
        $backend->setTableName($this, $name);
    }

    /**
     * Get the table name associated to the set
     *
     * @return	string	The table name associated to the set
     */
    public function getTableName()
    {
        $backend = $this->getBackend();
        return $backend->getTableName($this);
    }

    /**
     * Returns a new instance of the element class associated to the Set.
     *
     * @return ORM_Record
     */
    public function newRecord()
    {
        $sRecordClassName = $this->getRecordClassName();
        return new $sRecordClassName($this);
    }

    /**
     * Checks whether the specified field exists.
     *
     * @param string $fieldName The name of the field to check.
     *
     * @return bool
     */
    public function __isset($fieldName)
    {
        return array_key_exists($fieldName, $this->aField);
    }


    /**
     * Returns the specified field.
     *
     * @param string $sFieldName The name of the field to return.
     *
     * @return ORM_Field
     */
    public function __get($sFieldName)
    {
        $oField = $this->getField($sFieldName);
        //		assert('!is_null($oField) /* The field "' . $sFieldName . '" does not exist. */');
        return $oField;
    }


    /**
     * This defines a default criteria that will be applied to all select()
     * or joined access to this table.
     *
     * @param ORM_Criteria $criteria The default criteria that will be applied to all subsequent select().
     *
     * @return void
     */
    public function setDefaultCriteria(ORM_Criteria $criteria = null)
    {
        $this->defaultCriteria = $criteria;
    }


    /**
     * Returns the default criteria that will be applied to all select()
     * or joined access to this table.
     *
     * @return ORM_Criteria
     */
    public function getDefaultCriteria()
    {
        return $this->defaultCriteria;
    }


    /**
     * Checks that the record class associated to the set already exists.
     * If not, an empty one inheriting ORM_Record is declared.
     */
    /*
     protected function checkRecordClass()
     {
     if(!class_exists($this->sRecordClassName))
     {
     $php =
     'class ' . $this->sRecordClassName . ' extends ORM_Record
     {
     function __construct(ORM_RecordSet $oParentSet)
     {
     parent::__construct($oParentSet);
     }
     }';
     eval($php);
     }
     }
     //*/






    /**
     * Expands the foreign key identified by $sFkFieldName:
     * the foreign key field is replaced by an instance of the referenced set.
     *
     * @param	string	$fieldName
     *
     * @return	mixed	ORM_RecordSet on success, null otherwise
     */
    public function join($fieldName)
    {
        // assert('$this->getField($fieldName); /* The field ' . $fieldName . ' is not a part of ' . get_class($this) . '. */');
        // assert('!($this->getField($fieldName) instanceof ORM_RecordSet); /* The field ' . $fieldName . ' is already joined. */');
        // assert('$this->getField($fieldName) instanceof ORM_FkField; /* The field ' . $fieldName . ' is not an ORM_FkField. */');

        $field = $this->getField($fieldName);
        if (!($field instanceof ORM_FkField)) {
            return null;
        }

        $recordSet = $field->getForeignSet();
        $this->aField[$fieldName] = $recordSet;
        $recordSet->setParentSet($this);
        return $this;
    }



    /**
     * Returns the specified field, joining fkFields before.
     *
     * Returns the specified field in the same way as __get(), but if the
     * field is a fkField it is joined before and the resulting recordset
     * is returned.
     *
     * @param	string	$fieldName	The name of the field or relation for which the value must be returned.
     *
     * @return ORM_Field The value of the field or null if the field is not a part of the record.
     */
    public function __call($fieldName, $args)
    {
        $field = $this->getField($fieldName);
        if (!$field instanceof ORM_FkField || $field instanceof ORM_RecordSet) {
            return $field;
        }
        $this->join($fieldName);
        return $this->getField($fieldName);
    }



    /**
     * Specifies that the recordset is linked to one element of the recordset of class $foreignSetClassName.
     *
     * @param string $fieldName			Field name of the set that have a link
     * @param string $foreignSetClassName	Class name of the set on which the field is bound
     * @param string $fieldClassName		Class name of the field to use as foreign key
     *
     * @return ORM_FkField
     */
    public function hasOne($fieldName, $foreignSetClassName, $fieldClassName = 'ORM_IntField')
    {
        $ownerSetClassName = get_class($this);
        $ownerSetFieldName = $fieldName;


        if (substr($foreignSetClassName, -3, 3) !== 'Set') {
            $foreignSetClassName = $foreignSetClassName . 'Set';
        }

        $oRelations = bab_getInstance('ORM_Relations');
        $oRelations->addRelation(new ORM_OneRelation($ownerSetClassName, $ownerSetFieldName, $foreignSetClassName));

        $this->addFields(new ORM_FkField($fieldName, $foreignSetClassName, false, null, $fieldClassName));
        return $this->$fieldName;
    }




    /**
     * Specifies that the Set is linked to N elements of the Set corresponding to $recordClassName.
     *
     * @param string $sRelationName
     * @param string $sForeignSetClassName
     * @param string $sForeignRelationName
     * @return ORM_ManyRelation
     */
    public function hasMany($sRelationName, $sForeignSetClassName, $sForeignRelationName, $sRelationSetClassName = null)
    {
        $sOwnerSetClassName	= get_class($this);
        $oRelations = bab_getInstance('ORM_Relations');
        $oRelation = new ORM_ManyRelation($sOwnerSetClassName, $sRelationName, $sForeignSetClassName, $sForeignRelationName, $sRelationSetClassName);
        $oRelations->addRelation($oRelation);
        return $oRelation;
    }



    /**
     * Returns the specified ORM_Relation.
     *
     * @param string $sRelationName The name of the relation to return.
     *
     * @return ORM_OneRelation Or null if it does not exist.
     */
    public function getRelation($sRelationName)
    {
        $oRelations = bab_getInstance('ORM_Relations');
        $oRelation = $oRelations->getRelation(get_class($this), $sRelationName);
        return $oRelation;
    }


    /**
     * Join all hasOne relations
     * @param	int	$depth		optional parameter to define the recusivity depth
     * @return ORM_RecordSet
     */
    public function joinHasOneRelations($depth = 1)
    {
        foreach ($this->getHasOneRelations() as $relation) {
            $fieldname = $relation->getOwnerSetFieldName();

            if ($this->$fieldname instanceof ORM_RecordSet) {
                trigger_error(sprintf('field %s already joined', $fieldname));
                continue;
            }

            if (method_exists($this, $fieldname)) {
                trigger_error(sprintf('A method with the same name of the field "%s" exists', $fieldname));
                continue;
            }

            $this->$fieldname();

            $next = $depth - 1;

            if ($next > 0) {
                $this->$fieldname->joinHasOneRelations($next);
            }
        }

        return $this;
    }


    /**
     * Get all ORM_OneRelation.
     *
     * @return array <ORM_OneRelation>
     */
    public function getHasOneRelations()
    {
        /* @var $oRelations ORM_Relations */
        $oRelations = bab_getInstance('ORM_Relations');

        return $oRelations->getHasOneRelations(get_class($this));
    }


    /**
     * Get all ORM_ManyRelation.
     *
     * @return array <ORM_ManyRelation>
     */
    public function getHasManyRelations()
    {
        /* @var $oRelations ORM_Relations */
        $oRelations = bab_getInstance('ORM_Relations');

        return $oRelations->getHasManyRelations(get_class($this));
    }


    /**
     * Returns the specified ORM_Field.
     * If the field is an ORM_RecordSet, the primary key field of this recordset is returned.
     *
     * @param string $fieldName  The name of the field to return.
     * @since 0.11.15
     * @throws ORM_OutOfBoundException if the field does not exist.
     * @return ORM_Field
     */
    public function getScalarField($fieldName)
    {
        $field = $this->getField($fieldName);
        if ($field instanceof ORM_RecordSet) {
            $pkName = $field->getPrimaryKey();
            return $field->getField($pkName);
        }
        return $field;
    }

    /**
     * Returns the specified ORM_Field.
     *
     * @param string $fieldName The name of the field to return.
     *
     * @throws ORM_OutOfBoundException if the field does not exist.
     * @return ORM_Field if the field is a part of the set.
     */
    public function getField($fieldName)
    {
        if (!$this->fieldExist($fieldName)) {
            throw new ORM_OutOfBoundException('The field "' . $fieldName . '" is not a part of ' . get_class($this));
        }
        return $this->aField[$fieldName];
    }


    /**
     * Get an ORM_Field.
     *
     * @param string $sFieldPath Slash separated path to a field
     *
     * @throws ORM_OutOfBoundException if the field path does not exist.
     *
     * @return mixed ORM_Field if the field is a part of the set, null otherwise
     */
    public function getFieldByPath($sFieldPath)
    {
        if ($this->fieldExist($sFieldPath)) {
            return $this->aField[$sFieldPath];
        }
        if ($relation = $this->getRelation($sFieldPath)) {
            return $relation;
        }
        $firstSeparator = strpos($sFieldPath, '/');
        $fieldName = substr($sFieldPath, 0, $firstSeparator);
        $sFieldPath = substr($sFieldPath, $firstSeparator + 1);

        $field = $this->getField($fieldName);
        if (!(($field instanceof ORM_FkField) || ($field instanceof ORM_RecordSet))) {
            throw new ORM_OutOfBoundException('The field "' . $fieldName . '" is not a ORM_FkField or ORM_RecordSet');
        }
        if ($field instanceof ORM_FkField) {
            $this->join($field->getName());
        }
        return $this->aField[$field->getName()]->getFieldByPath($sFieldPath);
    }


    /**
     * Return a value that indicate if a field is a part of the set.
     *
     * @param string $sFieldName Name of the field
     *
     * @return boolean ORM_Field True if the field exist, false otherwise
     */
    public function fieldExist($sFieldName)
    {
        return array_key_exists($sFieldName, $this->aField);
    }


    /**
     * Get an array of ORM_Field of the set.
     *
     * @return ORM_Field[] An array of ORM_Field
     */
    public function getFields()
    {
        return $this->aField;
    }

    /**
     * Set the primary key name of the set.
     *
     * @param string $sFieldName
     *            Name of the primary key
     *
     * @return ORM_PkField
     */
    public function setPrimaryKey($sFieldName)
    {
        assert('is_string($sFieldName); /* ' . __METHOD__ . ' The primary key must be a string. */');
        assert('strlen(trim($sFieldName)) > 0; /* ' . __METHOD__ . ' The primary key name is not valid. */');

        if (is_string($sFieldName) && strlen(trim($sFieldName)) > 0) {

            if (isset($this->aField[$sFieldName])) {
                $this->primaryKey = $this->aField[$sFieldName];
            } else {
                $this->primaryKey = ORM_PkField($sFieldName);
                $this->primaryKey->setParentSet($this);
                $this->aField[$this->primaryKey->getName()] = $this->primaryKey;
            }

            $this->sPrimaryKeyName = $sFieldName;
        }

        return $this->primaryKey;
    }


    /**
     * Get the primary key field.
     *
     * @return ORM_PkField
     */
    public function getPrimaryKeyField()
    {
        return $this->primaryKey;
    }


    /**
     * Get the primary key name of the set.
     *
     * @return string Name of the primary key
     */
    public function getPrimaryKey()
    {
        return $this->sPrimaryKeyName;
    }



    /**
     * shortcut to addkey for index keys
     *
     * @param ORM_FIeld $field first field of key
     *                          more fields can be added as additional parameters
     *
     * @return ORM_RecordSet
     */
    public function addIndexKey(ORM_Field $field)
    {
        require_once dirname(__FILE__).'/fieldkey.class.php';

        $fields = func_get_args();
        $key = new ORM_FieldKey($fields);
        $key->unique = false;
        $this->addFieldKey($key);

        return $this;
    }

    /**
     * shortcut to addkey for unique keys
     *
     * @param ORM_FIeld $field first field of key
     *                          more fields can be added as additional parameters
     *
     * @return ORM_RecordSet
     */
    public function addUniqueKey(ORM_Field $field)
    {
        require_once dirname(__FILE__).'/fieldkey.class.php';

        $fields = func_get_args();
        $key = new ORM_FieldKey($fields);
        $key->unique = true;
        $this->addFieldKey($key);

        return $this;
    }

    /**
     * Add a key to recordSet, primary key is not handled by this method
     *
     * @param ORM_FieldKey $key
     *
     * @return ORM_RecordSet
     */
    public function addFieldKey(ORM_FieldKey $key)
    {
        $this->fieldKeys[] = $key;
        return $this;
    }

    /**
     * Get field keys
     *
     * @return array
     */
    public function getFieldKeys()
    {
        return $this->fieldKeys;
    }



    /**
     * Adds fields to the Set.
     *
     * Takes a variable number of ORM_Field parameters.
     *
     * @return void
     */
    public function addFields()
    {
        $iNumArgs = func_num_args();
        for ($iArg = 0; $iArg < $iNumArgs; $iArg++) {
            $oField = func_get_arg($iArg);
            if (!$oField instanceof ORM_Field && !$oField instanceof ORM_Criterion) {
                throw new ORM_IllegalArgumentException('Field must be a ORM_Field or an ORM_Criterion.');
            }
            $oField->setParentSet($this);
            $this->aField[$oField->getName()] = $oField;
        }
    }




    /**
     * Concatenation of all operands.
     *
     * @param mixed ...$operands		Any number of strings or ORM_Fields
     * @return ORM_ConcatOperation
     */
    public function concat()
    {
        $aArgList = func_get_args();
        $operation = new ORM_ConcatOperation('', $aArgList);
        $operation->setParentSet($this);
        return $operation;
    }


    /**
     * Replaces all multilang fields by corresponding fields in the specified language.
     * The fields of joined recordSets are also replaced.
     *
     * If $language is not specified or null, the current language is used (@see bab_getLanguage()).
     * If $language is false, all original multilang fields replaced after a previous call to useLang() are reverted.
     *
     * @param string|false $language
     * @return $this
     */
    public function useLang($language = null)
    {
        if (!isset($language)) {
            $language = bab_getLanguage();
        }

        $fields = $this->getFields();
        foreach ($fields as $field) {
            $field->useLang($language);
        }
        return $this;
    }

    //Backend part


    /**
     * Attaches the specified backend to this RecordSet.
     *
     * @param ORM_BackEnd $oBackEnd The backend to use with this RecordSet.
     *
     * @return void
     */
    public static function setBackend(ORM_BackEnd $oBackEnd)
    {
        self::$defaultBackend = $oBackEnd;
    }


    /**
     * Returns the backend attached to this RecordSet.
     *
     * @return ORM_BackEnd
     */
    public function getBackend()
    {
        return self::$defaultBackend;
    }


    /**
     * Returns the backend or throws an exception if the backend has not been set.
     *
     * @since 0.9.13
     * @throws ORM_Exception
     * @return ORM_BackEnd
     */
    public function requestBackend()
    {
        $backend = $this->getBackend();
        if (!$backend instanceof ORM_Backend) {
            throw new ORM_Exception('The backend has not been set.');
        }
        return $backend;
    }


    /**
     * Returns an iterator on records matching the specified criteria.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Criteria $criteria Criteria for selecting records
     *
     * @return ORM_Iterator Iterator on success.
     */
    public function select(ORM_Criteria $criteria = null)
    {
        $backend = $this->requestBackend();
        return $backend->select($this, $criteria);
    }


    /**
     * Returns the specified record.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Criteria|string $mixedParam    Criteria for selecting records
     *                                           or the value for selecting record
     * @param string              $sPropertyName The name of the property on which
     *                                           the value applies. If not
     *                                           specified or null, the set's
     *                                           primary key will be used.
     *
     * @return ORM_Record|null ORM_Record on success, null otherwise.
     */
    public function get($mixedParam = null, $sPropertyName = null)
    {
        $backend = $this->requestBackend();
        return $backend->get($this, $mixedParam, $sPropertyName);
    }


    /**
     * Similar to ORM_RecordSet::get() method  but throws a crm_NotFoundException if the
     * record is not found.
     *
     * @since 1.0.18
     *
     * @throws ORM_Exception
     * @throws ORM_NotFoundException
     *
     * @param ORM_Criteria|string $mixedParam    Criteria for selecting records
     *                                           or the value for selecting record
     * @param string              $sPropertyName The name of the property on which
     *                                           the value applies. If not
     *                                           specified or null, the set's
     *                                           primary key will be used.
     *
     * @return ORM_Record
     */
    public function request($mixedParam = null, $sPropertyName = null)
    {
        $record = $this->get($mixedParam, $sPropertyName);
        if (!isset($record)) {
            throw new ORM_NotFoundException($this, $mixedParam);
        }
        return $record;
    }


    /**
     * Computes and returns the select query corresponding to the specified criteria.
     * The result depends on the attached backend.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Criteria $oCriteria Criteria for selecting records.
     * @param array        $aOrder    array(array(ORM_Field, 'ASC'), array(ORM_Field, 'DESC')...);
     * @param array        $aGroupBy  array(ORM_Field, ORM_Field...);
     *
     * @return mixed string on success.
     */
    public function getSelectQuery(ORM_Criteria $oCriteria = null, $aOrder = array(), $aGroupBy = array(), $sLimit = null)
    {
        $backend = $this->requestBackend();
        return $backend->getSelectQuery($this, $oCriteria, $aOrder, $aGroupBy, $sLimit);
    }



    /**
     * Computes and returns the save query corresponding to the specified criteria.
     * The result depends on the attached backend.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Record $record The record to save.
     *
     * @return mixed string on success, null nothing to do.
     */
    public function getSaveQuery(ORM_Record $record)
    {
        $backend = $this->requestBackend();
        return $backend->getSaveQuery($this, $record);
    }


    /**
     * Saves a record.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Record $oRecord The record to save.
     *
     * @return boolean True on success, false otherwise.
     */
    public function save(ORM_Record $oRecord)
    {
        $backend = $this->requestBackend();
        return $backend->save($this, $oRecord);
    }


    /**
     * Deletes records, the number of records depends on criteria.
     *
     * @throws ORM_Exception
     *
     * @param ORM_Criteria $oCriteria Criteria for selecting records to delete.
     *
     * @return boolean True on success, False otherwise
     */
    public function delete(ORM_Criteria $oCriteria = null)
    {
        $backend = $this->requestBackend();
        return $backend->delete($this, $oCriteria);
    }


    /**
     * Returns the value with a suitable form for display.
     *
     * @param mixed $mixedValue
     * @return mixed
     */
    public function output($mixedValue)
    {
        if ($mixedValue instanceof ORM_Record) {
            return $mixedValue->getRecordTitle();
        }

        return parent::output($mixedValue);
    }


    /**
     * Call destructor before unset() to free memory usage of a set.
     *
     */
    public function __destruct()
    {
        parent::__destruct();
        $this->aField = array();
        $this->defaultCriteria = null;
    }
}
