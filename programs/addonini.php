; <?php/*
[general]
name                            ="LibOrm"
version                         ="0.11.15"
encoding                        ="UTF-8"
description                     ="Mapping library between relational database and PHP objects (object-relational mapping)"
description.fr                  ="Librairie partagée fournissant une API orientée objet d'accès à la base de données"
long_description.fr             ="README.md"
delete                          =1
longdesc                        =""
ov_version                      ="6.7.0"
php_version                     ="5.1.0"
mysql_version                   ="4.1.2"
addon_access_control            =0
author                          ="Laurent Choulette(laurent.choulette@cantico.fr) Samuel Zebina(samuel.zebina@cantico.fr)"
icon                            ="icon.png"
mysql_character_set_database    ="latin1,utf8"
tags                            ="library,orm"

[recommendations]
mysql_version                   ="5.1.0"

;*/?>