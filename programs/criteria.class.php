<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/field.class.php';
require_once dirname(__FILE__) . '/backend.class.php';

/**
 * Criteria are a combination of other criteria or criterion.
 * Base class for operators and filters criteria.
 */
class ORM_Criteria
{

    /**
     *
     */
	public function __construct()
	{
	}


	/**
	 * Returns a new ORM_Or criteria between this criteria and the specified criteria.
	 *
	 * @return ORM_Criteria
	 */
	public function _OR_(ORM_Criteria $oCriteria)
	{
	    if ($oCriteria instanceof ORM_TrueCriterion) {
	        return $oCriteria;
	    }
	    if ($this instanceof ORM_FalseCriterion) {
	        return $oCriteria;
	    }
	    if ($oCriteria instanceof ORM_FalseCriterion) {
	        return $this;
	    }
	    if ($this instanceof ORM_TrueCriterion) {
	        return $this;
	    }
	    return new ORM_Or($this, $oCriteria);
	}


	/**
	 * Returns a new ORM_And criteria between this criteria and the specified criteria.
	 *
	 * @return ORM_Criteria
	 */
	public function _AND_(ORM_Criteria $oCriteria)
	{
	    if ($oCriteria instanceof ORM_FalseCriterion) {
	        return $oCriteria;
	    }
	    if ($this instanceof ORM_TrueCriterion) {
	        return $oCriteria;
	    }
	    if ($oCriteria instanceof ORM_TrueCriterion) {
	        return $this;
	    }
	    if ($this instanceof ORM_FalseCriterion) {
	        return $this;
	    }
	    return new ORM_And($this, $oCriteria);
	}


	/**
	 *
	 * @deprecated by ORM_Criteria::_NOT(). This function does not use $this, so it's basically a static function.
	 * @param $oCriteria
	 * @return ORM_Not
	 */
	public function _NOT_(ORM_Criteria $oCriteria)
	{
		return new ORM_Not($oCriteria);
	}


	/**
	 * Negates this criteria.
	 *
	 * @return ORM_Not
	 */
	public function _NOT()
	{
		return new ORM_Not($this);
	}

    /**
     * Returns the corresponding string for the specified backend.
     *
     * @param ORM_BackEnd $oBackEnd
     *
     * @return string
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
    }


    /**
     * @return string
     */
    public function toJson()
    {
    }


	/**
	 * Call destructor before unset() to free memory usage of a criteria
	 */
	public function __destruct()
	{
	}
}



/**
 * Abstract for binary criteria (operators)
 */
abstract class ORM_BinaryCriteria extends ORM_CriterionBase
{

    /**
     * @var ORM_Criteria
     */
    protected $oLeftCriteria	= null;

	/**
     * @var ORM_Criteria
     */
    protected $oRightCriteria	= null;

	/**
	 * @param  ORM_Criteria    $oLeftCriteria
	 * @param  ORM_Criteria    $oRightCriteria
	 */
	public function __construct(ORM_Criteria $oLeftCriteria, ORM_Criteria $oRightCriteria)
	{
		parent::__construct();

		$this->oLeftCriteria	= $oLeftCriteria;
		$this->oRightCriteria	= $oRightCriteria;
	}

    /**
     * Get field
     * @return ORM_Field
     */
	public function getField()
	{
		return $this->oLeftCriteria->getField();
	}


	/**
	 * Call destructor before unset() to free memory usage of a criteria
	 *
	 */
	public function __destruct()
	{
		parent::__destruct();
		$this->oLeftCriteria = null;
		$this->oRightCriteria = null;
	}
}

/**
 * The AND operator
 */
class ORM_And extends ORM_BinaryCriteria
{

    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->andCriteria($this->oLeftCriteria, $this->oRightCriteria);
    }


    /**
     * @return string
     */
    public function toJson()
    {
        return '{ type: "and", [' . $this->oLeftCriteria->toJson(). ', ' . $this->oRightCriteria->toJson(). '] }';
    }
}

/**
 * The OR operator
 */
class ORM_Or extends ORM_BinaryCriteria
{

    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->orCriteria($this->oLeftCriteria, $this->oRightCriteria);
    }

    /**
     * @return string
     */
    public function toJson()
    {
        return '{ type: "or", [' . $this->oLeftCriteria->toJson(). ', ' . $this->oRightCriteria->toJson(). '] }';
    }
}

/**
 * The NOT operator
 */
class ORM_Not extends ORM_CriterionBase
{
    /**
     * @var ORM_Criteria
     */
	private $oCriteria = null;

	/**
	 * Creates a new ORM_Not object.
	 *
	 * @param ORM_Criteria $oCriteria  The criteria that will be negated.
	 */
	public function __construct(ORM_Criteria $oCriteria)
	{
		parent::__construct();
		$this->oCriteria = $oCriteria;
	}

    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->notCriteria($this->oCriteria);
	}


	/**
	 * @return string
	 */
	public function toJson()
	{
	    return '{ type: "not", [' . $this->oCriteria->toJson() . '] }';
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::__destruct()
	 */
	public function __destruct()
	{
		parent::__destruct();
		$this->oCriteria = null;
	}
}


/**
 * Critertion base
 */
abstract class ORM_CriterionBase extends ORM_Criteria
{
    /**
     * @var  ORM_Field   $oField
     */
	protected $oField = null;

	/**
	 * @param  ORM_Field   $oField
	 */
	public function __construct(ORM_Field $oField = null)
	{
		parent::__construct();
		$this->oField = $oField;
	}

    /**
     * @return ORM_Field
     */
	public function getField()
	{
		return $this->oField;
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::__destruct()
	 */
	public function __destruct()
	{
		parent::__destruct();
		$this->oField = null;
	}



    /**
     * Returns an iterator on records matching the specified criteria.
     *
     * @throws ORM_Exception
     *
     * @see ORM_RecordSet::select()
     *
     * @return ORM_Iterator Iterator on success, null if the backend have not been set
     */
	public function select()
	{
	    $field = $this->getField();
	    if (!isset($field)) {
	        throw new ORM_Exception('There is no field associated to the Criterion.');
	    }
	    $recordSet = $field->getParentSet();
		if (!isset($recordSet)) {
	        throw new ORM_Exception('There is no RecordSet associated to the Criterion field.');
	    }
	    return $recordSet->select($this);
	}
}

/**
 * Criterion abstract class
 */
abstract class ORM_Criterion extends ORM_CriterionBase
{
    protected $mixedValue = null;


    /**
     * Construct
     *
     * @param  ORM_Field   $oField
     * @param  mixed       $mixedValue
     */
    public function __construct(ORM_Field $oField, $mixedValue = null)
    {
        parent::__construct($oField);
        $this->mixedValue = $mixedValue;
    }

    /**
     * Get the tested criterion value
     * @return mixed
     */
    public function getValue()
    {
        return $this->mixedValue;
    }
}



/**
 * Greater Than Criterion
 */
class ORM_GreaterThanCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_GreaterThanCriterion.
     *
     * @param ORM_Field $oField
     * @param mixed $sValue
     */
    public function __construct(ORM_Field $oField, $sValue)
    {
        parent::__construct($oField, $sValue);
    }


    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->greaterThan($this->oField, $this->mixedValue);
    }
}

/**
 * Greater Than Or Equal Criterion
 */
class ORM_GreaterThanOrEqualCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_GreaterThanOrEqualCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
    public function __construct(ORM_Field $oField, $sValue)
    {
        parent::__construct($oField, $sValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->greaterThanOrEqual($this->oField, $this->mixedValue);
    }
}

/**
 * Less Than criterion
 */
class ORM_LessThanCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_LessThanCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->lessThan($this->oField, $this->mixedValue);
	}
}

/**
 * Less Than Or Equal Criterion
 */
class ORM_LessThanOrEqualCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_LessThanOrEqualCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->lessThanOrEqual($this->oField, $this->mixedValue);
	}
}

/**
 * Is criterion
 */
class ORM_IsCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_IsCriterion.
     *
     * @param ORM_Field $oField
     * @param mixed     $mixedValue
     */
	public function __construct(ORM_Field $oField, $mixedValue)
	{
		parent::__construct($oField, $mixedValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->is($this->oField, $this->mixedValue);
	}

	/**
	 * @return string
	 */
	public function toJson()
	{
	    return '{ "' . $this->oField->getPath() . '": { "is": ' . json_encode($this->mixedValue) . ' } }';
	}
}


/**
 * Not criterion
 */
class ORM_NotCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_NotCriterion.
     *
     * @param ORM_Field $oField
     * @param mixed    $mixedValue
     */
	public function __construct(ORM_Field $oField, $mixedValue)
	{
		parent::__construct($oField, $mixedValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->isNot($this->oField, $this->mixedValue);
	}


	/**
	 * @return string
	 */
	public function toJson()
	{
	    return '{ "' . $this->oField->getPath() . '": { "isNot": ' . json_encode($this->mixedValue) . ' } }';
	}
}

/**
 * In criterion
 */
class ORM_InCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_InCriterion.
     *
     * @param ORM_Field $oField
     * @param array    $aValue
     */
	public function __construct(ORM_Field $oField, $aValue)
	{
		parent::__construct($oField, $aValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->in($this->oField, $this->mixedValue);
	}


	/**
	 * @return string
	 */
	public function toJson()
	{
	    return '{ "' . $this->oField->getPath() . '": { "in": ' . json_encode($this->mixedValue) . ' } }';
	}
}

/**
 * Not in critertion
 */
class ORM_NotInCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_NotInCriterion.
     *
     * @param ORM_Field $oField
     * @param array $aValue
     */
	public function __construct(ORM_Field $oField, $aValue)
	{
		parent::__construct($oField, $aValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->notIn($this->oField, $this->mixedValue);
	}
}

/**
 * Like criterion
 */
class ORM_LikeCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_LikeCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->like($this->oField, $this->mixedValue);
	}
}

/**
 * Not like criterion
 */
class ORM_NotLikeCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_NotLikeCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->notLike($this->oField, $this->mixedValue);
	}
}

/**
 * Sounds like criterion
 *
 * @since 0.9.9
 */
class ORM_SoundsLikeCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_SoundsLikeCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->soundsLike($this->oField, $this->mixedValue);
	}
}

/**
 * Starts with criterion
 */
class ORM_StartsWithCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_StartsWithCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->startsWith($this->oField, $this->mixedValue);
	}
}

/**
 * Ends with criterion
 */
class ORM_EndsWithCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_EndsWithCriterion.
     *
     * @param ORM_Field $oField
     * @param string    $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->endsWith($this->oField, $this->mixedValue);
	}
}

/**
 * Contains Criterion
 * match string contain the test string
 */
class ORM_ContainsCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_ContainsCriterion.
     *
     * @param ORM_Field $oField
     * @param mixed $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->contains($this->oField, $this->mixedValue);
	}
}


/**
 * HasValue Criterion
 * match set contain the key
 */
class ORM_HasValueCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_ContainsCriterion.
     *
     * @param ORM_Field $oField
     * @param mixed $sValue
     */
    public function __construct(ORM_Field $oField, $sValue)
    {
        parent::__construct($oField, $sValue);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->hasValue($this->oField, $this->mixedValue);
    }
}


/**
 * The matched string must contain the test string and the matched substring
 * must begin with a space or begining of string and must end with a space or
 * end of string.
 */
class ORM_ContainsWordsCriterion extends ORM_Criterion
{
    /**
     * Creates a new ORM_ContainsWordsCriterion.
     *
     * @param   ORM_Field   $oField
     * @param   string      $sValue
     */
	public function __construct(ORM_Field $oField, $sValue)
	{
		parent::__construct($oField, $sValue);
	}

	/**
	 * (non-PHPdoc)
	 * @see ORM_Criteria::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->containsWords($this->oField, $this->mixedValue);
	}
}

/**
 * Match Search Criterion
 * search multiple words
 */
class ORM_MatchSearchCriterion extends ORM_Criterion
{
	private $method = null;


	/**
	 * '_OR_' or '_AND_'
	 * @var string
	 */
	private $operator = null;

	/**
	 * @param ORM_Field $field
	 * @param string    $searchString
	 * @param string    $operator		'_OR_' or '_AND_'
	 * @param string    $method         Any ORM_Field method name taking a string as
	 *                                  input parameter and returning an ORM_Criterion.
	 *                                  Eg. 'equals', 'contains', 'startsWith'...
	 *
	 */
	public function __construct(ORM_Field $field, $searchString, $operator, $method)
	{
		parent::__construct($field, $searchString);
		$this->operator = $operator;
		$this->method = $method;
	}



    /**
     * Extract the keywords from the searchString and return them as an array.
     *
     * @return array
     */
    public function getSearchStringKeywords()
    {
        $keywords = array();
        $matches = array();

        $regExp = '/(?:(?P<unquoted>[^"\s]+)|(?:"(?P<quoted>[^"]+)"))\s*/';

        if ($nbMatches = preg_match_all($regExp, $this->mixedValue, $matches)) {

        	for ($i = 0; $i < $nbMatches; $i++) {

        	    $keyword = trim($matches['unquoted'][$i], ' ,;.?!:');
				if ($keyword && mb_strlen($keyword) > 2) {
					$keywords[] = $keyword;
				}

				$keyword = $matches['quoted'][$i];
				if ($keyword) {
					$keywords[] = $keyword;
				}
			}
        }

        return $keywords;
    }


	/**
	 * (non-PHPdoc)
	 * @see ORM_Criterion::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		$criteria = null;
		$operator = $this->operator;
		$method = $this->method;
		$keywords = $this->getSearchStringKeywords();

		foreach ($keywords as $keyword) {

			if (null === $criteria) {
				$criteria = $this->oField->$method($keyword);
				continue;
			}

			$criteria = $criteria->$operator($this->oField->$method($keyword));
		}

		if (null !== $criteria) {
			return $criteria->toString($oBackEnd);
		}

		return $oBackEnd->contains($this->oField, $this->mixedValue);
	}
}


/**
 * True criterion
 */
class ORM_TrueCriterion extends ORM_CriterionBase
{
    /**
     * {@inheritDoc}
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->alwaysTrue();
    }
}

/**
 * False criterion
 */
class ORM_FalseCriterion extends ORM_CriterionBase
{
    /**
     * {@inheritDoc}
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->alwaysFalse();
    }
}

/**
 * Is Null Criterion
 */
class ORM_IsNullCriterion extends ORM_CriterionBase
{
	/**
	 * (non-PHPdoc)
	 * @see ORM_Criterion::toString()
	 */
	public function toString(ORM_BackEnd $oBackEnd)
	{
		return $oBackEnd->fieldIsNull($this->oField);
	}
}

/**
 * Between criterion
 */
class ORM_BetweenCriterion extends ORM_Criterion
{
    protected $min;
    protected $max;
    
    /**
     * Creates a new ORM_BetweenCriterion.
     *
     * @param   ORM_Field   $oField
     * @param   string      $min
     * @param   string      $max
     */
    public function __construct(ORM_Field $oField, $min, $max)
    {
        parent::__construct($oField);
        $this->min = $min;
        $this->max = $max;
    }
    
    /**
     * (non-PHPdoc)
     * @see ORM_Criteria::toString()
     */
    public function toString(ORM_BackEnd $oBackEnd)
    {
        return $oBackEnd->between($this->oField, $this->min, $this->max);
    }
}