<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/../backend.class.php';
require_once dirname(__FILE__) . '/iterator.class.php';







/**
 * MySql backend
 */
class ORM_MySqlBackend extends ORM_BackEnd
{
    private $sSeparator = '__';

    /**
     *
     * @var babDatabase
     */
    protected $oDataBaseAdapter = null;


    /**
     * Mapping between recordset classnames and mysql tablenames.
     * @var string[]
     */
    private $recordSetTableNames = array();



    /**
     * @param babDatabase $oDataBaseAdapter
     */
    public function __construct($oDataBaseAdapter)
    {
        parent::__construct();
        $this->sBackendType	= 'MySql';
        $this->setDataBaseAdapter($oDataBaseAdapter);

        // TODO : We should remove this when we manage correctly foreign keys.
        // (Foreign keys should not be NOT NULL and be NULL when not associated).
        $oDataBaseAdapter->db_query('SET FOREIGN_KEY_CHECKS = 0');
    }

    /**
     * @param babDatabase $oDataBaseAdapter
     */
    public function setDataBaseAdapter($oDataBaseAdapter)
    {
        $this->oDataBaseAdapter = $oDataBaseAdapter;
    }



    /**
     * Computes a table name corresponding to a record set class name.
     *
     * @param string $recordSetClassName
     * @return string
     */
    public function computeTableName($recordSetClassName)
    {
        return ltrim(
            strtolower(
                str_replace(
                    '\\',
                    '_',
                    substr($recordSetClassName, 0, -strlen(ORM_RecordSet::SUFFIX))
                )
            ),
            '_'
        );
    }

    /**
     * @return babDatabase
     */
    public function getDataBaseAdapter()
    {
        return $this->oDataBaseAdapter;
    }



    /**
     * Returns the table name associated to the specified recordset class.
     * @param ORM_RecordSet $recordSet
     *
     * @return string
     */
    public function getTableName(ORM_RecordSet $recordSet)
    {
        if (isset($recordSet->backendTableName)) {
            return $recordSet->backendTableName;
        }
        $recordSetClassName = get_class($recordSet);
        if (!isset($this->recordSetTableNames[$recordSetClassName])) {
            $this->recordSetTableNames[$recordSetClassName] = $this->computeTableName($recordSetClassName);
        }
        return $this->recordSetTableNames[$recordSetClassName];
    }


    /**
     * Sets the table name associated to the specified recordset class.
     *
     * @param ORM_RecordSet $recordSet
     * @param string $tablename
     *
     * @return self
     */
    public function setTableName(ORM_RecordSet $recordSet, $tablename)
    {
        $recordSet->backendTableName = $tablename;
        return $this;
    }


    /**
     * Get a flattened array of table aliases of the specified recordSet.
     * [
     *      't123' => ORM_RecordSet,
     *      'tXXX' => ORM_RecordSet,
     *      ...
     * ]
     *
     * @param ORM_RecordSet $recordSet
     *
     * @return	ORM_RecordSet[]	An array of ORM_RecordSet indexed by table alias
     */
    public function getSetTableAliasDefinition($recordSet)
    {
        $aliases = array();
        $this->getSets($recordSet, $aliases);
        return $aliases;
    }

    /**
     * Recursively build a flattened array of table aliases of the specified recordSet.
     * This method is used by the getSetTableAliasDefinition.
     *
     * @param ORM_RecordSet $recordSet
     * @param ORM_RecordSet[] $aliases
     * @return	void
     */
    private function getSets($recordSet, &$aliases)
    {
        $aliases[$this->getSetAlias($recordSet)] = $recordSet;
        foreach ($recordSet->getFields() as $field) {
            if ($field instanceof ORM_RecordSet) {
                $this->getSets($field, $aliases);
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::getRecordValue()
     */
    public function getRecordValue(ORM_Record $oRecord, $sFieldName)
    {
        $oParentSet = $oRecord->getParentSet();
        $oData		= $oRecord->getData();

        if ($oParentSet->fieldExist($sFieldName)) {
            $field = $oParentSet->getField($sFieldName);
            return $field->getRecordValue($oRecord);
//            return $oData[$sFieldName];
        } elseif (($oRelation = $oParentSet->getRelation($sFieldName)) instanceof ORM_ManyRelation) {
            $sSetName			= $oRelation->getForeignSetClassName();
            $oSet				= new $sSetName;
            $sForeignFieldName	= $oRelation->getForeignFieldName();
            $pkValue 			= $oData[$oParentSet->getPrimaryKey()];
            return $oSet->select($oSet->$sForeignFieldName->in($pkValue));
        }
        return null;
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::setRecordValue()
     */
    public function setRecordValue(ORM_Record $oRecord, $sFieldName, $mixedValue)
    {
        $oParentSet = $oRecord->getParentSet();
        if ($oParentSet->fieldExist($sFieldName)) {
            $field = $oParentSet->getField($sFieldName);
            $field->setRecordValue($oRecord, $mixedValue);
        }
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::initRecordValue()
     */
    public function initRecordValue(ORM_Record $oRecord, $sFieldName, $mixedValue)
    {
        $oParentSet = $oRecord->getParentSet();
        if ($oParentSet->fieldExist($sFieldName)) {
            $field = $oParentSet->getField($sFieldName);
            $field->initRecordValue($oRecord, $mixedValue);
        }
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::isRecordFieldSet()
     */
    public function isRecordFieldSet(ORM_Record $oRecord, $sFieldName)
    {
        /* @var $oRecord ORM_MysqlRecord */
        $oParentSet = $oRecord->getParentSet();

        if (!$oParentSet->fieldExist($sFieldName)) {
            return false;
        }

        $oData = $oRecord->getData();
        if (!$oData->offsetExists($sFieldName)) {
            return false;
        }

        $value = $oData[$sFieldName];

        if (($value instanceof ORM_Record) && 0 == $value->getData()->count()) {
            return false;
        }

        return isset($value);
    }

    /**
     * @param ORM_Field $oField
     * @return string
     */
    public function getAlias(ORM_Field $oField)
    {
        return $this->getSetAlias($oField->getParentSet()) .
            $this->sSeparator . $oField->getName();
    }

    /**
     * @param ORM_Field $oField
     * @return string
     */
    public function getFieldAlias(ORM_Field $oField)
    {
        return  't' . $oField->getParentSet()->getUniqueId() . '.' . $oField->getName();
    }

    /**
     * @param ORM_RecordSet $oSet
     * @return string
     */
    public function getSetFieldAlias(ORM_RecordSet $oSet)
    {
        return  't' . $oSet->getUniqueId() . '.' . $oSet->getName();
    }

    /**
     * @param ORM_RecordSet $oSet
     * @return string
     */
    public function getSetAlias(ORM_RecordSet $oSet)
    {
        return 't' . $oSet->getUniqueId();
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->sSeparator;
    }

    /**
     * Get the order part of a MySql query
     *
     * @param	array	$aOrder	The key of the array key must ORM_Field and the value must be ASC or DESC
     *
     * @return	string	The order part of a MySql query
     */
    private function processOrder($aOrder)
    {
        $aGoodValue = array('ASC' => 'ASC', 'DESC' => 'DESC', 'RAND()' => 'RAND()');

        $sOrder = '';
        if (is_array($aOrder) && count($aOrder) > 0) {


            $aValue = array();
            foreach ($aOrder as $aOrderItem) {
                if (isset($aOrderItem[1])) {
                    $oField = $aOrderItem[0];
                    $sOrder = $aOrderItem[1];

                    assert('$oField instanceof ORM_Field /* The key value of $aOrder must be an instanceof ORM_Field. */');
                    assert('array_key_exists($sOrder, $aGoodValue) /* Wrong value for ORDER BY. */');
                    if (array_key_exists($sOrder, $aGoodValue)) {
                        if ($oField instanceof ORM_Operation) {
                            $aValue[] = $oField->toString($this) . ' ' . $sOrder;
                        } else {
                            $aValue[] = $this->oDataBaseAdapter->backTick($this->getAlias($oField)) . ' ' . $sOrder;
                        }
                    }
                } else {
                    $sOrder = $aOrderItem[0];
                    assert('array_key_exists($sOrder, $aGoodValue) /* Wrong value for ORDER BY. */');
                    $aValue[] = $sOrder;
                }
            }

            if (count($aValue) > 0) {
                $sOrder = ' ORDER BY ' . implode(', ', $aValue);
            }
        }
        return $sOrder;
    }

    /**
     * Get the limit part of a MySql query
     *
     * @param	string	$sLimit	start,numelement
     *
     * @return	string	The limit part of a MySql query
     */
    private function processLimit($sLimit)
    {
        if(!$sLimit) {
            return '';
        }

        $explodedLimit = explode(',', $sLimit);
        if(count($explodedLimit) != 2 || !is_numeric($explodedLimit[0]) || !is_numeric($explodedLimit[1])) {
            assert('false /* Wrong value for LIMIT. */');
            return '';
        }

        return ' LIMIT ' . $explodedLimit[1] . ' OFFSET ' . $explodedLimit[0];
    }

    /**
     * Get the group by part of a MySql query
     *
     * @param	array	$aGroupBy	The key of the array key must ORM_Field
     *
     * @return	string	The group by part of a MySql query
     */
    private function processGroupBy($aGroupBy)
    {
        $sGroupBy = '';
        if (is_array($aGroupBy) && count($aGroupBy) > 0) {
            $aValue = array();
            foreach ($aGroupBy as $oField) {
                assert('$oField instanceof ORM_Field /* The key value of $aGroupBy must be an instanceof ORM_Field. */');
                if ($oField instanceof ORM_Field) {
                    $aValue[] = $this->oDataBaseAdapter->backTick($this->getAlias($oField));
                }
            }

            if (count($aValue) > 0) {
                $sGroupBy = ' GROUP BY ' . implode(', ', $aValue);
            }
        }
        return $sGroupBy;
    }



    /**
     * @param ORM_RecordSet $set
     */
    private function processFromClause(ORM_RecordSet $set)
    {
        $setsByAlias = $this->getSetTableAliasDefinition($set);

        $leftJoins = array();
        $first = true;
        foreach ($setsByAlias as $alias => $set) {
            $aliasedTable = $this->getTableName($set) . ' AS ' . $alias;
            if ($first) {
                $leftJoins[] = $aliasedTable;
                $first = false;
            } else {
                $leftJoins[] = ' LEFT JOIN ' . $aliasedTable
                . ' ON ' . $this->getFieldAlias($set) . ' = ' . $alias . '.' . $set->getPrimaryKey();
            }
        }

        $from = 'FROM ' . "\n" . implode(' ', $leftJoins) . "\n";

        return $from;
    }


    /**
     * Get the where clause of a MySql query
     *
     * @param	ORM_Criteria	The criteria which produce the where clause
     *
     * @return	string			The where clause
     */
    private function processWhereClause(ORM_Criteria $oCriteria = null)
    {
        if ($oCriteria instanceof ORM_Criteria) {
            return ' WHERE ' . $oCriteria->toString($this);
        }
        return '';
    }


    /**
     * Get the select query
     *
     * @param ORM_RecordSet $oSet
     *            The recordset on which the query is applied.
     * @param ORM_Criteria $oCriteria
     *            selecting records
     * @param array $aOrder
     *            set of record, the key of the array must be the fieldname and the value must be ASC or DESC
     * @param array $aGroupBy
     * @param string	$sLimit	start,numelement
     * @return string select query
     */
    public function getSelectQuery(ORM_RecordSet $oSet, ORM_Criteria $oCriteria = null, $aOrder = array(), $aGroupBy = array(), $sLimit = null)
    {
        $sSelect = $this->getSelectString($oSet);
        $sWhereClause = $this->processWhereClause($oCriteria);
        $sGroupBy = $this->processGroupBy($aGroupBy);
        $sOrder = $this->processOrder($aOrder);
        $sLimit = $this->processLimit($sLimit);
        $sQuery = $sSelect . $sWhereClause . $sGroupBy . $sOrder . $sLimit;
        // bab_debug($sQuery, DBG_TRACE, 'ORM');
        return $sQuery;
    }

    /**
     * Get the select part of the MySql query
     *
     * @param	ORM_RecordSet	$oSet	The set on which the query is applied
     *
     * @return	string					The select part of the MySql query
     */
    public function getSelectString(ORM_RecordSet $oSet)
    {
        $aSet = $this->getSetTableAliasDefinition($oSet);

        $aColumn = array();

        foreach ($aSet as $_oSet) {
            $aField = $_oSet->getFields();

            foreach ($aField as $oField) {
                if ($oField instanceof ORM_Operation) {
                    $aColumn[] = $oField->toString($this) . ' AS ' . $this->getAlias($oField);
                } elseif ($oField instanceof ORM_Field) {
                    $aColumn[] = $this->getFieldAlias($oField) . ' AS ' . $this->getAlias($oField);
                }
            }
        }

        return $this->getSelectStringTables($oSet, $aColumn);
    }


    /**
     * Get the select string
     *
     * @param	ORM_RecordSet     $set
     * @param	array             $columns	columns to select
     *
     * @return 	string
     */
    private function getSelectStringTables(ORM_RecordSet $set, array $columns)
    {
        $from = $this->processFromClause($set);

        $select = 'SELECT ' . "\n" .
            implode(",\n", $columns) . "\n" .
            $from . "\n";

        return $select;
    }


    /**
     * Get a record
     *
     * @param	ORM_RecordSet		$oSet			The set on which the query is applied
     * @param	ORM_Criteria|string	$oMixed			Criteria for selecting records or the value for selecting record
     * @param	string				$sPropertyName	The name of the property on which the value applies. If not specified or null, the set's primary key will be used.
     *
     * @return	ORM_Record|null						ORM_Record on success, null	otherwise
     */
    public function get(ORM_RecordSet $oSet, $oMixed = null, $sPropertyName = null)
    {
        $oMySqlIterator	= null;

        if ($oMixed instanceof ORM_Criteria) {
            $oMySqlIterator	= $this->select($oSet, $oMixed);
        } else {
            if (is_null($sPropertyName)) {
                $sPropertyName = $oSet->getPrimaryKey();
            }

            $oField	= $oSet->getField($sPropertyName);
            if (is_null($oField)) {
                return null;
            }
            $oMySqlIterator	= $this->select($oSet, $oField->is($oMixed));
        }
        $oMySqlIterator->rewind();

        if (!$oMySqlIterator->valid()) {
            return null;
        }

        $record = $oMySqlIterator->current();

        $oMySqlIterator->__destruct();
        unset($oMySqlIterator);

        return $record;
    }

    /**
     * Retrieves multiple records from a record set matching a specified criteria.
     *
     * @param ORM_RecordSet $set
     *            The record set from which the records will be retrieved.
     * @param ORM_Criteria $criteria
     *            The criteria used for selecting records.
     *
     * @return ORM_MySqlIterator
     */
    public function select(ORM_RecordSet $set, ORM_Criteria $criteria = null)
    {
        $mySqlIterator	= new ORM_MySqlIterator($this->oDataBaseAdapter);
        $mySqlIterator->setSet($set);
        if ($set->getDefaultCriteria() !== null) {
            // The default criteria is always prepended to the requested criteria.
            if (isset($criteria)) {
                $criteria = $set->getDefaultCriteria()->_AND_($criteria);
            } else {
                $criteria = $set->getDefaultCriteria();
            }
        }
        $mySqlIterator->setCriteria($criteria);

        return $mySqlIterator;
    }



    /**
     *
     * @param ORM_Record $record
     * @param string $fieldName
     * @param ORM_Field $field
     * @param array $sqlFieldNames
     * @param array $sqlValues
     * @param array $sqlOnDuplicateKey
     *
     * @return void
     */
    private function addRecordPrimaryKeyFieldNameAndValue(ORM_Record $record, $fieldName, ORM_PkField $field = null, &$sqlFieldNames, &$sqlValues, &$sqlOnDuplicateKey)
    {
        if (!isset($fieldName)) {
            return;
        }
        $database = $this->getDataBaseAdapter();

        $value = $record->$fieldName;

        $sqlFieldName = $database->backTick($fieldName);

        $useLastInsertId = isset($field) && $field->isAutoIncremented() && empty($value);
        if ($useLastInsertId) {
            $sqlOnDuplicateKey[] = $sqlFieldName . '=LAST_INSERT_ID(' . $fieldName . ')';
        } else {
            $sqlOnDuplicateKey[] = $sqlFieldName . '=' . $database->quote($value);
        }

        if (!is_null($fieldName) && !empty($fieldName) && $value) {
            $sqlFieldNames[] = $sqlFieldName;
            $sqlValues[] = $database->quote($value);
        }
    }



    /**
     * Updates $sqlFieldNames, $sqlValues and $sqlOnDuplicateKey
     * @param ORM_Record $record
     * @param string $fieldName
     * @param ORM_Field $field
     * @param array $sqlFieldNames
     * @param array $sqlValues
     * @param array $sqlOnDuplicateKey
     *
     * @return void
     */
    private function addRecordFieldNameAndValue(ORM_Record $record, $fieldName, $field, &$sqlFieldNames, &$sqlValues, &$sqlOnDuplicateKey)
    {
        $noSaveRecordField = $field instanceof ORM_Operation || !isset($record->$fieldName);

        if ($noSaveRecordField) {
            return;
        }

        $database = $this->getDataBaseAdapter();

        $value = $record->$fieldName;

        if ($field instanceof ORM_RecordSet) {
            $pkFieldName = $field->getPrimaryKey();

            if (null === $value->$pkFieldName) {
                // cela ne doit pas arriver car les record liees sont enregistrer avant d'arriver la
                //trigger_error(sprintf('The joinded field %s.%s contain a record with no ID', get_class($record), $fieldName));

                // quand cela se produit $value->getValues() contient un tableau ou toutes les valeurs sont NULL
                // malgre un passage dans la methode doSave() et que la methode save() a bien ete appelle sur le record et retourne TRUE

                $value = 0;
            } else {

                $value = $value->$pkFieldName;
            }

            $sqlValue = $database->quote($value);

        } else {
            $sqlValue = $field->getSanitizedValue($value, $database);
        }

        //$sqlValue = $database->quote($value);
        $sqlFieldName = $database->backTick($fieldName);

        $sqlFieldNames[] = $sqlFieldName;
        $sqlValues[] = $sqlValue;
        $sqlOnDuplicateKey[] = $sqlFieldName . '=' . $sqlValue;
    }



    /**
     * Computes the sql query for saving the record in the recordset.
     *
     * @param ORM_RecordSet $recordSet
     * @param ORM_Record $record
     *
     * @return NULL|string     The sql query
     */
    public function getSaveQuery(ORM_RecordSet $recordSet, ORM_Record $record)
    {
        $sqlFieldNames = array();
        $sqlValues = array();
        $sqlOnDuplicateKey = array();

        $fields = $recordSet->getFields();
        $database = $this->getDataBaseAdapter();

        $primaryKeyName = $recordSet->getPrimaryKey();
        $primaryKeyField = $recordSet->getPrimaryKeyField();

        $this->addRecordPrimaryKeyFieldNameAndValue($record, $primaryKeyName, $primaryKeyField, $sqlFieldNames, $sqlValues, $sqlOnDuplicateKey);

        unset($fields[$primaryKeyName]);

        foreach ($fields as $fieldName => $field) {
            $this->addRecordFieldNameAndValue($record, $fieldName, $field, $sqlFieldNames, $sqlValues, $sqlOnDuplicateKey);
        }

        if (count($sqlFieldNames) < 1) {
            return null;
        }

        $id = isset($primaryKeyName) ? $record->$primaryKeyName : null;

        $isUpdate = (isset($id) && $id !== '' && !$record->isPrimaryKeyModified());

        if ($isUpdate) {
            return 'UPDATE ' . $this->getTableName($recordSet) .
                ' SET ' . implode(', ', $sqlOnDuplicateKey) .
                ' WHERE ' . $database->backTick($primaryKeyName) . '=' . $database->quote($id) .
                ' LIMIT 1';
        }

        return 'INSERT INTO ' . $this->getTableName($recordSet) . ' (' . implode(', ', $sqlFieldNames) . ')'
            . "\n" . 'VALUES (' . implode(', ', $sqlValues) . ')';
    }


    /**
     * Saves the specified record in the recordSet.
     *
     * @param	ORM_RecordSet	$recordSet		The set on which the query is applied
     * @param	ORM_Record		$record	The record to save.
     * @return	bool						True if successful
     */
    public function doSave(ORM_RecordSet $recordSet, ORM_Record $record)
    {
        $fields = $recordSet->getFields();
        if (count($fields) === 0) {
            return false;
        }

        foreach ($fields as $fieldName => $field) {
            if ($field instanceof ORM_RecordSet) {
                //$fieldName = $field->getName();
                $joinedRecord = $record->$fieldName;



                if (!($joinedRecord instanceof ORM_Record)) {
                    throw new ORM_BackEndSaveException(
                        sprintf(
                            'A joined record is expected on %s, found %s instead',
                            $recordSet->getName().'->'.$fieldName,
                            print_r($joinedRecord, true)
                        ),
                        ''
                    );
                }



                //testSavingForeignKeyOnJoinedSetUpdatedElement()
                $primaryKeyName = $field->getPrimaryKey();
                $previous = $joinedRecord->$primaryKeyName;
                $joinedRecord->save();
                $current = $joinedRecord->$primaryKeyName;

                if ($previous != $current) {
                    $record->setModified(true);
                }
            }
        }

        if (!$record->isModified()) {
            return true;
        }

        $database = $this->oDataBaseAdapter;

        $primaryKeyName = $recordSet->getPrimaryKey();

        $sqlQuery = $this->getSaveQuery($recordSet, $record);
        if (!isset($sqlQuery)) {
            return false;
        }


        //bab_debug($sqlQuery);
        $oResult = $database->db_queryWem($sqlQuery);
        if (false === $oResult) {
            throw new ORM_BackEndSaveException($database->db_error(), $sqlQuery);
        }

        if (!is_null($primaryKeyName)) {
            $newId = $database->db_insert_id();
            if ($newId != 0) {
                $record->initValue($primaryKeyName, $newId);
            }
        }

        $record->setModified(false);

        return true;
    }



    /**
     * Mysql Save
     *
     * @param ORM_RecordSet $recordSet
     * @param ORM_Record $record          The document to save
     *
     * @return bool						  True if successful
     */
    public function save(ORM_RecordSet $recordSet, ORM_Record $record)
    {
        //$this->startLog('save', get_class($recordSet), $record->getValues());
        $result = $this->doSave($recordSet, $record);

        $fields = $recordSet->getFields();

        foreach ($fields as $field) {
            $field->afterSave($record);
        }
        if ($record->isModified()) {
            $result = $this->doSave($recordSet, $record);
        }

        //$this->endLog($result);
        return $result;
    }


    /**
     * Remove records, the number of records depends on criteria
     *
     * @param	ORM_RecordSet	$oSet		The set on which the query is applied
     * @param	ORM_Criteria	$oCriteria	Criteria for selecting records
     *
     * @return bool						True on success, False otherwise
     */
    public function delete(ORM_RecordSet $oSet, ORM_Criteria $oCriteria = null)
    {
        $sFromClause = $this->processFromClause($oSet);

        $sWhereClause = $this->processWhereClause($oCriteria);

        $sQuery = 'DELETE ' . "\n" .
            $this->getSetAlias($oSet) . "\n" .
            $sFromClause . "\n" .
            $sWhereClause;

        //bab_debug($sQuery, 1, 'ORM');
        $res = $this->oDataBaseAdapter->db_queryWem($sQuery);

        if (false === $res) {
            throw new ORM_BackEndDeleteException($this->oDataBaseAdapter->db_error(), $sQuery);
        }

        return $res;
    }



    /**
     * @param ORM_Criteria $oLeftCriteria
     * @param string $sOperator
     * @param ORM_Criteria $oRightCriteria
     * @return string
     */
    private function binaryCriteria(ORM_Criteria $oLeftCriteria, $sOperator, ORM_Criteria $oRightCriteria)
    {
        return '(' . $oLeftCriteria->toString($this) . ' ' . $sOperator . ' ' . $oRightCriteria->toString($this) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::andCriteria()
     */
    public function andCriteria(ORM_Criteria $oLeftCriteria, ORM_Criteria $oRightCriteria)
    {
        return $this->binaryCriteria($oLeftCriteria, 'AND', $oRightCriteria);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::orCriteria()
     */
    public function orCriteria(ORM_Criteria $oLeftCriteria, ORM_Criteria $oRightCriteria)
    {
        return $this->binaryCriteria($oLeftCriteria, 'OR', $oRightCriteria);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::notCriteria()
     */
    public function notCriteria(ORM_Criteria $oCriteria)
    {
        return 'NOT (' . $oCriteria->toString($this) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::greaterThan()
     */
    public function greaterThan(ORM_Field $oField, $sValue)
    {
        return sprintf(
            '%s > %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($sValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::greaterThanOrEqual()
     */
    public function greaterThanOrEqual(ORM_Field $oField, $sValue)
    {
        return sprintf(
            '%s >= %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($sValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::lessThan()
     */
    public function lessThan(ORM_Field $oField, $sValue)
    {
        return sprintf(
            '%s < %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($sValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::lessThanOrEqual()
     */
    public function lessThanOrEqual(ORM_Field $oField, $sValue)
    {
        return sprintf(
            '%s <= %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($sValue)
        );
    }




    /**
     * Get field alias for a subselect
     * @param ORM_RecordSet $criterionSet   RecordSet extracted from the sub select criteria
     * @param array         $mixedValue     Parameters for the IN criterion
     * @param ORM_Field     $oField
     *
     * @return string
     */
    private function getSubSelectAlias(ORM_RecordSet $criterionSet, array $mixedValue, $oField)
    {
    	$fieldname = null;
        if (isset($mixedValue[1])) {
            if ($mixedValue[1] instanceof ORM_FkField) {
                $fieldname = $mixedValue[1]->getName();
                $set = $mixedValue[1]->getParentSet();
            } else {

                $set = $criterionSet;
                $fieldname = $mixedValue[1];

                assert('is_string($fieldname) /* The second parameter of IN must be a string with fieldname used as source of subselect or an FkField. */');
            }
        } else {

            $set = $criterionSet;

            foreach ($set->getFields() as $f) {
                if (($f instanceof ORM_FkField) && $f->getForeignSetName() == get_class($oField->getParentSet())) {

                    $fieldname = $f->getName();
                    break;
                }
            }
        }

		assert('is_string($fieldname) /* The subselect link can not found, you should specified it in the the second parameter of the IN method. */');

        return 't'.$set->getUniqueId().'.'.$fieldname.' AS '.$this->getSetAlias($set).$this->sSeparator.$fieldname;
    }



    /**
     * Create a subselect query
     * @param ORM_Field $oField
     * @param array $mixedValue
     * @return string
     */
    private function subselect(ORM_Field $oField, Array $mixedValue, $inverted = false)
    {
        $criterion = $mixedValue[0];

        $field = $criterion->getField();
        while ($field = $field->getParentSet()) {
            $set = $field;
        }

        $alias = $this->getSubSelectAlias($set, $mixedValue, $oField);

        $query = $this->getSelectStringTables($set, array($alias));
        $query .= $this->processWhereClause($criterion);

        $queryString = $inverted ? '%s NOT IN(%s)' : '%s IN(%s)';
        return sprintf($queryString, $this->fieldAsString($oField), $query);
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::in()
     */
    public function in(ORM_Field $oField, $mixedValue)
    {
        if (isset($mixedValue[0]) && (($mixedValue[0] instanceof ORM_CriterionBase) || ($mixedValue[0] instanceof ORM_BinaryCriteria))) {
            return $this->subselect($oField, $mixedValue);
        }

        if (is_array($mixedValue) && 0 === count($mixedValue)) {
            return ' FALSE ';
        }

        return sprintf(
            '%s IN(%s)',
            $this->fieldAsString($oField),
            $this->oDataBaseAdapter->quote($mixedValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::is()
     */
    public function is(ORM_Field $oField, $mixedValue)
    {
        if ($oField instanceof ORM_Criterion) {
            return sprintf(
                '%s = %s',
                $this->fieldAsString($oField),
                $this->fieldAsString($mixedValue)
            );
        }
        if ($mixedValue === null) {
            return sprintf('%s IS NULL', $this->fieldAsString($oField));
        }

        if ($mixedValue === false) {
            $mixedValue = '0';
        }


        return sprintf(
            '%s = %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($mixedValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::isNot()
     */
    public function isNot(ORM_Field $oField, $mixedValue)
    {
        return 'NOT (' . $this->is($oField, $mixedValue) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::notIn()
     */
    public function notIn(ORM_Field $oField, $mixedValue)
    {
        if (isset($mixedValue[0]) && (($mixedValue[0] instanceof ORM_CriterionBase) || ($mixedValue[0] instanceof ORM_BinaryCriteria))) {
            return $this->subselect($oField, $mixedValue, true);
        }
        
        if (is_array($mixedValue) && 0 === count($mixedValue)) {
            return ' TRUE ';
        }
        
        return sprintf(
            '%s NOT IN(%s)',
            $this->fieldAsString($oField),
            $this->oDataBaseAdapter->quote($mixedValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::notLike()
     */
    public function notLike(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' NOT LIKE \''
            . $this->oDataBaseAdapter->db_escape_like($sValue) . '\' ';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::soundsLike()
     */
    public function soundsLike(ORM_Field $oField, $sValue)
    {
        return sprintf(
            '%s SOUNDS LIKE %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($sValue)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::like()
     */
    public function like(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' LIKE \''
            . $this->oDataBaseAdapter->db_escape_like($sValue) . '\' ';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::startsWith()
     */
    public function startsWith(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' LIKE \''
            . $this->oDataBaseAdapter->db_escape_like($sValue) . '%\' ';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::endsWith()
     */
    public function endsWith(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' LIKE \'%'
            . $this->oDataBaseAdapter->db_escape_like($sValue) . '\' ';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::contains()
     */
    public function contains(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' LIKE \'%'
            . $this->oDataBaseAdapter->db_escape_like($sValue) . '%\' ';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::containsWords()
     */
    public function containsWords(ORM_Field $oField, $sValue)
    {
        return $this->fieldAsString($oField) . ' REGEXP \'[[:<:]]('
            . $this->oDataBaseAdapter->db_escape_string(preg_quote($sValue))
            . ')[[:>:]]\'';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::hasValue()
     */
    public function hasValue(ORM_Field $oField, $sValue)
    {
        return sprintf(
            "FIND_IN_SET(%s, %s)",
            $this->fieldAsString($sValue),
            $this->fieldAsString($oField)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::between()
     */
    public function between(ORM_Field $oField, $min, $max)
    {
        return sprintf(
            '%s BETWEEN %s AND %s',
            $this->fieldAsString($oField),
            $this->fieldAsString($min),
            $this->fieldAsString($max)
        );
    }

    /**
     *
     * @return string
     */
    public function alwaysTrue()
    {
        return 'TRUE';
    }


    /**
     *
     * @return string
     */
    public function alwaysFalse()
    {
        return 'FALSE';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::fieldIsNull()
     */
    public function fieldIsNull(ORM_Field $oField)
    {
        return $this->fieldAsString($oField) . ' IS NULL';
    }
    //----------------


    /**
     * Field as string for operations
     *
     * @param	mixed	$oField
     * @return string
     */
    private function fieldAsString($oField)
    {
        if ($oField instanceof ORM_Operation) {
            return $oField->toString($this);
        }
        if ($oField instanceof ORM_Field) {
            return $this->getFieldAlias($oField);
        }
        if ($oField instanceof ORM_Criterion) {
            $field = $oField->getField();
            return $this->subselect($field, array($oField));
        }
        return $this->oDataBaseAdapter->quote((string) $oField);
    }


    //

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::isNullOperation()
     */
    public function isNullOperation(ORM_IsNullOperation $operation)
    {
        return 'ISNULL(' . $this->fieldAsString($operation->getValue()). ')';
    }



    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::ifNullOperation()
     */
    public function ifNullOperation(ORM_IfNullOperation $operation)
    {
        return 'IFNULL(' . $this->fieldAsString($operation->getValue())
        . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::notOperation()
     */
    public function notOperation(ORM_NotOperation $operation)
    {
        return '!(' . $this->fieldAsString($operation->getValue()). ')';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::orOperation()
     */
    public function orOperation(ORM_OrOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue())
            . ' || ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::andOperation()
     */
    public function andOperation(ORM_AndOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue())
            . ' && ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::equalsOperation()
     */
    public function equalsOperation(ORM_EqualsOperation $operation)
    {
        return '(' . $this->in($operation->getValue(), $operation->getRightValue()) . ')';
    }

    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::inOperation()
     */
    public function inOperation(ORM_InOperation $operation)
    {
        return '(' . $this->in($operation->getValue(), $operation->getRightValue()) . ')';
    }



    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::ifEqualsOperation()
     */
    public function ifEqualsOperation(ORM_IfEqualsOperation $operation)
    {
        return 'IF (' . $this->is($operation->getValue(), $operation->getExpression()) . ',' . $this->fieldAsString($operation->getTrueValue()) . ',' . $this->fieldAsString($operation->getFalseValue()) . ')';
    }


    /**
     * {@inheritDoc}
     * @see ORM_BackEnd::criterionOperation()
     */
    public function criterionOperation(ORM_CriterionOperation $operation)
    {
        return '(' . $operation->getValue()->toString($this) . ')'; //$this->fieldAsString($operation->getValue());
    }

    // Aggregate operations usable with groupBy.
    ////////////////////////////////////////////

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::countOperation()
     */
    public function countOperation(ORM_CountOperation $operation)
    {
        $distinct = $operation->getDistinct() ? 'DISTINCT ' : '';
        return 'COUNT(' . $distinct . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::maxOperation()
     */
    public function maxOperation(ORM_MaxOperation $operation)
    {
        return 'MAX(' . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::minOperation()
     */
    public function minOperation(ORM_MinOperation $operation)
    {
        return 'MIN(' . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::sumOperation()
     */
    public function sumOperation(ORM_SumOperation $operation)
    {
        return 'SUM(' . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::averageOperation()
     */
    public function averageOperation(ORM_AverageOperation $operation)
    {
        return 'AVG(' . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::groupConcatOperation()
     */
    public function groupConcatOperation(ORM_GroupConcatOperation $operation)
    {
        $fieldName = $this->fieldAsString($operation->getValue());
        $distinct = $operation->getDistinct() ? 'DISTINCT' : '';
        $order = 'ORDER BY ' . $fieldName . ' ASC ';
        $separator = 'SEPARATOR \'' . $operation->getSeparator() . '\'';

        return 'GROUP_CONCAT(' . $distinct . ' ' .  $fieldName . ' ' . $order . ' ' . $separator . ')';
    }


    // String manipulation operations.
    //////////////////////////////////

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::lengthOperation()
     */
    public function lengthOperation(ORM_LengthOperation $operation)
    {
        return 'LENGTH(' . $this->fieldAsString($operation->getValue()). ')';
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::toUpperOperation()
     */
    public function toUpperOperation(ORM_ToUpperOperation $operation)
    {
        return 'UPPER(' . $this->fieldAsString($operation->getValue()). ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::toLowerOperation()
     */
    public function toLowerOperation(ORM_ToLowerOperation $operation)
    {
        return 'LOWER(' . $this->fieldAsString($operation->getValue()). ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::rightOperation()
     */
    public function rightOperation(ORM_RightOperation $operation)
    {
        return 'RIGHT(' . $this->fieldAsString($operation->getValue())
            . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::leftOperation()
     */
    public function leftOperation(ORM_LeftOperation $operation)
    {
        return 'LEFT(' . $this->fieldAsString($operation->getValue())
            . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::leftPadOperation()
     */
    public function leftPadOperation(ORM_LeftPadOperation $operation)
    {
        return 'LPAD(' . $this->fieldAsString($operation->str)
            . ', ' . $this->fieldAsString($operation->length)
            . ', ' . $this->fieldAsString($operation->padStr) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::concatOperation()
     */
    public function concatOperation(ORM_ConcatOperation $operation)
    {
        $parameters = array();
        $values = $operation->getValue();
        foreach ($values as $field) {
            $parameters[] = $this->fieldAsString($field);
        }

        return 'CONCAT(' . implode(', ', $parameters) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::strCmpOperation()
     */
    public function strCmpOperation(ORM_StrCmpOperation $operation)
    {
        return 'STRCMP(' . $this->fieldAsString($operation->getValue())
            . ',' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::trimOperation()
     */
    public function trimOperation(ORM_TrimOperation $operation)
    {
        $value = $operation->getValue();
        $remstr = $operation->getRemoveStr();

        if (' ' === $remstr) {
            $remstr ='';
        } else {
            $remstr =''.$this->oDataBaseAdapter->quote($remstr).' FROM ';
        }
        switch ($operation->getWhere()) {
            case ORM_TrimOperation::LEADING:
                $where = 'LEADING ';
                break;
            case ORM_TrimOperation::TRAILING:
                $where = 'TRAILING ';
                break;
            default:
            case ORM_TrimOperation::BOTH:
                $where = '';
                break;
        }

        return sprintf(
            'TRIM(%s%s%s)',
            $where,
            $remstr,
            $this->fieldAsString($value)
        );
    }


    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::replaceOperation()
     */
    public function replaceOperation(ORM_ReplaceOperation $operation)
    {
        $value = $operation->getValue();
        $fromStr = $operation->getFromStr();
        $toStr = $operation->getToStr();

        return sprintf(
            'REPLACE(%s, %s, %s)',
            $this->fieldAsString($value),
            $this->oDataBaseAdapter->quote($fromStr),
            $this->oDataBaseAdapter->quote($toStr)
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::findInSetOperation()
     */
    public function findInSetOperation(ORM_FindInSetOperation $operation)
    {
        return sprintf(
            "FIND_IN_SET(%s,'%s')",
            $this->fieldAsString($operation->getValue()),
            $this->oDataBaseAdapter->db_escape_string(implode(',', $operation->getList()))
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::fieldOperation()
     */
    public function fieldOperation(ORM_FieldOperation $operation)
    {
        return sprintf(
            "FIELD(%s,%s)",
            $this->fieldAsString($operation->getValue()),
            $this->oDataBaseAdapter->quote($operation->getList())
        );
    }

    /**
     * @param ORM_FindOperation $operation
     */
    public function findOperation(ORM_FindOperation $operation)
    {
        return sprintf(
            "FIND_IN_SET(%s, %s)",
            $this->fieldAsString($operation->getKeyValue()),
            $this->fieldAsString($operation->getValue())
        );
    }


    // Arithmetic operations.
    /////////////////////////

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::radiansOperation()
     */
    public function radiansOperation(ORM_RadiansOperation $operation)
    {
        return 'RADIANS(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::cosOperation()
     */
    public function cosOperation(ORM_CosOperation $operation)
    {
        return 'COS(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::acosOperation()
     */
    public function acosOperation(ORM_AcosOperation $operation)
    {
        return 'ACOS(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::sinOperation()
     */
    public function sinOperation(ORM_SinOperation $operation)
    {
        return 'SIN(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::asinOperation()
     */
    public function asinOperation(ORM_AsinOperation $operation)
    {
        return 'ASIN(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * Returns the tangent of X, where X is given in radians.
     * @param ORM_TanOperation $operation
     * @return string
     */
    public function tanOperation(ORM_TanOperation $operation)
    {
        return 'TAN(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * Returns the arc tangent of the two variables
     *
     * @param  ORM_AtanOperation $operation
     * @return string
     */
    public function atanOperation(ORM_AtanOperation $operation)
    {
        return 'ATAN(' . $this->fieldAsString($operation->getValue()).')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::roundOperation()
     */
    public function roundOperation(ORM_RoundOperation $operation)
    {
        return 'ROUND(' . $this->fieldAsString($operation->getValue())
          . ', ' . $operation->getDecimals().')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::absOperation()
     */
    public function absOperation(ORM_AbsOperation $operation)
    {
        return 'ABS(' . $this->fieldAsString($operation->getValue()). ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::addOperation()
     */
    public function addOperation(ORM_AddOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue())
          . ' + ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::subtractOperation()
     */
    public function subtractOperation(ORM_SubtractOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue())
          . ' - ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::multiplyOperation()
     */
    public function multiplyOperation(ORM_MultiplyOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue()) . ' * ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::divideOperation()
     */
    public function divideOperation(ORM_DivideOperation $operation)
    {
        return '(' . $this->fieldAsString($operation->getValue()) . ' / ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }


    // Date/time operations.
    /////////////////////////

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::dateDiffOperation()
     */
    public function dateDiffOperation(ORM_DateDiffOperation $operation)
    {
        return 'DATEDIFF(' . $this->fieldAsString($operation->getValue()) . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::timeDiffOperation()
     */
    public function timeDiffOperation(ORM_TimeDiffOperation $operation)
    {
        return 'TIMEDIFF(' . $this->fieldAsString($operation->getValue()) . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::weekOperation()
     */
    public function weekOperation(ORM_WeekOperation $operation)
    {
        return 'WEEK('.$this->fieldAsString($operation->getValue()).', 3)';
    }

    /**
     * (non-PHPdoc)
     * @see ORM_BackEnd::randOperation()
     */
    public function randOperation(ORM_RandOperation $operation)
    {
        $value = $operation->getValue();
        if (isset($value)) {
            return 'RAND(' . $this->fieldAsString($value). ')';
        }
        return 'RAND()';
    }

    // XML manipulation operations.
    ///////////////////////////////

    /**
     * @param ORM_ExtractValueOperation $operation
     * @return string
     */
    public function extractValueOperation(ORM_ExtractValueOperation $operation)
    {
        return 'ExtractValue(' . $this->fieldAsString($operation->getValue()) . ', ' . $this->fieldAsString($operation->getRightValue()) . ')';
    }

    //----------------



    protected function allowNull(ORM_Field $oField)
    {
        if ($oField->isNullAllowed()) {
            return '';
        }
        return ' NOT NULL';
    }

    /**
     * Convert to SQL a index key or unique key on a list of field
     *
     * @param ORM_FieldKey $key
     * @return string
     */
    protected function fieldKeyToSql(ORM_FieldKey $key)
    {
        if ($key->unique) {
            $sql = 'UNIQUE ';
        } else {
            $sql = '';
        }

        $sql .= 'KEY '.$this->oDataBaseAdapter->backTick($key->getName());

        $params = array();
        foreach ($key->getFields() as $field) {
            $params[] = $this->oDataBaseAdapter->backTick($field->getName());
        }

        $sql .= ' ('.implode(', ', $params).')';
        return $sql;
    }


    /**
     * @param ORM_PkField $oField
     * @param string $type
     * @return string
     */
    protected function pkFieldToSql(ORM_PkField $oField, $type = 'INT(11)')
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' '.$type;
        $sSql .= $this->allowNull($oField);

        if ($oField->isAutoIncremented()) {
            $sSql .= ' AUTO_INCREMENT';
        }

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_PkField $oField
     * @return string
     */
    protected function pkStringFieldToSql(ORM_PkField $oField)
    {
        return $this->pkFieldToSql($oField, 'VARCHAR(255)');
    }


    /**
     * @param ORM_IntField $oField
     * @return string
     */
    protected function intFieldToSql(ORM_IntField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' INT(11)';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0';

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_UserField $oField
     * @return string
     */
    protected function userFieldToSql(ORM_UserField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' INT(11)';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0';

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_GroupField $oField
     * @return string
     */
    protected function groupFieldToSql(ORM_GroupField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' INT(11)';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0';

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_BoolField $oField
     * @return string
     */
    protected function boolFieldToSql(ORM_BoolField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' TINYINT(1)';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0';

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_DecimalField $oField
     * @return string
     */
    protected function decimalFieldToSql(ORM_DecimalField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' DECIMAL('.$oField->getPrecision().','.$oField->getDecimalPlaces().')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0.'.str_repeat('0', $oField->getDecimalPlaces());

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_CurrencyField $oField
     * @return string
     */
    protected function currencyFieldToSql(ORM_CurrencyField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' DECIMAL('.$oField->getPrecision().','.$oField->getDecimalPlaces().')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT 0.'.str_repeat('0', $oField->getDecimalPlaces());

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_DateField $oField
     * @return string
     */
    protected function dateFieldToSql(ORM_DateField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' DATE';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . ($oField->isNullAllowed() ? 'NULL' : $this->oDataBaseAdapter->quote('0000-00-00'));

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_TimeField $oField
     * @return string
     */
    protected function timeFieldToSql(ORM_TimeField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' TIME';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . ($oField->isNullAllowed() ? 'NULL' : $this->oDataBaseAdapter->quote('00:00:00'));

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_DatetimeField $oField
     * @return string
     */
    protected function datetimeFieldToSql(ORM_DatetimeField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' DATETIME';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . ($oField->isNullAllowed() ? 'NULL' : $this->oDataBaseAdapter->quote('0000-00-00 00:00:00'));

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * @param ORM_StringField $oField
     * @return string
     */
    protected function stringFieldToSql(ORM_StringField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' VARCHAR(' . $oField->getMaxLength() . ')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . $this->oDataBaseAdapter->quote('');

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * URL field to SQL
     * @param ORM_UrlField $oField
     * @return string
     */
    protected function urlFieldToSql(ORM_UrlField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' VARCHAR(' . $oField->getMaxLength() . ')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . $this->oDataBaseAdapter->quote('');

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * File field to SQL
     * @param ORM_FileField $oField
     * @return string
     */
    protected function fileFieldToSql(ORM_FileField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' VARCHAR(' . $oField->getMaxLength() . ')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . $this->oDataBaseAdapter->quote('');

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * File field to SQL
     * @param ORM_FileField $oField
     * @return string
     */
    protected function imageFieldToSql(ORM_FileField $oField)
    {
        return $this->fileFieldToSql($oField);
    }

    /**
     * Email field to SQL
     * @param ORM_EmailField $oField
     * @return string
     */
    protected function emailFieldToSql(ORM_EmailField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' VARCHAR(' . $oField->getMaxLength() . ')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . $this->oDataBaseAdapter->quote('');

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * Phone field to SQL
     * @param ORM_PhoneField $oField
     * @return string
     */
    protected function phoneFieldToSql(ORM_PhoneField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' VARCHAR(' . $oField->getMaxLength() . ')';
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT ' . $this->oDataBaseAdapter->quote('');

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * Text field to SQL
     * @param ORM_TextField $oField
     * @return string
     */
    protected function textFieldToSql(ORM_TextField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' TEXT';
        $sSql .= $this->allowNull($oField);

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * Text field to SQL
     * @param ORM_TextField $oField
     * @return string
     */
    protected function areaFieldToSql(ORM_TextField $oField)
    {
        return $this->textFieldToSql($oField);
    }


    /**
     * Multilang text field to SQL
     * @param ORM_MultilangTextField $oField
     * @return string
     */
    protected function multilangTextFieldToSql(ORM_MultilangTextField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' TEXT';
        $sSql .= $this->allowNull($oField);

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * HTML field to SQL
     * @param ORM_HtmlField $oField
     * @return string
     */
    protected function htmlFieldToSql(ORM_HtmlField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' TEXT';
        $sSql .= $this->allowNull($oField);

        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }

    /**
     * Enum field to SQL
     * @param ORM_EnumField $oField
     * @return string
     */
    protected function enumFieldToSql(ORM_EnumField $oField)
    {
        $type = 'INT(11)';
        $default = '0';

        $valuePairs = array();
        foreach ($oField->getValues() as $key => $value) {
            $valuePairs[] = $key . ':' . $value;

            if ('INT(11)' === $type && !is_numeric($key)) {
                $type = 'VARCHAR(255)';
                $default = $this->oDataBaseAdapter->quote('');
            }
        }


        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' '.$type;
        $sSql .= $this->allowNull($oField);
        $sSql .= ' DEFAULT '.$default;

        if ($oField->getDescription()) {
            $comment = $oField->getDescription() . ' (' .implode(',', $valuePairs). ')';

            if (strlen($comment) > 255) {
                $comment = substr($comment, 0, 255);
            }

            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($comment);
        }
        return $sSql;
    }


    /**
     * Set field to SQL
     * @param ORM_SetField $oField
     * @return string
     */
    protected function setFieldToSql(ORM_SetField $oField)
    {
        $values = $oField->getValues();
        $keyValues = array();
        foreach (array_keys($values) as $value) {
            $keyValues[] = $this->oDataBaseAdapter->quote($value);
        }

        $type = 'SET(' . implode(',', $keyValues) . ')';

        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' '.$type;
        $sSql .= $this->allowNull($oField);
//	    $sSql .= ' DEFAULT '.$default;

        if ($oField->getDescription()) {
            $comment = $oField->getDescription() . ' (' .implode(',', $values). ')';

            if (strlen($comment) > 255) {
                $comment = substr($comment, 0, 255);
            }

            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($comment);
        }
        return $sSql;
    }



    /**
     * Foreign key field to SQL
     * @param ORM_FkField $oField
     * @return string
     */
    protected function fkFieldToSql(ORM_FkField $oField)
    {
        $fkFUnction = $oField->getForeignKeyType();
        $fkField = $fkFUnction($oField->getName());
        $fkField->setDescription($oField->getDescription());
        return $this->fieldToSql($fkField);
    }


    /**
     * Get SQL string for a field
     * @param ORM_Field $oField
     * @return string
     */
    protected function fieldToSql(ORM_Field $oField)
    {
        $fieldClassName = get_class($oField);
        $methodName = substr($fieldClassName, strlen('ORM_')) . 'ToSql';
        return $this->$methodName($oField);
    }



    /**
     * Return the sql script to create the SQL table for the Set.
     *
     * @return string
     */
    public function setToSql(ORM_RecordSet $oSet)
    {
        $sSql = 'CREATE TABLE ' . $this->oDataBaseAdapter->backTick($this->getTableName($oSet)) . ' (' . "\n";
        $foreign = array();
        $rows = array();

        foreach ($oSet->getFields() as $oField) {
            if ($oField instanceof ORM_Operation) {
                continue;
            }

            $rows[] = $this->fieldToSql($oField);

            if ($oField instanceof ORM_fkField) {
                $fktable = $this->computeTableName($oField->getForeignSetName());

                $constraintName = $this->oDataBaseAdapter->backtick($this->getTableName($oSet) . '_' . $oField->getName());
                $foreign[] = ' ADD CONSTRAINT ' . $constraintName . '
                    FOREIGN KEY ( '.$this->oDataBaseAdapter->backtick($oField->getName()).' )
                    REFERENCES ' . $this->oDataBaseAdapter->backtick($fktable) . '
                        (' . $this->oDataBaseAdapter->backtick($oField->getParentSet()->getPrimaryKey()) . ')';
            }
        }


        if (null !== $pkname = $oSet->getPrimaryKey()) {
            $rows[] = 'PRIMARY KEY (' . $this->oDataBaseAdapter->backTick($pkname) . ')';
        }

        foreach ($oSet->getFieldKeys() as $fieldKey) {
            $rows[] = $this->fieldKeyToSql($fieldKey);
        }

        $sSql .= implode(",\n", $rows);
        $sSql .= ')';


        if ($comment = $oSet->getDescription()) {
            // parenthesis in comment break the bab_synchronizeSql detection of fields
            // TODO: Fix bab_synchronizeSql before removing str_replace
            $comment = str_replace(array('(', ')'), '', $comment);
            $sSql .= ' COMMENT=' . $this->oDataBaseAdapter->quote($comment);
        }


        $sSql .= ';' . "\n";

        if ($foreign) {
            $sSql .= 'ALTER TABLE '.$this->oDataBaseAdapter->backTick($this->getTableName($oSet))." \n";
            $sSql .= implode(",\n ", $foreign) . ";\n";
        }

        return $sSql;
    }

    /**
     * @param ORM_SitemapItemField $oField
     * @return string
     */
    protected function sitemapItemFieldToSql(ORM_SitemapItemField $oField)
    {
        return $this->stringFieldToSql($oField);
    }

    /**
     * @param ORM_RruleField $oField
     * @return string
     */
    protected function rruleFieldToSql(ORM_RruleField $oField)
    {
        return $this->stringFieldToSql($oField);
    }
    
    
    /**
     * Medium text field to SQL
     * @param ORM_MediumTextField $oField
     * @return string
     */
    protected function mediumTextFieldToSql(ORM_MediumTextField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' MEDIUMTEXT';
        $sSql .= $this->allowNull($oField);
        
        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }
    
    /**
     * Long text field to SQL
     * @param ORM_LongTextField $oField
     * @return string
     */
    protected function longTextFieldToSql(ORM_LongTextField $oField)
    {
        $sSql = $this->oDataBaseAdapter->backtick($oField->getName()) . ' LONGTEXT';
        $sSql .= $this->allowNull($oField);
        
        if ($oField->getDescription()) {
            $sSql .= ' COMMENT ' . $this->oDataBaseAdapter->quote($oField->getDescription());
        }
        return $sSql;
    }
}
