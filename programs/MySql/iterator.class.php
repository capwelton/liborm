<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/../iterator.class.php';
require_once dirname(__FILE__) . '/backend.class.php';
require_once dirname(__FILE__) . '/set.class.php';

class ORM_MySqlIterator extends ORM_Iterator implements SeekableIterator, Countable
{

    /**
     * The mysql resource
     *
     * @var mysqli_result|false mysql resource
     */
    private $oResult = null;

    /**
     * The current object to which the iterator is pointing.
     *
     * @var ORM_Record
     */
    private $oObject = null;

    /**
     * Index position of the mysql resource
     *
     * @var integer Used in the Iterator implementation
     */
    private $iKey = 0;


    /**
     * The database object.
     *
     * @var babDatabase
     */
    private $oDataBaseAdapter = null;


    /**
     * Creates a new ORM_MySqlIterator object.
     *
     * @param babDatabase $oDataBaseAdapter
     */
    public function __construct($oDataBaseAdapter)
    {
        parent::__construct();
        $this->setDataBaseAdapter($oDataBaseAdapter);
    }


    // Iterator interface function implementation

    /**
     * Return the current element
     *
     * @return ORM_Record
     */
    public function current()
    {
        return $this->oObject;
    }


    /**
     * Return the key of the current element
     *
     * @return integer The key of the current element
     */
    public function key()
    {
        return $this->iKey;
    }


    /**
     * Go to the next element
     */
    public function next()
    {
        if (false !== ($aData = $this->oDataBaseAdapter->db_fetch_assoc($this->oResult))) {
            $this->iKey ++;
            $this->oObject = $this->getObject($aData);
        } else {
            $this->oObject = null;
            $this->iKey = ORM_EOF;
        }
    }


    /**
     * Rewind the Iterator to the first element.
     */
    public function rewind()
    {
        $this->seek(0);
    }


    /**
     * Place the Iterator on the $iRowNumberth element, starting at 0.
     *
     * @param int $iRowNumber
     */
    public function seek($iRowNumber)
    {
        $this->select();

        if ($this->oDataBaseAdapter->db_num_rows($this->oResult) !== 0
            && false !== @$this->oDataBaseAdapter->db_data_seek($this->oResult, $iRowNumber)
            && false !== ($aData = $this->oDataBaseAdapter->db_fetch_assoc($this->oResult))
        ) {
            $this->iKey = $iRowNumber;
            $this->oObject = $this->getObject($aData);
        } else {
            $this->oObject = null;
            $this->iKey = ORM_EOF;
        }
    }


    /**
     * Check if there is a current element after calls to rewind(), seek() or next().
     *
     * @return bool True if there is a current element after calls to rewind(), seek() or next()
     */
    public function valid()
    {
        return (! is_null($this->oObject));
    }


    /**
     * Returns the number of elements in the iterator.
     *
     * @return int
     */
    public function count()
    {
        $this->select();
        return $this->oDataBaseAdapter->db_num_rows($this->oResult);
    }



    /**
     * Sets the database adapter.
     *
     * @param babDatabase $oDataBaseAdapter
     */
    public function setDataBaseAdapter($oDataBaseAdapter)
    {
        $this->oDataBaseAdapter = $oDataBaseAdapter;
    }


    /**
     * Returns the database adapter.
     *
     * @return babDatabase
     */
    public function getDataBaseAdapter()
    {
        return $this->oDataBaseAdapter;
    }



    /**
     * Initializes the mysql result resource.
     *
     * @throws ORM_BackEndSelectException
     */
    private function select()
    {
        if (is_null($this->oResult)) {
            $sQuery = $this->oSet->getSelectQuery($this->oCriteria, $this->aOrder, $this->aGroupBy, $this->sLimit);
            $this->oResult = $this->oDataBaseAdapter->db_queryWem($sQuery);

            if (false === $this->oResult) {
                $exception = new ORM_BackEndSelectException($this->oDataBaseAdapter->db_error(), $sQuery);
                //bab_debug($exception->__toString());
                throw $exception;
            }
            // $this->freeOrmLinks();
        }
    }


    /**
     * Returns the MySql query string corresponding to the current state of the iterator.
     *
     * @return string
     */
    public function getSelectQuery()
    {
        return $this->oSet->getSelectQuery($this->oCriteria, $this->aOrder, $this->aGroupBy, $this->sLimit);
    }



    /**
     *
     * @param string $sTableAlias
     * @return ORM_Record
     */
    private function getRecordFromTableAlias($sTableAlias)
    {
        $recordSet = $this->oSet;
        $backend = $recordSet->getBackend();
        $aTableAlias = $backend->getSetTableAliasDefinition($recordSet);
        if (array_key_exists($sTableAlias, $aTableAlias)) {
            return $aTableAlias[$sTableAlias]->newRecord();
        }
        return null;
    }


    /**
     *
     * @param string $sTableAlias
     * @param ORM_Record $oRecord
     * @param array $aTable
     */
    private function setRecordValue($sTableAlias, $oRecord, $aTable)
    {
        $recordSet = $this->oSet;
        $backend = $recordSet->getBackend();
        $aTableAlias = $backend->getSetTableAliasDefinition($recordSet);

        foreach ($aTable[$sTableAlias] as $sFieldName => $sFieldValue) {
            $oField = $aTableAlias[$sTableAlias]->getField($sFieldName);
            if ($oField instanceof ORM_FkField) {
                $sFieldValue = (int)$sFieldValue;
            }
            if (! ($oField instanceof ORM_RecordSet)) {
                $oRecord->initValue($sFieldName, $sFieldValue);
            } else {
                $oNewRecord = $this->getRecordFromTableAlias(
                    $backend->getSetAlias($oField)
                );
                if (! is_null($oNewRecord)) {
                    $this->setRecordValue(
                        $backend->getSetAlias($oField),
                        $oNewRecord,
                        $aTable
                    );
                    $oRecord->initValue($sFieldName, $oNewRecord);
                } else {
                    $oRecord->initValue($sFieldName, $sFieldValue);
                }
            }
        }
    }


    /**
     * Returns a new ORM_Record corresponding to the associative array $aDatas.
     *
     * @param array $aDatas
     * @return ORM_Record
     */
    private function getObject($aDatas)
    {
        $aTable = array();

        $iTableAliasIdx = 0;
        $iFieldAliasIdx = 1;

        foreach ($aDatas as $sKey => $sValue) {
            $aFieldInfo = explode($this->oSet->getBackend()->getSeparator(), $sKey);
            if (false !== $aFieldInfo && 2 === count($aFieldInfo)) {
                // TODO: Add a test to know if the field (name) belong to the set, maybe this test must be done in the function setRecordValue
                $aTable[$aFieldInfo[$iTableAliasIdx]][$aFieldInfo[$iFieldAliasIdx]] = $sValue;
            }
        }

        $oRecord = null;
        if (count($aTable) > 0) {
            reset($aTable);
            $aItem = each($aTable);
            if (false !== $aItem) {
                $sTableAlias = $aItem['key'];

                $oRecord = $this->getRecordFromTableAlias($sTableAlias);

                if (! is_null($oRecord)) {
                    $this->setRecordValue($sTableAlias, $oRecord, $aTable);
                }
            }
        }
        return $oRecord;
    }


    /**
     * (non-PHPdoc)
     *
     * @see programs/ORM_Iterator#orderAsc($oField)
     * @return ORM_MySqlIterator
     */
    public function orderAsc(ORM_Field $oField)
    {
        if (isset($this->oResult)) {
            throw new ORM_BackEndException('Cannot modify an already initialized iterator.', '');
        }
        return parent::orderAsc($oField);
    }


    /**
     * (non-PHPdoc)
     *
     * @see programs/ORM_Iterator#orderRand()
     * @return ORM_MySqlIterator
     */
    public function orderRand()
    {
        if (isset($this->oResult)) {
            throw new ORM_BackEndException('Cannot modify an already initialized iterator.', '');
        }
        return parent::orderRand();
    }


    /**
     * (non-PHPdoc)
     *
     * @see programs/ORM_Iterator#orderDesc($oField)
     * @return ORM_MySqlIterator
     */
    public function orderDesc(ORM_Field $oField)
    {
        if (isset($this->oResult)) {
            throw new ORM_BackEndException('Cannot modify an already initialized iterator.', '');
        }
        return parent::orderDesc($oField);
    }


    /**
     * (non-PHPdoc)
     *
     * @see programs/ORM_Iterator#groupBy($oField)
     * @return ORM_MySqlIterator
     */
    public function groupBy(ORM_Field $oField)
    {
        if (isset($this->oResult)) {
            throw new ORM_BackEndException('Cannot modify an already initialized iterator.', '');
        }
        return parent::groupBy($oField);
    }


    /**
     * (non-PHPdoc)
     *
     * @see programs/ORM_Iterator#setCriteria($oCriteria)
     * @return ORM_MySqlIterator
     */
    public function setCriteria(ORM_Criteria $oCriteria = null)
    {
        if (isset($this->oResult)) {
            throw new ORM_BackEndException('Cannot modify an already initialized iterator.', '');
        }
        return parent::setCriteria($oCriteria);
    }


    /**
     * (non-PHPdoc)
     *
     * @see ORM_Iterator::__destruct()
     */
    public function __destruct()
    {
        parent::__destruct();

        $this->oResult = null;
        $this->oObject = null;
        $this->oDataBaseAdapter = null;
    }
}
