<?php

require_once dirname(__FILE__) . '/mock/core.php';



class ORM_RecordSetTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test adding two ORM_Field to a record set using addFields().
     */
    public function testAddFields()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $nameField = $set->getField('name');

        $ageField = $set->getField('age');

        $this->assertSame($field1, $nameField);
        $this->assertSame($field2, $ageField);
    }


    /**
     * Test adding a non ORM_Field to a record set using addFields().
     *
     * @expectedException ORM_IllegalArgumentException
     *                  Adding a non ORM_Field to a record set using addFields()
     *                  should throw an ORM_IllegalArgumentException.
     */
    public function testInvalidAddFields()
    {
        $set = new ORM_TestRecordSet();
        $field = 'myStringField';
        $set->addFields($field);
    }

    /**
     * Test adding a non ORM_Field to a record set using addFields().
     *
     * @expectedException ORM_IllegalArgumentException
     *                  Adding a non ORM_Field to a record set using addFields()
     *                  must throw an ORM_IllegalArgumentException.
     */
    public function testAddFieldsWithInvalidName()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField(null, 255);
        $set->addFields($field1);

    }

    /**
     * Test using isset() to check the existence of record set fields.
     */
    public function testIsset()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $nameFieldIsset = isset($set->name);
        $ageFieldIsset = isset($set->age);
        $nonExistentFieldIsset = isset($set->nonExistent);

        $this->assertTrue(
            $nameFieldIsset,
            'Using isset() on a field must return true.'
        );
        $this->assertTrue(
            $ageFieldIsset,
            'Using isset() on a field must return true.'
        );
        $this->assertFalse(
            $nonExistentFieldIsset,
            'Using isset() on a non-existent field must return false.'
        );
    }


    /**
     * Test accessing a field from a record set using getField().
     */
    public function testGetField()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $nameField = $set->getField('name');

        $this->assertEquals($nameField, $field1);
    }



    /**
     * Test accessing a field from a record set using __get().
     */
    public function testGetFieldUsingMagicGetMethod()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $nameField = $set->name;

        $this->assertEquals($nameField, $field1);
    }



    /**
     * Test accessing a field from a record set getFieldByPath().
     */
    public function testGetFieldUsingGetFieldByPath()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);

        $set->addFields($field1);
        $set->hasOne('otherTest', 'ORM_RecordSet');

        $nameField = $set->getFieldByPath('name');

        $this->assertEquals($nameField, $field1);
    }


    /**
     * Test accessing a joined nested field from a record set getFieldByPath().
     */
    public function testGetJoinedNestedFieldUsingGetFieldByPath()
    {
        $recordSet = new ORM_MockSet();
        $recordSet->parent();
        $recordSet->parent->parent();
        $recordSet->parent->parent->parent();

        $field = $recordSet->getFieldByPath('string');

        $this->assertEquals($field, $recordSet->string);

        $field = $recordSet->getFieldByPath('parent/string');

        $this->assertEquals($field, $recordSet->parent->string);

        $field = $recordSet->getFieldByPath('parent/parent/string');

        $this->assertEquals($field, $recordSet->parent->parent->string);

        $field = $recordSet->getFieldByPath('parent/parent/parent/string');

        $this->assertEquals($field, $recordSet->parent->parent->parent->string);
    }

    /**
     * Test accessing a non-joined nested field from a record set getFieldByPath().
     */
    public function testGetNonJoinedNestedFieldUsingGetFieldByPath()
    {
        $recordSet = new ORM_MockSet();

        $field = $recordSet->getFieldByPath('string');

        $this->assertEquals($field, $recordSet->string);

        $field = $recordSet->getFieldByPath('parent/string');

        $this->assertEquals($field, $recordSet->parent->string);

        $field = $recordSet->getFieldByPath('parent/parent/string');

        $this->assertEquals($field, $recordSet->parent->parent->string);

        $field = $recordSet->getFieldByPath('parent/parent/parent/string');

        $this->assertEquals($field, $recordSet->parent->parent->parent->string);
    }


    /**
     * Test accessing a non-existent field from a record set getFieldByPath().
     *
     *  @expectedException ORM_OutOfBoundException
     *                  Accessing a non-existent field on a record set
     *                  must throw an ORM_OutOfBoundException.
     */
    public function testGetInexistentFieldUsingGetFieldByPath()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);

        $set->addFields($field1);

        $set->getFieldByPath('wrong');

        $nameField = $set->getFieldByPath('name');

        $this->assertEquals($nameField, $field1);
    }

    /**
     * Test accessing a field from a record set using __call().
     */
    public function testGetFieldUsingMagicCallMethod()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $nameField = $set->name();

        $this->assertEquals($nameField, $field1);
    }


    /**
     * Test accessing an non-existent field from a record set.
     *
     * @expectedException ORM_Exception
     *                  Accessing an non-existent field from a record set
     *                  must throw an ORM_Exception.
     */
    public function testInvalidGetField()
    {
        $set = new ORM_TestRecordSet();

        $set->getField('nonExistent');
    }



    /**
     * Test adding primary key using setPrimaryKey().
     */
    public function testPrimaryKey()
    {
        $set = new ORM_TestRecordSet();
        $set->setPrimaryKey('id');

        $idField = $set->getField('id');

        $this->assertInstanceOf('ORM_PkField', $idField);

        $pkFieldName = $set->getPrimaryKey();
        $this->assertEquals($pkFieldName, 'id');

        $pkField = $set->getPrimaryKeyField();
        $this->assertSame($pkField, $idField);

        //     	$record = $set->newRecord();

        //     	$this->assertInstanceOf('ORM_TestRecord', $record);

        //     	$values = $record->getValues();
        //     	$this->assertInternalType('array', $values);

        //     	$this->assertArrayHasKey('id', $values);
    }



    /**
     * Test adding primary key using setPrimaryKey() on an existing field.
     */
    public function testPrimaryKeyOnExistingField()
    {
        $set = new ORM_TestRecordSet();

    	$field1 = new ORM_StringField('email', 255);
    	$set->addFields($field1);

    	$set->setPrimaryKey('email');

    	$emailField = $set->getField('email');
    	$this->assertSame($emailField, $field1);

    	$pkField = $set->getPrimaryKeyField();
    	$this->assertSame($pkField, $field1);

    	$pkFieldName = $set->getPrimaryKey();
    	$this->assertEquals($pkFieldName, 'email');
    }



    /**
     * Test adding an hasOne relation using the RecordSet classname.
     *
     * @return ORM_RecordSet
     */
    public function testHasOne()
    {
        $set = new ORM_TestRecordSet();
    	$set->setPrimaryKey('id');
    	$set->hasOne('otherTest', 'ORM_RecordSet');

    	$fkField = $set->getField('otherTest');

    	$this->assertInstanceOf(
    	    'ORM_FkField',
    	    $fkField
    	);

    	return $set;
    }



    public function testGetRelation()
    {
        static $uniq = 0;
        $uniq++;
        $set = new ORM_TestRecordSet();
    	$set->setPrimaryKey('id');

        $oneRelations = $set->getHasOneRelations();
        $nbOneRelations = count($oneRelations);

        $set->hasOne('otherTest' . $uniq, 'ORM_RecordSet');

        $relation = $set->getRelation('otherTest' . $uniq);

        $this->assertInstanceOf('ORM_OneRelation', $relation);

        $oneRelations = $set->getHasOneRelations();

        $this->assertCount($nbOneRelations + 1, $oneRelations);
    }


    /**
     * Test adding an hasOne relation using the Record classname
     * instead of RecordSet classname.
     */
    public function testHasOneUsingRecordClassname()
    {
        $set = new ORM_TestRecordSet();
        $set->setPrimaryKey('id');
        $set->hasOne('otherTest', 'ORM_TestRecord');

        $fkField = $set->getField('otherTest');

        $this->assertInstanceOf(
            'ORM_FkField',
            $fkField
        );
    }



    /**
     *
     */
    public function testHasOneJoined()
    {
        $set = $this->testHasOne();

        $currentSet = $set->join('otherTest');

        $field = $set->getField('otherTest');

        $this->assertInstanceOf(
            'ORM_RecordSet',
            $field,
            'Joining a FkField using the $recordSet->join() method, the joined field must be replaced by the corresponding RecordSet'
        );

        $this->assertSame(
            $currentSet,
            $set,
            'Joining a FkField using the $recordSet->join() method must return $recordSet'
        );
    }



    /**
     * Test joining a FkField using the ORM_RecordSet::__call() method.
     */
    public function testHasOneJoined2()
    {
        $set = $this->testHasOne();
        $joinedSet = $set->otherTest();

        $field = $set->getField('otherTest');

        $this->assertInstanceOf(
            'ORM_RecordSet',
            $field,
            'Joining a FkField using the ORM_RecordSet::__call() method, the joined field must be replaced by the corresponding RecordSet'
        );

        $this->assertSame(
            $joinedSet,
            $field,
            'Joining a FkField using the ORM_RecordSet::__call() method must return the joined RecordSet'
        );
    }





    /**
     * Test using getScalarField() returns the primary key field of a joined recordset/fkfield.
     */
    public function testGetScalarFieldOnJoinedFkField()
    {
        $set = new ORM_TestRecordSet();
        $set->setPrimaryKey('id');
        $set->hasOne('otherTest', 'ORM_TestRecordSet');

        $joinedSet = $set->otherTest();

        $field = $set->getField('otherTest');

        $this->assertInstanceOf(
            'ORM_RecordSet',
            $field,
            'Joining a FkField using the ORM_RecordSet::__call() method, the joined field must be replaced by the corresponding RecordSet'
        );

        $pk = $set->getScalarField('otherTest');

        $this->assertInstanceOf(
            'ORM_PkField',
            $pk
        );

        $this->assertSame(
            $joinedSet->id,
            $pk,
            'Joining a FkField using the ORM_RecordSet::__call() method must return the joined RecordSet'
        );
    }

    /**
     * Test using getScalarField() returns the foreign key field of a non-joined recordset/fkfield.
     */
    public function testGetScalarFieldOnNonJoinedFkField()
    {
        $set = new ORM_TestRecordSet();
        $set->setPrimaryKey('id');
        $set->hasOne('otherTest', 'ORM_TestRecordSet');

        $field = $set->getField('otherTest');

        $fk = $set->getScalarField('otherTest');

        $this->assertSame(
            $field,
            $fk,
            'Using getScalarField() on a non-joined FkField returns the FkField'
        );
    }



    /**
     * Test joining a field that is not a FkField.
     */
    public function testJoinOnWrongField()
    {
        $set = new ORM_TestRecordSet();
        $field1 = new ORM_StringField('name', 255);
        $field2 = new ORM_IntField('age');
        $set->addFields($field1, $field2);

        $result = $set->join('name');

        $this->assertNull($result);
    }


    /**
     * Test adding primary key using setPrimaryKey().
     */
    public function testNewRecord()
    {
        $set = new ORM_TestRecordSet();

        $record = $set->newRecord();

        $this->assertInstanceOf('ORM_TestRecord', $record);
    }

//     /**
//      * Test adding primary key using setPrimaryKey().
//      */
//     public function testNewRecord()
//     {
//         $set = new ORM_RecordSet();
//         $set->setPrimaryKey('id');

//         $idField = $set->getField('id');

//         $this->assertInstanceOf('ORM_PkField', $idField);

//         $pkFieldName = $set->getPrimaryKey();
//         $this->assertEquals($pkFieldName, 'id');

//         $pkField = $set->getPrimaryKeyField();
//         $this->assertSame($pkField, $idField);

//         //     	$record = $set->newRecord();

//         //     	$this->assertInstanceOf('ORM_TestRecord', $record);

//         //     	$values = $record->getValues();
//         //     	$this->assertInternalType('array', $values);

//         //     	$this->assertArrayHasKey('id', $values);
//     }



//     /**
//      * Test setting a backend.
//      */
//     public function testSetBackend()
//     {
//         $mockBackend = $this->getMock('ORM_Backend');

//         $set = new ORM_TestRecordSet();

//         ORM_RecordSet::setBackend($mockBackend);

//         $backend = $set->getBackend();

//         $this->assertSame($backend, $mockBackend);
//     }
}
