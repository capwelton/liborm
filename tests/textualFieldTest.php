<?php
require_once dirname(__FILE__) . '/fieldTest.php';

abstract class ORM_textualFieldTest extends ORM_fieldTest
{
    protected $fieldClass = 'ORM_TextualField';


    public function testIsValueSetWithNullAllowed()
    {
        $field = $this->construct('name');
        $field->setNullAllowed(true);

        $this->assertFalse($field->isValueSet(null));
        $this->assertTrue($field->isValueSet(''));
        $this->assertTrue($field->isValueSet(false), 'when saved, (string) false will equal an empty string');
        $this->assertTrue($field->isValueSet(0));
    }

    public function testIsValueSetWithNullNotAllowed()
    {
        $field = $this->construct('name');
        $field->setNullAllowed(false);

        $this->assertFalse($field->isValueSet(null), 'Null value is not set');
        $this->assertFalse($field->isValueSet(''), 'an empty string is not a set string if null value not allowed');
        $this->assertFalse($field->isValueSet(false), 'when saved, (string) false will equal an empty string');
        $this->assertTrue($field->isValueSet(0));
    }
}
