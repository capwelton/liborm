<?php

require_once dirname(__FILE__) . '/mock/core.php';


abstract class ORM_fieldTest extends PHPUnit_Framework_TestCase
{
    protected $fieldClass = 'ORM_Field';

    /**
     * @return ORM_Field
     */
    protected function construct()
    {
        require_once dirname(__FILE__) . '/../programs/field.class.php';
        $args = func_get_args();
        $field = $this->getMockForAbstractClass($this->fieldClass, $args);
        return $field;
    }


    protected function setUp()
    {

    }




    public function invalidNameProvider()
    {
        return array(
            array('1name'),
            array('a name'),
            array('a-name'),
//            array(''),
            array('abc/def')
        );
    }


    /**
     *
     */
    public function testNewField()
    {
        // Creates a Mock_ORM_Field.
        $field = $this->construct('name');

        $this->assertInstanceOf($this->fieldClass, $field);
    }




    /**
     * Trying to create a field with an invalid name must throw
     * an ORM_IllegalArgumentException.
     *
     * @dataProvider invalidNameProvider
     * @expectedException ORM_IllegalArgumentException
     */
    public function testCreateFieldWithInvalidName($name)
    {
        // Creates a Mock_ORM_Field.
        $field = $this->construct($name);
    }


    public function testPathIsSameAsNameForNewlyCreatedField()
    {
        // Creates a Mock_ORM_Field.
        $field = $this->construct('name');

        $this->assertEquals(
            $field->getName(),
            $field->getPath()
        );
    }



    public function testPathIsSameAsNameForFieldAddedToNamelessRecordSet()
    {
//        require_once dirname(__FILE__) . '/../programs/recordset.class.php';
        require_once dirname(__FILE__) . '/../programs/set.class.php';
        $set = $this->getMockForAbstractClass('ORM_RecordSet');

        // Creates a Mock_ORM_Field.
        $field = $this->construct('name');

        $set->addFields($field);

        $this->assertEquals(
            $field->getName(),
            $field->getPath()
        );
    }



    public function testPathOfFieldInNestedRecordSets()
    {
//        require_once dirname(__FILE__) . '/../programs/recordset.class.php';
        require_once dirname(__FILE__) . '/../programs/set.class.php';

        $set = new ORM_RecordSet();

        $subset = new ORM_RecordSet('subset');

        // Creates a Mock_ORM_Field.
        $field = $this->construct('name');

        $subset->addFields($field);

        $set->addFields($subset);

        $this->assertEquals(
            'subset/name',
            $field->getPath()
        );
    }



    public function testIsNullAllowedReturnsNull()
    {
        require_once dirname(__FILE__) . '/../programs/set.class.php';

        $set = new ORM_RecordSet();

        $field = $this->construct('nullAllowed');
        $field->setNullAllowed(true);

        $set->addFields($field);

        $result = $field->input(null);

        $this->assertNull($result);
    }


    public function testIsNullAllowedIsCheckedCorrectly()
    {
        $field = $this->construct('nullAllowed');
        $field->setNullAllowed(true);

        $this->assertEquals(null, $field->checkInput(null));
    }


    public function testIsNotNullAllowedIsCheckedCorrectly()
    {
        $field = $this->construct('nullNotAllowed');
        $field->setNullAllowed(false);

        $this->assertNotEquals(null, $field->checkInput(null));
    }




    public function testOutputWidgetIsDisplayable()
    {
        $field = $this->construct('display');

        $item = $field->outputWidget(null);

        $this->assertInstanceOf('Widget_Displayable_Interface', $item);
    }


    public function testGetWidgetIsDisplayable()
    {
        $field = $this->construct('display');

        $item = $field->getWidget();

        $this->assertInstanceOf('Widget_Displayable_Interface', $item);
    }
}
