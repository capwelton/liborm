<?php

require_once dirname(__FILE__) . '/mock/core.php';



class ORM_CriteriaTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test adding two ORM_Field to a record set using addFields().
     */
    public function testMatchSearchCriterion()
    {
        $field = new ORM_TextField('text');
        $criterion = new ORM_MatchSearchCriterion($field, 'A house of cards', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();

        $this->assertCount(2, $keywords);
        $this->assertContains('house', $keywords);
        $this->assertContains('cards', $keywords);


        $criterion = new ORM_MatchSearchCriterion($field, 'A "house of cards"', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();

        $this->assertCount(1, $keywords);
        $this->assertContains('house of cards', $keywords);

        $criterion = new ORM_MatchSearchCriterion($field, 'Hello, how are you?', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();

        $this->assertCount(4, $keywords);
        $this->assertContains('Hello', $keywords);
        $this->assertContains('how', $keywords);
        $this->assertContains('are', $keywords);
        $this->assertContains('you', $keywords);


        $criterion = new ORM_MatchSearchCriterion($field, 'Hello, "how are you?"', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();

        $this->assertCount(2, $keywords);
        $this->assertContains('Hello', $keywords);
        $this->assertContains('how are you?', $keywords);

        $criterion = new ORM_MatchSearchCriterion($field, 'a "b" c', '_AND_', 'contains');

        $keywords = $criterion->getSearchStringKeywords();

        $this->assertCount(1, $keywords);
        $this->assertContains('b', $keywords);
    }


    public function testOr()
    {
        $bool1 = new ORM_BoolField('MyBoolean1');
        $bool2 = new ORM_BoolField('MyBoolean2');

        $condition1 = $bool1->is(true);
        $condition2 = $bool2->is(true);

        $this->assertInstanceOf(
            'ORM_Or',
            $condition1->_OR_($condition2)
        );
    }


    public function testOrSimplification()
    {
        $bool = new ORM_BoolField('MyBoolean');
        $true = new ORM_TrueCriterion();
        $false = new ORM_FalseCriterion();

        $condition = $bool->is(true);

        $this->assertInstanceOf(
            'ORM_TrueCriterion',
            $condition->_OR_($true)
        );

        $this->assertSame(
            $condition,
            $false->_OR_($condition)
        );

        $this->assertSame(
            $condition,
            $condition->_OR_($false)
        );

        $this->assertInstanceOf(
            'ORM_TrueCriterion',
            $true->_OR_($condition)
        );
    }


    public function testAnd()
    {
        $bool1 = new ORM_BoolField('MyBoolean1');
        $bool2 = new ORM_BoolField('MyBoolean2');

        $condition1 = $bool1->is(true);
        $condition2 = $bool2->is(true);

        $this->assertInstanceOf(
            'ORM_And',
            $condition1->_AND_($condition2)
        );
    }


    public function testAndSimplification()
    {
        $bool = new ORM_BoolField('MyBoolean');
        $true = new ORM_TrueCriterion();
        $false = new ORM_FalseCriterion();

        $condition = $bool->is(true);

        $this->assertInstanceOf(
            'ORM_FalseCriterion',
            $condition->_AND_($false)
        );

        $this->assertSame(
            $condition,
            $true->_AND_($condition)
        );

        $this->assertSame(
            $condition,
            $condition->_AND_($true)
        );

        $this->assertInstanceOf(
            'ORM_FalseCriterion',
            $false->_AND_($condition)
        );
    }
}
