<?php

$GLOBALS['babInstallPath'] = dirname(__FILE__).'/../../vendor/capwelton/core/programs/';


require_once $GLOBALS['babInstallPath'].'utilit/dbutil.php';
require_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/userincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/addonapi.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/utilit.php';
require_once $GLOBALS['babInstallPath'].'utilit/skinincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/functionality.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';



// create babDB

$GLOBALS['babDBHost'] = getenv('babDBHost') !== false ? getenv('babDBHost') : '127.0.0.1';
$GLOBALS['babDBLogin'] = getenv('babDBLogin') !== false ? getenv('babDBLogin') : 'test';
$GLOBALS['babDBPasswd'] = getenv('babDBPasswd') !== false ? getenv('babDBPasswd') : null;
$GLOBALS['babDBName'] = getenv('babDBName') !== false ? getenv('babDBName') : 'test';

$GLOBALS['babDB'] = new babDatabase();

$GLOBALS['babStyle'] = 'ovidentia css';

// exec('mysql -u test -Nse "show tables" test | while read table; do mysql -u test -e "drop table $table" test; done');

exec('mysql -u ' . $GLOBALS['babDBLogin'] . (isset($GLOBALS['babDBPasswd']) ? ' -p' .  $GLOBALS['babDBPasswd'] : '') . ' ' . $GLOBALS['babDBName'] . ' < vendor/capwelton/core/install/babinstall.sql 2>/dev/null');


$GLOBALS['babDB']->db_query("INSERT INTO bab_addons(title) VALUES ('widgets')");

$GLOBALS['babLanguage'] = 'en';

if (!defined('FUNC_WIDGETS_PHP_PATH')) {
    define('FUNC_WIDGETS_PHP_PATH', realpath(dirname(__FILE__) . '/../../vendor/capwelton/widgets/programs/widgets') . '/');
}

$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */
$session->setStorage(new bab_SessionMockStorage());


$functionalities = new bab_functionalities();
$functionalities->register('LibOrm', dirname(__FILE__) . '/../../programs/orm.class.php');
$functionalities->register('Widgets', dirname(__FILE__) . '/../../vendor/capwelton/widgets/programs/widgets.php');
// $functionalities->register('PhoneNumber', dirname(__FILE__) . '/../../vendor/capwelton/libphonenumber/programs/phonenumber.class.php');
//$functionalities->register('jquery', dirname(__FILE__) . '/../../vendor/ovidentia/jquery/programs/jquery.php');

/*@var $LibOrm Func_LibOrm */

$LibOrm = bab_Functionality::get('LibOrm');
$LibOrm->initMysql();
$babDB = bab_getDB();
$mysqlBackend = new ORM_MySqlBackend($babDB);
ORM_RecordSet::setBackend($mysqlBackend);

$babDB->db_query('DROP TABLE IF EXISTS orm_mock');
$babDB->db_query('DROP TABLE IF EXISTS orm_mockfieldkey');
$babDB->db_query('DROP TABLE IF EXISTS orm_testrecord');

require_once dirname(__FILE__) . '/testrecord.class.php';
require_once dirname(__FILE__) . '/mock.class.php';
require_once dirname(__FILE__) . '/mockfieldkey.class.php';


require_once dirname(__FILE__) . '/../../vendor/capwelton/core/programs/utilit/devtools.php';

$synchronize = new bab_synchronizeSql();

$sql = $mysqlBackend->setToSql(new ORM_MockSet()) . "\n";
$synchronize->fromSqlString($sql);

$sql = $mysqlBackend->setToSql(new ORM_MockFieldKeySet()) . "\n";
$synchronize->fromSqlString($sql);

$sql = $mysqlBackend->setToSql(new ORM_TestRecordSet()) . "\n";
$synchronize->fromSqlString($sql);


