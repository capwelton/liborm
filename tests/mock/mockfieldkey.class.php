<?php

require_once dirname(__FILE__) . '/mock.class.php';


class ORM_MockFieldKeySet extends ORM_MockSet
{
    public function __construct()
    {
        parent::__construct();

        $this->addIndexKey($this->getField('int'));
        $this->addUniqueKey($this->getField('datetime'), $this->getField('enum'));
    }
}


class ORM_MockFieldKey extends ORM_Mock
{
}
