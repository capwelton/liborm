<?php

require_once dirname(__FILE__) . '/numericFieldTest.php';

class ORM_decimalFieldTest extends ORM_numericFieldTest
{
    protected $fieldClass = 'ORM_DecimalField';


    public function testDecimalFieldWithAccuracy()
    {
        $field = $this->construct('decimal', 3);

        $this->assertInstanceOf('ORM_DecimalField', $field);
        $this->assertEquals(3, $field->getDecimalPlaces());
    }


    public function testDecimalFieldWithPrecision()
    {
        $field = $this->construct('decimal');
        $field->setPrecision(8, 2);

        $this->assertInstanceOf('ORM_DecimalField', $field);
        $this->assertEquals(8, $field->getPrecision());
        $this->assertEquals(2, $field->getDecimalPlaces());
    }


    public function testInputValuesAreCheckedCorrectly()
    {
        $field = $this->construct('decimal', 3);

        $this->assertEquals(null, $field->checkInput('0'));

        $this->assertEquals(null, $field->checkInput('12.6'));

        $this->assertEquals(null, $field->checkInput('  12 . 6 '));

        $this->assertEquals(null, $field->checkInput('  12,6 '));

        $this->assertEquals(null, $field->checkInput('1 000 000.62'));

        $this->assertEquals(null, $field->checkInput('-1 000 000,62'));

        $this->assertEquals(null, $field->checkInput('+1 000 000,62'));


        $this->assertNotEquals(null, $field->checkInput('abc'));

        $this->assertNotEquals(null, $field->checkInput('12.6.2'));
    }


    public function testInputValuesAreConvertedCorrectly()
    {
        $field = $this->construct('decimal', 3);

        // Empty string yields zero
        $this->assertEquals('0.000', $field->input(''));

        // Non numeric string is returned unmodified.
        $this->assertEquals('text', $field->input('text'), 'Non numeric string is returned unmodified.');

        $this->assertEquals('1.123', $field->input(1.1234));
        $this->assertEquals('1.124', $field->input(1.1235));
        $this->assertEquals('1.124', $field->input(1.1236));

        $this->assertEquals('1.123', $field->input('1.1234'));
        $this->assertEquals('1.124', $field->input('1.1235'));
        $this->assertEquals('1.124', $field->input('1.1236'));

        $this->assertEquals('1.123', $field->input('1,1234'));
        $this->assertEquals('1.124', $field->input('1,1235'));
        $this->assertEquals('1.124', $field->input('1,1236'));

        $this->assertEquals('-1.123', $field->input('-1.1234'));
        $this->assertEquals('-1.124', $field->input('-1.1235'));
        $this->assertEquals('-1.124', $field->input('-1.1236'));

        $this->assertEquals('-1.123', $field->input('-1,1234'));
        $this->assertEquals('-1.124', $field->input('-1,1235'));
        $this->assertEquals('-1.124', $field->input('-1,1236'));

        $this->assertEquals('2.000', $field->input('2'));

        $this->assertEquals('2123.000', $field->input('2 123'));

        // nbsp in iso-8859-15 as returned by the bab_nbsp() in mockObjects.php
        $this->assertEquals('2123.000', $field->input('2'.chr(160).'123'));

        $this->assertEquals('12.6', $field->input('  12 . 6 '), 'Spaces are removed');
    }



    public function testIsValueSetWithNullAllowed()
    {
        $field = $this->construct('decimal', 3);
        $field->setNullAllowed(true);

        $this->assertFalse($field->isValueSet(null));
        $this->assertTrue($field->isValueSet(''));
        $this->assertTrue($field->isValueSet(false));
        $this->assertTrue($field->isValueSet(0));
        $this->assertTrue($field->isValueSet(0.000));
        $this->assertTrue($field->isValueSet('0.000'));
    }

    public function testIsValueSetWithNullNotAllowed()
    {
        $field = $this->construct('decimal', 3);
        $field->setNullAllowed(false);

        $this->assertFalse($field->isValueSet(null), 'Null value is not set');
        $this->assertFalse($field->isValueSet(''), 'an empty string is not a set string if null value not allowed');
        $this->assertFalse($field->isValueSet(false));
        $this->assertFalse($field->isValueSet(0.000));
        $this->assertFalse($field->isValueSet('0.000'));
        $this->assertTrue($field->isValueSet(0.001));
        $this->assertTrue($field->isValueSet('0.001'));
    }
}
