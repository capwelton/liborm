## Interface avec la base de donn�es MYSQL ##

Biblioth�que de correspondance entre base de donn�es et objets PHP (mapping objet-relationel)

[![Build Status](https://scrutinizer-ci.com/b/capwelton/liborm/badges/build.png?b=master)](https://scrutinizer-ci.com/b/capwelton/liborm/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/capwelton/liborm/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/capwelton/liborm/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/capwelton/liborm/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/capwelton/liborm/?branch=master)
